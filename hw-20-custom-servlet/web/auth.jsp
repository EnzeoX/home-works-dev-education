<%--
  Created by IntelliJ IDEA.
  User: Boyko
  Date: 4/22/2021
  Time: 12:33 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>LOGIN</title>
</head>
<style>
    .authBox {
        width: 30%;
        height: 200px;
        margin-right: auto;
        margin-left: auto;
        margin-top: 10%;
        border: 1px solid black;
    }

    #inputBox {
        display: flex;
        flex-direction: column;
        width: 90%;
        height: 60px;
        margin-left: auto;
        margin-right: auto;
        margin-top: 10px;
    }

    input {
        width: 50%;
        height: 20px;
        margin-right: auto;
        margin-left: auto;
    }

    #buttonBox {
        display: flex;
        flex-direction: column;
        width: 90%;
        height: 60px;
        margin-left: auto;
        margin-right: auto;
        margin-top: 10px;
    }
</style>
<body>
<form class="authBox"  method="post" action="auth">
    <div id="inputBox">
        <input type="text" name="login" placeholder="login">
        <input type="text" name="password" placeholder="password">
    </div>
    <div id="buttonBox">
        <input type="submit" value="LogIN" name="getIn">
    </div>
</form>
</body>
</html>
