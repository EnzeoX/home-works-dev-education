package org.bitbucket;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("auth")
public class AuthServlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("AUTH GET");
        String name = request.getParameter("login");
        System.out.println(name);
        String password = request.getParameter("password");
        System.out.println(password);
        if (AuthAccepted.accepted(name, password)) {
            response.setStatus(200);
            HttpSession session = request.getSession();
            session.setAttribute("Test", "Test");
            session.setMaxInactiveInterval(10000);
            Cookie cookie = new Cookie("Test", name);
            cookie.setMaxAge(10000);
            response.addCookie(cookie);
            request.getRequestDispatcher("/authorized").forward(request, response);
        } else {
            response.setStatus(403);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/");
            PrintWriter out = response.getWriter();
            out.println("<h2> Username or password wrong. Status code: " + response.getStatus() + " </h2>");
            dispatcher.include(request, response);
        }
    }
}
