package org.bitbucket;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

import javax.servlet.ServletException;

public class Run {

    public static void servletStart() throws LifecycleException, ServletException {
        Tomcat tomcat = new Tomcat();
        tomcat.setPort(8080);
        String str = "C:\\Users\\Admin\\Desktop\\Java\\home-works-dev-education\\hw-20-custom-servlet\\web\\";
        tomcat.addWebapp("",str);
        tomcat.start();
        tomcat.getServer().await();
    }
}
