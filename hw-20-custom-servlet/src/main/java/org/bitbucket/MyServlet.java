package org.bitbucket;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/main")
public class MyServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("GET");
        System.out.println(request.getRequestURI());
        if (request.getParameter("loginBtn") != null) {
            request.getRequestDispatcher("auth.jsp").forward(request, response);
        } else if(request.getParameter("registerBtn") != null) {
            request.getRequestDispatcher("test.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("main.jsp").forward(request, response);
        }
    }
}
