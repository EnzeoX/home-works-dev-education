package org.bitbucket;

import java.io.IOException;
import java.net.Socket;

public class RequestResponseExecutor implements Runnable {

    private String name;

    private Socket socket;

    public RequestResponseExecutor(Socket socket, String name) {
        this.name = name;
        this.socket = socket;
    }

    private SimpleHttpRequest request = new SimpleHttpRequest();

    private SimpleHttpResponse response = new SimpleHttpResponse();

    @Override
    public void run() {
        try {
            this.request.readRequest(this.socket);
            ActionSelector.selectAction(this.request, this.response);
            this.response.sendResponse(this.socket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void doGet(SimpleHttpRequest request, SimpleHttpResponse response) {
        String stringMsg = "<h1>GET method</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setCode(200);
        response.setStatusText("Ok");
        response.putHeaders("Content-Type", "text/html");
        response.putHeaders("Content-Length", "" + stringMsg.length());
        response.setBody(stringMsg);
    }

    public static void doPut(SimpleHttpRequest request, SimpleHttpResponse response) {
        String stringMsg = "<h1>PUT method</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setCode(200);
        response.setStatusText("Ok");
        response.putHeaders("Content-Type", "text/html");
        response.putHeaders("Content-Length", "" + stringMsg.length());
        response.setBody(stringMsg);
    }

    public static void doPost(SimpleHttpRequest request, SimpleHttpResponse response) {
        String stringMsg = "<h1>POST method</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setCode(200);
        response.setStatusText("Ok");
        response.putHeaders("Content-Type", "text/html");
        response.putHeaders("Content-Length", "" + stringMsg.length());
        response.setBody(stringMsg);
    }

    public static void doConnect(SimpleHttpRequest request, SimpleHttpResponse response) {
        String stringMsg = "<h1>CONNECT method</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setCode(200);
        response.setStatusText("Ok");
        response.putHeaders("Content-Type", "text/html");
        response.putHeaders("Content-Length", "" + stringMsg.length());
        response.setBody(stringMsg);
    }

    public static void doDelete(SimpleHttpRequest request, SimpleHttpResponse response) {
        String stringMsg = "<h1>DELETE method</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setCode(200);
        response.setStatusText("Ok");
        response.putHeaders("Content-Type", "text/html");
        response.putHeaders("Content-Length", "" + stringMsg.length());
        response.setBody(stringMsg);
    }
}
