package org.bitbucket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.*;

public class Server {

    private int port;

    private ServerSocket serverSocket;

    private int threadCounter;

    private boolean serverActive = true;

    private static Socket socket = null;

    private ExecutorService service = Executors.newFixedThreadPool(80);

    public Server(int port) {
        this.port = port;
    }

    public void serverStop() {
        this.serverActive = false;
    }

    public void serverStart() {
        try {
            this.serverSocket = new ServerSocket(this.port);
            this.serverSocket.setSoTimeout(20000);
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (this.serverActive) {
            try {
                socket = serverSocket.accept();
                this.service.execute(new RequestResponseExecutor(socket, RequestResponseExecutor.class.getName()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
