package org.bitbucket;

public class ActionSelector {

    public static void selectAction(SimpleHttpRequest request, SimpleHttpResponse response) {
        switch(request.getMethod()) {
            case "GET":
                RequestResponseExecutor.doGet(request, response);
                break;
            case "PUT":
                RequestResponseExecutor.doPut(request, response);
                break;
            case "POST":
                RequestResponseExecutor.doPost(request, response);
                break;
            case "CONNECT":
                RequestResponseExecutor.doConnect(request, response);
                break;
            case "DELETE":
                RequestResponseExecutor.doDelete(request, response);
                break;
            default:
                throw new IllegalArgumentException(request.getMethod() + "unavailable");
        }
    }
}
