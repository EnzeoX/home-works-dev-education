package org.bitbucket;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class SimpleHttpResponse {

    private String protocol;

    private String statusText;

    private String body;

    private int code;

    private Map<String, String> headers = new HashMap<>();

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public SimpleHttpResponse() {

    }

    public SimpleHttpResponse(String protocol, String statusText, String body, int code, Map<String, String> headers) {
        this.protocol = protocol;
        this.statusText = statusText;
        this.body = body;
        this.code = code;
        this.headers = headers;
    }

    public void putHeaders(String headers, String key) {
        this.headers.put(headers, key);
    }

    public void sendResponse(Socket socket) throws IOException {
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        bw.write(this.toString());
        bw.flush();
        bw.close();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.protocol);
        sb.append(" ");
        sb.append(this.code);
        sb.append(" ");
        sb.append(this.statusText);
        sb.append("\n\r");
        for (String header : this.headers.keySet()) {
            sb.append(header);
            sb.append(": ");
            sb.append(this.headers.get(header));
            sb.append("\n\r");
        }
        sb.append("\n\r");
        sb.append(this.body);
        sb.append("\n\r\n\r");
        return sb.toString();
    }
}
