package org.bitbucket;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;

import java.util.*;

public class TokenProvider {

    private static final String KEY = "secretKeyIsSecretPublic";

    private static final Map<String, Object> headerClaims = new HashMap<>();

    private static final Calendar calendar = Calendar.getInstance();

    private static final int expirationTime = 1;

    private static final Base64.Decoder decoder = Base64.getDecoder();

    public static String generateToken(long id) {
        try {
            Date date = calendar.getTime();
            calendar.add(Calendar.SECOND, expirationTime);
            Date expirationDate = calendar.getTime();
            headerClaims.put("typ", "JWT");
            headerClaims.put("alg", "HMAC256");
            Algorithm result = Algorithm.HMAC256(KEY);
            return JWT.create()
                    .withIssuer(String.valueOf(id))
                    .withExpiresAt(expirationDate)
                    .withIssuedAt(date)
                    .withNotBefore(date)
                    .withHeader(headerClaims)
                    .sign(result);
        } catch (JWTCreationException e) {
            e.printStackTrace();
            throw new JWTCreationException("Token creation error", e);
        }
    }

    public static void isValidToken(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(KEY);
            JWTVerifier verifier = JWT.require(algorithm)
                    .build();
            verifier.verify(token);
        } catch (JWTVerificationException e) {
            throw new JWTVerificationException(e.getMessage());
        }
    }
}
