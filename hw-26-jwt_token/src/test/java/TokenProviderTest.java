import com.auth0.jwt.exceptions.JWTVerificationException;
import org.bitbucket.TokenProvider;
import org.junit.Test;

public class TokenProviderTest {

    private static final String notValidToken = "eyJ0exAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYmYiOjE2MjM1MjYzNDEsImlzcyI6IjEwIiwiZXhwIjoxNjIzNTI2MzQzLCJpYXQiOjE2MjM1MjYzNDF9.jZXUw-TLV3xf01usfzIhjVspdMR4Lup46JVincEaaw";

    private static final long userId = 10L;

    @Test
    public void tokenIsValid() {
        String token = TokenProvider.generateToken(userId);
        TokenProvider.isValidToken(token);
    }

    @Test(expected = JWTVerificationException.class)
    public void tokenValidationIncorrectToken() {
        TokenProvider.isValidToken(notValidToken);
    }

    @Test(expected = JWTVerificationException.class)
    public void tokenValidationExpiredToken() throws InterruptedException {
        String exp = TokenProvider.generateToken(10L);
        Thread.sleep(2000);
        TokenProvider.isValidToken(exp);
    }
}
