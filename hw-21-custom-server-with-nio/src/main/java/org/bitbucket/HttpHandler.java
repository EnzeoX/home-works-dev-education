package org.bitbucket;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

public class HttpHandler {

    private CustomHttpRequest req;

    private CustomHttpResponse resp;

    private SocketChannel client;

    public HttpHandler(SocketChannel client) throws IOException {
        this.client = client;
        this.req = new CustomHttpRequest(client);
        this.resp = new CustomHttpResponse();
    }

    public void formatResponse() throws IOException {
        String typeRequest = this.req.getRequestType();
        if(typeRequest.equals("GET")){
            doGet(this.req, this.resp);
        } else if (typeRequest.equals("PUT")){
            doPut(this.req, this.resp);
        } else if (typeRequest.equals("POST")){
            doPost(this.req, this.resp);
        } else if (typeRequest.equals("DELETE")) {
            doDelete(this.req, this.resp);
        } else {
            doOther(this.req, this.resp);
        }
    }

    public void doGet(CustomHttpRequest req, CustomHttpResponse resp) throws IOException {
        String response = CustomHttpResponse.responseOnGet();
        byte[] r = response.getBytes(StandardCharsets.UTF_8);
        client.write(ByteBuffer.wrap(r));
    }

    public void doPost(CustomHttpRequest req, CustomHttpResponse resp) throws IOException {
        String body = req.getBody();
        String name = getName(body);
        String greeting = getGreetings(body);
        String response = CustomHttpResponse.responseOnPost(name, greeting);
        byte[] r = response.getBytes(StandardCharsets.UTF_8);
        client.write(ByteBuffer.wrap(r));
    }

    public void doPut(CustomHttpRequest req, CustomHttpResponse resp) throws IOException {
        String response = CustomHttpResponse.responseOnPut();
        byte[] r = response.getBytes(StandardCharsets.UTF_8);
        client.write(ByteBuffer.wrap(r));
    }

    public void doDelete(CustomHttpRequest req, CustomHttpResponse resp) throws IOException {
        String response = CustomHttpResponse.responseOnDelete();
        byte[] r = response.getBytes(StandardCharsets.UTF_8);
        client.write(ByteBuffer.wrap(r));
    }

    public void doOther(CustomHttpRequest req, CustomHttpResponse resp) throws IOException {
        String response = CustomHttpResponse.responseOnOther();
        byte[] r = response.getBytes(StandardCharsets.UTF_8);
        client.write(ByteBuffer.wrap(r));
    }

    public static String getName(String body) {
        int str = body.indexOf("&to=");
        String name = body.substring(str+4);
        return name;
    }

    public static String getGreetings(String body) {
        int f = body.indexOf("&to=");
        int s = body.indexOf("say=");
        String name = body.substring(s+4, f);
        return name;
    }
}
