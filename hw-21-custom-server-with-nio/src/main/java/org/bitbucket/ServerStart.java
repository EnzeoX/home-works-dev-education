package org.bitbucket;

import java.io.IOException;

public class ServerStart {

    public static void main(String[] args) throws IOException {
        ServerRun.serverStart(8080);
    }
}
