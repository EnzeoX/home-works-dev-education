package org.bitbucket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class ServerRun {

    public static void serverStart(int port) throws IOException {
        ServerSocketChannel serverSocket;
        SocketChannel client;
        serverSocket = ServerSocketChannel.open();
        serverSocket.socket().bind(new InetSocketAddress(port));
        System.out.println("Server started with port " + port);

        while (true) {
            client = serverSocket.accept();
            System.out.println("ConnectionSet: " + client.getRemoteAddress());
            HttpHandler handler = new HttpHandler(client);
            handler.formatResponse();
            System.out.println("Socket closed.");
            client.close();
        }
    }
}
