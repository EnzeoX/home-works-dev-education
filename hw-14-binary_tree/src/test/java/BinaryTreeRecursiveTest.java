import org.bitbucket.BSTree;
import org.bitbucket.ITree;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class BinaryTreeRecursiveTest {

    public ITree binaryTree = new BSTree();

    //{}{}{}{}{}{}{}{}{} INIT TEST {}{}{}{}{}{}{}{}{}{}{}{}

    @Test
    public void initFiveNotOrganized() {
        this.binaryTree.init(TreeMockData.getArrayOfFiveNotOrganized());
        int[] exp = {3, 2, 1, 4, 5};
        int[] act = this.binaryTree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void initFive() {
        this.binaryTree.init(TreeMockData.getArrayOfFive());
        int[] exp = {1, 2, 3, 4, 5};
        int[] act = this.binaryTree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void initTen() {
        this.binaryTree.init(TreeMockData.getArrayOfTenNotOrganized());
        this.binaryTree.print();
        int[] exp = {10, 4, 2, 1, 3, 5, 6, 9, 7, 8};
        int[] act = this.binaryTree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void initTestZero() {
        this.binaryTree.init(TreeMockData.getArrayZero());
    }

    //{}{}{}{}{}{}{}{}{} SIZE TEST {}{}{}{}{}{}{}{}{}{}{}{}

    @Test
    public void sizeFive() {
        this.binaryTree.init(TreeMockData.getArrayOfFive());
        int exp = 5;
        int act = this.binaryTree.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeFiveNotOrganized() {
        this.binaryTree.init(TreeMockData.getArrayOfFiveNotOrganized());
        int exp = 5;
        int act = this.binaryTree.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeTen() {
        this.binaryTree.init(TreeMockData.getArrayOfTen());
        int exp = 10;
        int act = this.binaryTree.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeTenNotOrganized() {
        this.binaryTree.init(TreeMockData.getArrayOfTenNotOrganized());
        int exp = 10;
        int act = this.binaryTree.size();
        assertEquals(exp, act);
    }

    //{}{}{}{}{}{}{}{}{} PRINT TEST {}{}{}{}{}{}{}{}{}{}{}{}

    @Test
    public void printFive() {
        this.binaryTree.init(TreeMockData.getArrayOfFive());
        this.binaryTree.print();
    }

    @Test
    public void printTen() {
        this.binaryTree.init(TreeMockData.getArrayOfTen());
        this.binaryTree.print();
    }

    //{}{}{}{}{}{}{}{}{} CLEAR TEST {}{}{}{}{}{}{}{}{}{}{}{}

    @Test
    public void clearFive() {
        this.binaryTree.init(TreeMockData.getArrayOfFive());
        this.binaryTree.clear();
        int act = this.binaryTree.size();
        int exp = 0;
        assertEquals(exp, act);
    }

    @Test
    public void clearTen() {
        this.binaryTree.init(TreeMockData.getArrayOfTen());
        this.binaryTree.clear();
        int act = this.binaryTree.size();
        int exp = 0;
        assertEquals(exp, act);
    }


    //{}{}{}{}{}{}{}{}{} NODES TEST {}{}{}{}{}{}{}{}{}{}{}{}

    @Test
    public void nodesFiveNotOrganized() {
        this.binaryTree.init(TreeMockData.getArrayOfFiveNotOrganized());
        int act = this.binaryTree.nodes();
        int exp = 5;
        assertEquals(exp, act);
    }

    @Test
    public void nodesFive() {
        this.binaryTree.init(TreeMockData.getArrayOfFive());
        int act = this.binaryTree.nodes();
        int exp = 5;
        assertEquals(exp, act);
    }

    @Test
    public void nodesTenNotOrganized() {
        this.binaryTree.init(TreeMockData.getArrayOfTenNotOrganized());
        int act = this.binaryTree.nodes();
        int exp = 10;
        assertEquals(exp, act);
    }

    @Test
    public void nodesTen() {
        this.binaryTree.init(TreeMockData.getArrayOfTen());
        int act = this.binaryTree.nodes();
        int exp = 10;
        assertEquals(exp, act);
    }

    //{}{}{}{}{}{}{}{}{} LEAVES TEST {}{}{}{}{}{}{}{}{}{}{}{}

    @Test
    public void leavesFive() {
        this.binaryTree.init(TreeMockData.getArrayOfFive());
        int act = this.binaryTree.leaves();
        int exp = 1;
        assertEquals(exp, act);
    }

    @Test
    public void leavesTen() {
        this.binaryTree.init(TreeMockData.getArrayOfTen());
        int act = this.binaryTree.leaves();
        int exp = 1;
        assertEquals(exp, act);
    }

    @Test
    public void leavesTenNotOrganized() {
        this.binaryTree.init(TreeMockData.getArrayOfTenNotOrganized());
        int act = this.binaryTree.leaves();
        int exp = 3;
        assertEquals(exp, act);
    }

    @Test
    public void leavesFiveNotOrganized() {
        this.binaryTree.init(TreeMockData.getArrayOfFiveNotOrganized());
        int act = this.binaryTree.leaves();
        int exp = 2;
        assertEquals(exp, act);
    }


    //{}{}{}{}{}{}{}{}{} DELETE TEST {}{}{}{}{}{}{}{}{}{}{}{}

    @Test
    public void deleteFive() {
        this.binaryTree.init(TreeMockData.getArrayOfFive());
        this.binaryTree.delete(3);
        this.binaryTree.print();
    }

    @Test
    public void deleteFiveNotOrganized() {
        this.binaryTree.init(TreeMockData.getArrayOfFiveNotOrganized());
        this.binaryTree.delete(3);
        this.binaryTree.print();
    }

    @Test
    public void deleteTen() {
        this.binaryTree.init(TreeMockData.getArrayOfTen());
        this.binaryTree.delete(10);
        this.binaryTree.print();
    }

    //{}{}{}{}{}{}{}{}{} WIDTH TEST {}{}{}{}{}{}{}{}{}{}{}{}

    @Test
    public void widthFiveNotOrganized() {
        this.binaryTree.init(TreeMockData.getArrayOfFiveNotOrganized());
        int act = this.binaryTree.width();
        int exp = 2;
        assertEquals(exp, act);
    }

    @Test
    public void widthTenNotOrganized() {
        this.binaryTree.init(TreeMockData.getArrayOfTenNotOrganized());
        int act = this.binaryTree.width();
        int exp = 3;
        assertEquals(exp, act);
    }

    @Test
    public void widthFive() {
        this.binaryTree.init(TreeMockData.getArrayOfFive());
        int act = this.binaryTree.width();
        int exp = 1;
        assertEquals(exp, act);
    }

    @Test
    public void widthTen() {
        this.binaryTree.init(TreeMockData.getArrayOfTen());
        int act = this.binaryTree.width();
        int exp = 1;
        assertEquals(exp, act);
    }


    //{}{}{}{}{}{}{}{}{} HEIGHT TEST {}{}{}{}{}{}{}{}{}{}{}{}

    @Test
    public void heightFiveNotOrganized() {
        this.binaryTree.init(TreeMockData.getArrayOfFiveNotOrganized());
        int act = this.binaryTree.height();
        int exp = 3;
        assertEquals(exp, act);
    }

    @Test
    public void heightFive() {
        this.binaryTree.init(TreeMockData.getArrayOfFive());
        int act = this.binaryTree.height();
        int exp = 5;
        assertEquals(exp, act);
    }


    //{}{}{}{}{}{}{}{}{} REVERSE TEST {}{}{}{}{}{}{}{}{}{}{}{}

    @Test
    public void reverseFive() {
        this.binaryTree.init(TreeMockData.getArrayOfTen());
        this.binaryTree.reverse();
        this.binaryTree.print();
    }
}
