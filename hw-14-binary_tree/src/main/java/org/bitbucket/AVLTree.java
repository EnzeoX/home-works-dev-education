package org.bitbucket;

public class AVLTree implements ITree {

    private Node root;

    public class Node {
        Node left;
        Node right;
        private int height = 1;
        int value;

        private Node(int value) {
            this.value = value;
        }
    }

    @Override
    public void init(int[] arr) {
        for (int i : arr) {
            add(i);
        }
    }

    @Override
    public void print() {
        if (this.root == null)
            throw new NullPointerException("Tree is null");
        print(this.root);
    }

    private void print(Node node) {
        if (node == null) {
            return;
        }
        System.out.print(node.value + " ");
        print(node.left);
        print(node.right);
    }

    @Override
    public void clear() {
        this.root = null;
    }

    @Override
    public int size() {
        return size(this.root);
    }

    private int size(Node node) {
        if (node == null) {
            return 0;
        } else {
            return size(node.left) + 1 + size(node.right);
        }
    }


    @Override
    public int[] toArray() {
        return toArray(this.root, new int[size()],new Counter());
    }

    private int[] toArray(Node node, int[] arr, Counter counter) {
        if (node != null) {
            arr[counter.count] = node.value;
            counter.count++;
            toArray(node.left, arr, counter);
            toArray(node.right, arr, counter);
        }
        return arr;
    }

    @Override
    public void add(int val) {
        if(this.root == null) {
            this.root = new Node(val);
        } else {
           this.root = add(this.root, val);
        }
    }

    private Node add(Node node, int value) {
        if (node == null) {
            return new Node(value);
        }
        if (value < node.value) {
            node.left = add(node.left, value);
        } else {
            node.right = add(node.right, value);
        }
        node.height = Math.max(height(node.left), height(node.right)) + 1;
        int balance = balanceNode(node);
        if (balance > 1 && value < node.left.value)
            return rightRotate(node);
        if (balance < -1 && value > node.right.value)
            return leftRotate(node);
        if (balance > 1 && value > node.left.value) {
            node.left = leftRotate(node.left);
            return rightRotate(node);
        }
        if (balance < -1 && value < node.right.value) {
            node.right = rightRotate(node.right);
            return leftRotate(node);
        }
        return node;
    }

    private int balanceNode(Node node) {
        if (node == null)
            return 0;
        return height(node.left) - height(node.right);
    }

    private Node rightRotate(Node y) {
        Node x = y.left;
        Node T2 = x.right;

        x.right = y;
        y.left = T2;

        y.height = Math.max(height(y.left), height(y.right)) + 1;
        x.height = Math.max(height(x.left), height(x.right)) + 1;

        return x;
    }

    private Node leftRotate(Node x) {
        Node y = x.right;
        Node T2 = y.left;

        y.left = x;
        x.right = T2;

        x.height = Math.max(height(x.left), height(x.right)) + 1;
        y.height = Math.max(height(y.left), height(y.right)) + 1;

        return y;
    }

    @Override
    public void delete(int val) {
        if (this.root == null)
            throw new NullPointerException("Tree is null");
        delete(this.root, val);
    }

    private Node delete(Node node, int val) {
        if (node == null)
            return null;
        if (val < node.value) {
            node.left = delete(node.left, val);
        } else if (val > node.value) {
            node.right = delete(node.right, val);
        } else {
            if ((node.left == null) || (node.right == null)) {
                Node temp;
                if (node.left != null) {
                    temp = node.left;
                } else {
                    temp = node.right;
                }
                if (temp == null) {
                    temp = node;
                    node = null;
                } else {
                    node = temp;
                }
                temp = null;
            } else {
                Node temp = minValueNode(node.right);
                node.value = temp.value;
                node.right = delete(node.right, temp.value);
            }
        }
        if (node == null)
            return node;
        node.height = Math.max(height(node.left), height(node.right)) + 1;
        int balance = balanceNode(node);
        if (balance > 1 && balanceNode(node.left) >= 0)
            return rightRotate(root);

        if (balance > 1 && balanceNode(node.left) < 0) {
            node.left = leftRotate(node.left);
            return rightRotate(node);
        }

        if (balance < -1 && balanceNode(node.right) <= 0)
            return leftRotate(node);

        if (balance < -1 && balanceNode(node.right) > 0) {
            node.right = rightRotate(node.right);
            return leftRotate(node);
        }
        return node;
    }

    private Node minValueNode(Node node) {
        Node current = node;
        while (current.left != null)
            current = current.left;
        return current;
    }

    @Override
    public int width() {
        int maxWidth = 0;
        int width;
        int i;
        int h = height();
        for (i = 1; i <= h; i++) {
            width = width(this.root, i);
            if (width > maxWidth)
                maxWidth = width;
        }
        return maxWidth;
    }

    private int width(Node node, int level) {
        if (node == null)
            return 0;
        if (level == 1)
            return 1;
        if (level > 1)
            return width(node.left, level - 1) + width(node.right, level - 1);
        return 0;
    }

    @Override
    public int height() {
        return height(this.root);
    }

    private int height(Node node) {
        if (node == null)
            return 0;
        return node.height;
    }


    @Override
    public int nodes() {
        return nodes(this.root);
    }

    private int nodes(Node node) {
        if (node == null)
            return 0;
        int nodeQuantity = 0;
        nodeQuantity++;
        nodeQuantity += size(node.left);
        nodeQuantity += size(node.right);
        return nodeQuantity;
    }

    @Override
    public int leaves() {
        return leaves(this.root);
    }

    private int leaves(Node node) {
        if (node == null) {
            return 0;
        }
        if (node.left == null && node.right == null) {
            return 1;
        } else {
            return leaves(node.left) + leaves(node.right);
        }
    }

    @Override
    public void reverse() {
        reverse(this.root);
    }

    private void reverse(Node node) {
        if (node != null) {
            reverse(node.left);
            reverse(node.right);
            Node temp = node.left;
            node.left = node.right;
            node.right = temp;
        }
    }

    public static class Counter {
        int count = 0;
    }
}
