package org.bitbucket;

public class BSTree implements ITree {

    private Node root;

    int nodeSize;

    public class Node {
        int value;
        Node left;
        Node right;

        Node(int value) {
            this.value = value;
        }
    }

    @Override
    public void init(int[] arr) {
        if (arr.length == 0)
            throw new NullPointerException("Array is null");
        for (int i : arr) {
            add(i);
        }
    }

    @Override
    public void add(int val) {
        if (this.root == null) {
            this.root = new Node(val);
            this.nodeSize += 1;
        } else {
            this.root = add(this.root, val);
        }
    }

    private Node add(Node node, int value) {
        if (node == null) {
            node = new Node(value);
        } else {
            if (value <= node.value) {
                node.left = add(node.left, value);
            } else {
                node.right = add(node.right, value);
            }
        }
        return node;
    }

    @Override
    public void print() {
        print(this.root);
    }

    private void print(Node node) {
        if (node == null) {
            return;
        }
        System.out.print(node.value + " ");
        print(node.left);
        print(node.right);
    }

    @Override
    public void clear() {
        this.root = null;
    }

    @Override
    public int size() {
        return size(this.root);
    }

    private int size(Node node) {
        if (node == null) {
            return 0;
        } else {
            return size(node.left) + 1 + size(node.right);
        }
    }

    @Override
    public int[] toArray() {
        return toArray(this.root, new int[size()],new Counter());
    }

    private int[] toArray(Node node, int[] arr, Counter counter) {
        if (node != null) {
            arr[counter.count] = node.value;
            counter.count++;
            toArray(node.left, arr, counter);
            toArray(node.right, arr, counter);
        }
        return arr;
    }

    @Override
    public void delete(int val) {
        if (this.root == null)
            throw new NullPointerException("Tree is null");
        delete(this.root, val);
    }

    private Node delete(Node node, int value) {
        if (node == null) {
            return null;
        }
        if (value == node.value) {
            if (node.left == null && node.right == null) {
                return null;
            }
            if (node.right == null) {
                return node.left;
            }
            if (node.left == null) {
                return node.right;
            }
            int minimumVal = minValue(node.right);
            node.value = minimumVal;
            node.right = delete(node.right, minimumVal);
            return node;
        }
        if (value < node.value) {
            node.left = delete(node.left, value);
            return node;
        }
        node.right = delete(node.right, value);
        return node;
    }

    private int minValue(Node node) {
        return node.left == null ? node.value : minValue(node.left);
    }

    @Override
    public int width() {
        int maxWidth = 0;
        int width;
        int i;
        int h = height();
        for (i = 1; i <= h; i++) {
            width = width(this.root, i);
            if (width > maxWidth)
                maxWidth = width;
        }
        return maxWidth;
    }

    private int width(Node node, int level) {
        if (node == null)
            return 0;
        if (level == 1)
            return 1;
        if (level > 1)
            return width(node.left, level - 1) + width(node.right, level - 1);
        return 0;
    }

    @Override
    public int height() {
        return height(this.root);
    }

    private int height(Node node) {
        if (node == null) {
            return 0;
        }
        int left = height(node.left);
        int right = height(node.right);
        return left > right ? left + 1 : right + 1;
    }

    @Override
    public int nodes() {
        return nodes(this.root);
    }

    private int nodes(Node node) {
        if (node == null)
            return 0;
        int nodeQuantity = 0;
        nodeQuantity++;
        nodeQuantity += size(node.left);
        nodeQuantity += size(node.right);
        return nodeQuantity;
    }

    @Override
    public int leaves() {
        return leaves(this.root);
    }

    private int leaves(Node node) {
        if (node == null) {
            return 0;
        }
        if (node.left == null && node.right == null) {
            return 1;
        } else {
            return leaves(node.left) + leaves(node.right);
        }
    }

    @Override
    public void reverse() {
        reverse(this.root);
    }

    private void reverse(Node node) {
        if (node != null) {
            reverse(node.left);
            reverse(node.right);
            Node temp = node.left;
            node.left = node.right;
            node.right = temp;
        }
    }

    @Override
    public String toString() {
        return null;
    }

    public static class Counter {
        int count = 0;
    }
}
