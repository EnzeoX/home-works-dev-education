package org.bitbucket;


public interface ITree {

    void init(int[] arr);

    void print();

    void clear();

    int size();

    int[] toArray();

    void add(int val);

    void delete(int val);

    int width();

    int height();

    int nodes();

    int leaves();

    void reverse();

    String toString();
}
