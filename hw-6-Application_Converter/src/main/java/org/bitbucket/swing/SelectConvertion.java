package org.bitbucket.swing;

public class SelectConvertion {
    public static double typeSelector(String valueOfBox1, double number, String valueOfBox2, String valueOfBox3){

        double convertedNumber = 0.0;
        switch (valueOfBox3) {
            case "Temperature":
                convertedNumber = ConvertValuesToDefault.numberConvertorTemperature(valueOfBox1, number, valueOfBox2);
                break;
            case "Volume":
                convertedNumber = ConvertValuesToDefault.numberConvertorVolume(valueOfBox1, number, valueOfBox2);
                break;
            case "Weight":
                convertedNumber = ConvertValuesToDefault.numberConvertorWeight(valueOfBox1, number, valueOfBox2);
                break;
            case "Length":
                convertedNumber = ConvertValuesToDefault.numberConvertorLength(valueOfBox1, number, valueOfBox2);
                break;
            case "Time":
                convertedNumber = ConvertValuesToDefault.numberConvertorTime(valueOfBox1, number, valueOfBox2);
                break;
        }
        return convertedNumber;
    }
}
