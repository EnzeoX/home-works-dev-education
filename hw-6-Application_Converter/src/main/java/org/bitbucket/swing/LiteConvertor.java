package org.bitbucket.swing;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;

public class LiteConvertor {
    private JPanel contentPane;
    private JTextField textField1;
    private JTextField textField2;
    private JComboBox<String> comboBox1;
    private JComboBox<String> comboBox2;
    private JComboBox<String> comboBox3;
    private JButton resetButton;
    String switchCase;
    String[] stringOfLength = {"Meters", "Kilometers", "Mile", "Nautical mile", "Cable", "League", "Foot", "Yard"};
    String[] stringOfWeight = {"Gram", "Kilogram", "Carat", "Eng pound", "Pound", "Stone", "Rus pound"};
    String[] stringOfTemperature = {"Celsius", "Kelvin", "Reaumur", "Romer", "Rankine", "Newton", "Delisle", "Fahrenheit"};
    String[] stringOfVolume = {"M^3", "Liter", "Gallon", "Pint", "Quart", "Barrel", "Cubic foot", "Cubic inch"};
    String[] stringOfTime = {"Seconds", "Minutes", "Hour", "Day", "Week", "Month", "Astronomical year"};


    public LiteConvertor() {
        DocumentListener listenerBox1 = new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                double number = Double.parseDouble(textField1.getText());
                String valueOfBox1 = String.valueOf(comboBox1.getSelectedItem());
                String valueOfBox2 = String.valueOf(comboBox2.getSelectedItem());
                String valueOFBox3 = String.valueOf(comboBox3.getSelectedItem());
                textField2.setText(Double.toString(SelectConvertion.typeSelector(valueOfBox1, number, valueOfBox2, valueOFBox3)));
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                try {
                    double number = Double.parseDouble(textField1.getText());
                    String valueOfBox1 = String.valueOf(comboBox1.getSelectedItem());
                    String valueOfBox2 = String.valueOf(comboBox2.getSelectedItem());
                    String valueOFBox3 = String.valueOf(comboBox3.getSelectedItem());
                    textField2.setText(Double.toString(SelectConvertion.typeSelector(valueOfBox1, number, valueOfBox2, valueOFBox3)));
                } catch (NumberFormatException Ignore) {
                    textField2.setText(null);
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {

            }
        };

        textField1.getDocument().addDocumentListener(listenerBox1);

        comboBox3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switchCase = String.valueOf(comboBox3.getSelectedItem());
                switch (switchCase) {
                    case "Length":
                        comboBox1.removeAllItems();
                        comboBox2.removeAllItems();
                        for (int i = 0; i < stringOfLength.length; i++) {
                            comboBox1.addItem(stringOfLength[i]);
                            comboBox2.addItem(stringOfLength[i]);
                        }
                        break;
                    case "Weight":
                        comboBox1.removeAllItems();
                        comboBox2.removeAllItems();
                        for (int i = 0; i < stringOfWeight.length; i++) {
                            comboBox1.addItem(stringOfWeight[i]);
                            comboBox2.addItem(stringOfWeight[i]);
                        }
                        break;
                    case "Temperature":
                        comboBox1.removeAllItems();
                        comboBox2.removeAllItems();
                        for (int i = 0; i < stringOfTemperature.length; i++) {
                            comboBox1.addItem(stringOfTemperature[i]);
                            comboBox2.addItem(stringOfTemperature[i]);
                        }
                        break;
                    case "Volume":
                        comboBox1.removeAllItems();
                        comboBox2.removeAllItems();
                        for (int i = 0; i < stringOfVolume.length; i++) {
                            comboBox1.addItem(stringOfVolume[i]);
                            comboBox2.addItem(stringOfVolume[i]);
                        }
                        break;
                    case "Time":
                        comboBox1.removeAllItems();
                        comboBox2.removeAllItems();
                        for (int i = 0; i < stringOfTime.length; i++) {
                            comboBox1.addItem(stringOfTime[i]);
                            comboBox2.addItem(stringOfTime[i]);
                        }
                        break;
                    default:
                        textField1.setText("No text for u");
                        break;
                }
            }
        });

        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textField1.setText(null);
                textField2.setText(null);
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Calculator");
        frame.setContentPane(new LiteConvertor().contentPane);
        frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.pack();
        frame.setVisible(true);
    }
}
