package org.bitbucket.swing;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.FontUIResource;
import javax.swing.text.StyleContext;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Locale;

public class Convertor {

    private JTextField textField1;
    private JTextField textField2;
    private JPanel mainField;
    private JButton RESETButton;
    private JButton lengthButton;
    private JButton temperatureButton;
    private JButton weightButton;
    private JButton timeButton;
    private JButton volumeButton;
    private JButton button5;
    private JButton button4;
    private JButton button3;
    private JButton button2;
    private JButton button1;
    private JButton button6;
    private JButton button7;
    private JButton button8;
    private JLabel label1;
    private JLabel label2;
    String switchCase;
    String[] weight = {"kg", "g", "carat", "eng pound", "pound", "stone", "rus pound"};
    String[] time = {"sec", "min", "hour", "day", "week", "month", "year"};
    String[] temperature = {"C", "K", "F", "Re", "Ro", "Ra", "N", "D"};
    String[] length = {"m", "km", "mile", "nautrical mile", "cable", "league", "foot", "yard"};
    String[] volume = {"l", "m^3", "gallon", "pint", "quart", "barrel", "cubic foot", "cubic inch"};

    public Convertor() {
        RESETButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textField2.setText(null);
                textField1.setText(null);
            }
        });

        lengthButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                label1.setText("Заданная величина, " + length[0]);
                button1.setText("m to Km");
                button2.setText("m to Mile");
                button3.setText("m to nautical mile");
                button4.setText("m to cable");
                button5.setText("m to league");
                button6.setText("m to foot");
                button7.setText("m to yard");
                button1.setVisible(true);
                button2.setVisible(true);
                button3.setVisible(true);
                button4.setVisible(true);
                button5.setVisible(true);
                button6.setVisible(true);
                button7.setVisible(true);
                switchCase = "length";
            }
        });

        temperatureButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                label1.setText("Заданная величина, " + temperature[0]);
                button1.setText("C to K");
                button2.setText("C to F");
                button3.setText("C to Re");
                button4.setText("C to Ro");
                button5.setText("C to Ra");
                button6.setText("C to N");
                button7.setText("C to D");
                button1.setVisible(true);
                button2.setVisible(true);
                button3.setVisible(true);
                button4.setVisible(true);
                button5.setVisible(true);
                button6.setVisible(true);
                button7.setVisible(true);
                switchCase = "temperature";
            }
        });

        weightButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                label1.setText("Заданная величина, " + weight[0]);
                button1.setText("kg to g");
                button2.setText("kg to carat");
                button3.setText("kg to eng pound");
                button4.setText("kg to stone");
                button5.setText("kg to rus pound");
                button6.setText("kg to pound");
                button1.setVisible(true);
                button2.setVisible(true);
                button3.setVisible(true);
                button4.setVisible(true);
                button5.setVisible(true);
                button6.setVisible(true);
                button7.setVisible(false);
                switchCase = "weight";
            }
        });

        timeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                label1.setText("Заданная величина, " + time[0]);
                button1.setText("sec to min");
                button2.setText("sec to hour");
                button3.setText("sec to day");
                button4.setText("sec to week");
                button5.setText("sec to month");
                button6.setText("sec to year");
                button1.setVisible(true);
                button2.setVisible(true);
                button3.setVisible(true);
                button4.setVisible(true);
                button5.setVisible(true);
                button6.setVisible(true);
                button7.setVisible(false);
                switchCase = "time";
            }
        });

        button8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showInternalMessageDialog(null, "- Кнопками \"Weight\", \"Length\", \"Time\", \"Temperature\", \"Volume\"\nвыбирается режим конвертирования"
                        + "\n- Кнопками, к примеру \" kg to carat\", заданная величина конвертируется из\nкиллограмов в караты"
                        + "\n- Конпка \"RESET\" делает отчистку в полях ввода");
            }
        });
        volumeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                label1.setText("Заданная величина, " + volume[0]);
                button1.setText("l to m^3");
                button2.setText("l to gallon");
                button3.setText("l to pint");
                button4.setText("l to quart");
                button5.setText("l to barrel");
                button6.setText("l to cubic foot");
                button7.setText("l to cubic inch");
                button1.setVisible(true);
                button2.setVisible(true);
                button3.setVisible(true);
                button4.setVisible(true);
                button5.setVisible(true);
                button6.setVisible(true);
                button7.setVisible(true);
                switchCase = "volume";
            }
        });
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double number = Double.parseDouble(textField1.getText());
                switch (switchCase) {
                    case "volume":
                        number = number / 1000;
                        label2.setText("Конвертированная, " + volume[1]);
                        break;
                    case "length":
                        number = number / 1000;
                        label2.setText("Конвертированная, " + length[1]);
                        break;
                    case "weight":
                        number = number * 1000;
                        label2.setText("Конвертированная, " + weight[1]);
                        break;
                    case "time":
                        number = number / 60;
                        label2.setText("Конвертированная, " + time[1]);
                        break;
                    case "temperature":
                        label2.setText("Конвертированная, " + temperature[1]);
                        number = number + 273.15;
                        break;
                    default:
                        break;
                }
                textField2.setText(Double.toString(Math.floor(number * 100000000) / 100000000));
            }
        });

        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double number = Double.parseDouble(textField1.getText());
                switch (switchCase) {
                    case "volume":
                        number = number / 3.785;
                        label2.setText("Конвертированная, " + volume[2]);
                        break;
                    case "length":
                        number = number / 1609;
                        label2.setText("Конвертированная, " + length[2]);
                        break;
                    case "weight":
                        number = number * 5000;
                        label2.setText("Конвертированная, " + weight[2]);
                        break;
                    case "time":
                        number = number / 3600;
                        label2.setText("Конвертированная, " + time[2]);
                        break;
                    case "temperature":
                        label2.setText("Конвертированная, " + temperature[2]);
                        number = number * 1.8 + 32;
                        break;
                    default:
                        break;
                }
                textField2.setText(Double.toString(Math.floor(number * 100000000) / 100000000));
            }
        });
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double number = Double.parseDouble(textField1.getText());
                switch (switchCase) {
                    case "volume":
                        number = number * 2.133;
                        label2.setText("Конвертированная, " + volume[3]);
                        break;
                    case "length":
                        number = number / 1852;
                        label2.setText("Конвертированная, " + length[3]);
                        break;
                    case "weight":
                        number = number * 2.205;
                        label2.setText("Конвертированная, " + weight[3]);
                        break;
                    case "time":
                        number = number / 86400;
                        label2.setText("Конвертированная, " + time[3]);
                        break;
                    case "temperature":
                        label2.setText("Конвертированная, " + temperature[3]);
                        number = number * 0.8;
                        break;
                    default:
                        break;
                }
                textField2.setText(Double.toString(Math.floor(number * 100000000) / 100000000));
            }
        });
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double number = Double.parseDouble(textField1.getText());
                switch (switchCase) {
                    case "volume":
                        number = number * 1.057;
                        label2.setText("Конвертированная, " + volume[4]);
                        break;
                    case "length":
                        number = number / 185.3184;
                        label2.setText("Конвертированная, " + length[4]);
                        break;
                    case "weight":
                        number = number / 6.35;
                        label2.setText("Конвертированная, " + weight[4]);
                        break;
                    case "time":
                        number = number / 604800;
                        label2.setText("Конвертированная, " + time[4]);
                        break;
                    case "temperature":
                        label2.setText("Конвертированная, " + temperature[4]);
                        number = number * 0.525 + 7.5;
                        break;
                    default:
                        break;
                }
                textField2.setText(Double.toString(Math.floor(number * 100000000) / 100000000));
            }
        });
        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double number = Double.parseDouble(textField1.getText());
                switch (switchCase) {
                    case "volume":
                        number = number / 117;
                        label2.setText("Конвертированная, " + volume[5]);
                        break;
                    case "length":
                        number = number / 5556;
                        label2.setText("Конвертированная, " + length[5]);
                        break;
                    case "weight":
                        number = number / 0.45359237;
                        label2.setText("Конвертированная, " + weight[5]);
                        break;
                    case "time":
                        number = number / (2.628 * Math.pow(10, 6));
                        label2.setText("Конвертированная, " + time[5]);
                        break;
                    case "temperature":
                        label2.setText("Конвертированная, " + temperature[5]);
                        number = number * 1.8 + 491.67;
                        break;
                    default:
                        break;
                }
                textField2.setText(Double.toString(Math.floor(number * 100000000) / 100000000));
            }
        });
        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double number = Double.parseDouble(textField1.getText());
                switch (switchCase) {
                    case "volume":
                        number = number / 28.317;
                        label2.setText("Конвертированная, " + volume[6]);
                        break;
                    case "length":
                        number = number * 3.218;
                        label2.setText("Конвертированная, " + length[6]);
                        break;
                    case "weight":
                        number = number * 2.205;
                        label2.setText("Конвертированная, " + weight[6]);
                        break;
                    case "time":
                        number = number / (3.154 * Math.pow(10, 7));
                        label2.setText("Конвертированная, " + time[6]);
                        break;
                    case "temperature":
                        label2.setText("Конвертированная, " + temperature[6]);
                        number = number * 0.33;
                        break;
                    default:
                        break;
                }
                textField2.setText(Double.toString(Math.floor(number * 100000000) / 100000000));
            }
        });
        button7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double number = Double.parseDouble(textField1.getText());
                switch (switchCase) {
                    case "volume":
                        number = number * 61.024;
                        label2.setText("Конвертированная, " + volume[7]);
                        break;
                    case "length":
                        number = number * 1.094;
                        label2.setText("Конвертированная, " + length[7]);
                        break;
                    case "temperature":
                        label2.setText("Конвертированная, " + temperature[7]);
                        number = (100 - number) * 1.5;
                        break;
                    default:
                        break;
                }
                textField2.setText(Double.toString(Math.floor(number * 100000000) / 100000000));
            }
        });
    }

    public static void main(String[] args) throws IOException {
        JFrame frame = new JFrame("Convertor");
        frame.setContentPane(new Convertor().mainField);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.pack();
        frame.setVisible(true);
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        mainField = new JPanel();
        mainField.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(5, 6, new Insets(0, 0, 0, 0), 1, 1));
        mainField.setAlignmentX(0.0f);
        mainField.setAlignmentY(0.0f);
        mainField.setBackground(new Color(-3620423));
        Font mainFieldFont = this.$$$getFont$$$("Charlemagne Std", Font.ITALIC, 12, mainField.getFont());
        if (mainFieldFont != null) mainField.setFont(mainFieldFont);
        mainField.setForeground(new Color(-4521979));
        mainField.setMaximumSize(new Dimension(720, 160));
        mainField.setMinimumSize(new Dimension(720, 160));
        mainField.setOpaque(true);
        mainField.setPreferredSize(new Dimension(720, 160));
        mainField.setBorder(BorderFactory.createTitledBorder(BorderFactory.createRaisedBevelBorder(), null, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, null));
        textField2 = new JTextField();
        mainField.add(textField2, new com.intellij.uiDesigner.core.GridConstraints(0, 5, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(110, 30), new Dimension(110, 30), new Dimension(110, 30), 0, false));
        label2 = new JLabel();
        label2.setForeground(new Color(-16777216));
        label2.setHorizontalAlignment(4);
        label2.setHorizontalTextPosition(4);
        label2.setText("Конвертированная");
        mainField.add(label2, new com.intellij.uiDesigner.core.GridConstraints(0, 4, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_VERTICAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(150, 30), new Dimension(150, 30), new Dimension(150, 30), 0, false));
        label1 = new JLabel();
        label1.setForeground(new Color(-16777216));
        label1.setHorizontalAlignment(4);
        label1.setHorizontalTextPosition(4);
        label1.setText("Заданная величина");
        mainField.add(label1, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(150, 30), new Dimension(150, 30), new Dimension(150, 30), 0, false));
        RESETButton = new JButton();
        RESETButton.setBackground(new Color(-458747));
        RESETButton.setBorderPainted(true);
        RESETButton.setForeground(new Color(-16777216));
        RESETButton.setOpaque(true);
        RESETButton.setSelected(false);
        RESETButton.setText("RESET");
        RESETButton.setToolTipText("");
        mainField.add(RESETButton, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_NORTHWEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(110, 30), new Dimension(110, 30), new Dimension(110, 30), 0, false));
        lengthButton = new JButton();
        lengthButton.setText("Length");
        mainField.add(lengthButton, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(110, 30), new Dimension(110, 30), new Dimension(110, 30), 0, false));
        temperatureButton = new JButton();
        temperatureButton.setText("Temperature");
        mainField.add(temperatureButton, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(110, 30), new Dimension(110, 30), new Dimension(110, 30), 0, false));
        weightButton = new JButton();
        weightButton.setText("Weight");
        mainField.add(weightButton, new com.intellij.uiDesigner.core.GridConstraints(1, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(110, 30), new Dimension(110, 30), new Dimension(110, 30), 0, false));
        timeButton = new JButton();
        timeButton.setText("Time");
        mainField.add(timeButton, new com.intellij.uiDesigner.core.GridConstraints(1, 4, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(110, 30), new Dimension(110, 30), new Dimension(110, 30), 0, false));
        volumeButton = new JButton();
        volumeButton.setText("Volume");
        mainField.add(volumeButton, new com.intellij.uiDesigner.core.GridConstraints(1, 5, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(110, 30), new Dimension(110, 30), new Dimension(110, 30), 0, false));
        button5 = new JButton();
        button5.setText("Button");
        button5.setVisible(false);
        mainField.add(button5, new com.intellij.uiDesigner.core.GridConstraints(3, 5, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(110, 30), new Dimension(110, 30), new Dimension(110, 30), 0, false));
        button4 = new JButton();
        button4.setText("Button");
        button4.setVisible(false);
        mainField.add(button4, new com.intellij.uiDesigner.core.GridConstraints(3, 4, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(110, 30), new Dimension(110, 30), new Dimension(110, 30), 0, false));
        button3 = new JButton();
        button3.setText("Button");
        button3.setVisible(false);
        mainField.add(button3, new com.intellij.uiDesigner.core.GridConstraints(3, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(110, 30), new Dimension(110, 30), new Dimension(110, 30), 0, false));
        button2 = new JButton();
        button2.setText("Button");
        button2.setVisible(false);
        mainField.add(button2, new com.intellij.uiDesigner.core.GridConstraints(3, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(110, 30), new Dimension(110, 30), new Dimension(110, 30), 0, false));
        button1 = new JButton();
        button1.setText("Button");
        button1.setVisible(false);
        mainField.add(button1, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(110, 30), new Dimension(110, 30), new Dimension(110, 30), 0, false));
        button7 = new JButton();
        button7.setText("Button");
        button7.setVisible(false);
        mainField.add(button7, new com.intellij.uiDesigner.core.GridConstraints(4, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(110, 30), new Dimension(110, 30), new Dimension(110, 30), 0, false));
        button8 = new JButton();
        button8.setText("Help me pls :(");
        mainField.add(button8, new com.intellij.uiDesigner.core.GridConstraints(4, 5, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(110, 30), new Dimension(110, 30), new Dimension(110, 30), 0, false));
        button6 = new JButton();
        button6.setText("Button");
        button6.setVisible(false);
        mainField.add(button6, new com.intellij.uiDesigner.core.GridConstraints(4, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(110, 30), new Dimension(110, 30), new Dimension(110, 30), 0, false));
        textField1 = new JTextField();
        mainField.add(textField1, new com.intellij.uiDesigner.core.GridConstraints(0, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(110, 30), new Dimension(110, 30), new Dimension(110, 30), 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer1 = new com.intellij.uiDesigner.core.Spacer();
        mainField.add(spacer1, new com.intellij.uiDesigner.core.GridConstraints(2, 5, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, 1, new Dimension(110, -1), new Dimension(110, -1), new Dimension(110, -1), 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer2 = new com.intellij.uiDesigner.core.Spacer();
        mainField.add(spacer2, new com.intellij.uiDesigner.core.GridConstraints(2, 4, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, 1, new Dimension(110, 30), new Dimension(110, 30), new Dimension(110, 30), 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer3 = new com.intellij.uiDesigner.core.Spacer();
        mainField.add(spacer3, new com.intellij.uiDesigner.core.GridConstraints(2, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, 1, new Dimension(110, -1), new Dimension(110, -1), new Dimension(110, -1), 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer4 = new com.intellij.uiDesigner.core.Spacer();
        mainField.add(spacer4, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, 1, new Dimension(110, -1), new Dimension(110, -1), new Dimension(110, -1), 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer5 = new com.intellij.uiDesigner.core.Spacer();
        mainField.add(spacer5, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, 1, new Dimension(110, -1), new Dimension(110, -1), new Dimension(110, -1), 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer6 = new com.intellij.uiDesigner.core.Spacer();
        mainField.add(spacer6, new com.intellij.uiDesigner.core.GridConstraints(4, 4, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_VERTICAL, 1, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, new Dimension(-1, 30), new Dimension(-1, 30), new Dimension(-1, 30), 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer7 = new com.intellij.uiDesigner.core.Spacer();
        mainField.add(spacer7, new com.intellij.uiDesigner.core.GridConstraints(3, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_VERTICAL, 1, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, new Dimension(-1, 30), new Dimension(-1, 30), new Dimension(-1, 30), 0, false));
        label2.setLabelFor(textField1);
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        Font font = new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
        boolean isMac = System.getProperty("os.name", "").toLowerCase(Locale.ENGLISH).startsWith("mac");
        Font fontWithFallback = isMac ? new Font(font.getFamily(), font.getStyle(), font.getSize()) : new StyleContext().getFont(font.getFamily(), font.getStyle(), font.getSize());
        return fontWithFallback instanceof FontUIResource ? fontWithFallback : new FontUIResource(fontWithFallback);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainField;
    }

}
