package org.bitbucket.swing;

public class FromDefaultToSelectedItem {
    // Конвертирование дефолтного значения в заданное(температура)
    public static double convertToValueTemperature(String valueOfBox2, double fromValueToDefault) {

        double convertedNum = 0.0;
        switch (valueOfBox2) {
            case "Celsius":
                convertedNum = fromValueToDefault;
                break;
            case "Kelvin":
                convertedNum = fromValueToDefault + 273.15;
                break;
            case "Fahrenheit":
                convertedNum = fromValueToDefault * 1.8 + 32;
                break;
            case "Reaumur":
                convertedNum = fromValueToDefault * 0.8;
                break;
            case "Romer":
                convertedNum = fromValueToDefault * 0.525 + 7.5;
                break;
            case "Rankine":
                convertedNum = fromValueToDefault * 1.8 + 32 + 459.67;
                break;
            case "Newton":
                convertedNum = fromValueToDefault * 0.33;
                break;
            case "Delisle":
                convertedNum = (100 - fromValueToDefault) * 1.5;
                break;
            default:
                break;
        }
        return convertedNum;
    }
    // Конвертирование дефолтного значения в заданное(длинна)
    public static double convertToValueLength(String valueOfBox2, double fromValueToDefault) {
        double convertedNum = 0.0;
        switch (valueOfBox2) {
            case "Meters":
                convertedNum = fromValueToDefault;
                break;
            case "Kilometers":
                convertedNum = fromValueToDefault / 1000;
                break;
            case "Mile":
                convertedNum = fromValueToDefault / 1609;
                break;
            case "Nautical mile":
                convertedNum = fromValueToDefault / 1852;
                break;
            case "Cable":
                convertedNum = fromValueToDefault / 185.2;
                break;
            case "League":
                convertedNum = fromValueToDefault / 5556;
                break;
            case "Foot":
                convertedNum = fromValueToDefault * 3.28084;
                break;
            case "Yard":
                convertedNum = fromValueToDefault * 1.094;
                break;
            default:
                break;
        }
        return convertedNum;
    }

    // Конвертирование дефолтного значения в заданное(Обьем)
    public static double convertToValueVolume(String valueOfBox2, double fromValueToDefault) {

        double convertedNum = 0.0;
        switch (valueOfBox2) {
            case "Liter":
                convertedNum = fromValueToDefault;
                break;
            case "M^3":
                convertedNum = fromValueToDefault / 1000;
                break;
            case "Gallon":
                convertedNum = fromValueToDefault / 3.785;
                break;
            case "Pint":
                convertedNum = fromValueToDefault * 2.1133764189;
                break;
            case "Quart":
                convertedNum = fromValueToDefault * 1.057;
                break;
            case "Barrel":
                convertedNum = fromValueToDefault / 117;
                break;
            case "Cubic foot":
                convertedNum = fromValueToDefault / 28.317;
                break;
            case "Cubic inch":
                convertedNum = fromValueToDefault * 61.024;
                break;
            default:
                break;
        }
        return convertedNum;
    }

    // Конвертирование дефолтного значения в заданное(Вес)
    public static double convertToValueWeight(String valueOfBox2, double fromValueToDefault) {

        double convertedNum = 0.0;
        switch (valueOfBox2) {
            case "Gram":
                convertedNum = fromValueToDefault;
                break;
            case "Kilogram":
                convertedNum = fromValueToDefault / 1000;
                break;
            case "Carat":
                convertedNum = fromValueToDefault * 5;
                break;
            case "Eng pound":
            case "Pound":
                convertedNum = fromValueToDefault / 453.5923;
                break;
            case "Stone":
                convertedNum = fromValueToDefault / 6350;
                break;
            case "Rus pound":
                convertedNum = fromValueToDefault / 409.5171;
                break;
            default:
                break;
        }
        return convertedNum;
    }

    // Конвертирование дефолтного значения в заданное(Время)
    public static double convertToValueTime(String valueOfBox2, double fromValueToDefault) {

        double convertedNum = 0.0;
        switch (valueOfBox2) {
            case "Seconds":
                convertedNum = fromValueToDefault;
                break;
            case "Minutes":
                convertedNum = fromValueToDefault / 60;
                break;
            case "Hour":
                convertedNum = fromValueToDefault / 3600;
                break;
            case "Day":
                convertedNum = fromValueToDefault / 86400;
                break;
            case "Week":
                convertedNum = fromValueToDefault / 604800;
                break;
            case "Month":
                convertedNum = fromValueToDefault / (2.628 * Math.pow(10, 6));
                break;
            case "Astronomical year":
                convertedNum = fromValueToDefault / (3.154 * Math.pow(10, 7));
                break;
            default:
                break;
        }
        return convertedNum;
    }
}
