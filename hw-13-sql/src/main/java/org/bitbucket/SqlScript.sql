SELECT COUNT(id) FROM Person_list;

SELECT AVG age FROM Person_list;

SELECT DISTINCT LastName FROM Person_list ORDER BY LastName ASC;

SELECT LastName, COUNT(*) FROM Person_list GROUP BY LastName HAVING COUNT(*)>1;

SELECT * FROM People_list WHERE LastName LIKE '%б%';

SELECT * FROM person WHERE id_street IS NULL

SELECT Person_list.first_name, Person_list.last_name, Person_list.age, Street_list.name
FROM Person_list LEFT JOIN Street_list
ON Person_list.id_street = Street_list.id
WHERE age < 18 AND Street_list.name = 'проспект Правды';

SELECT DISTINCT name, COUNT(*) FROM Street_list
INNER JOIN Person_list ON Person_list.id = Person_list.id_street
GROUP BY name HAVING COUNT(*)>0 ORDER BY name ASC;

SELECT * FROM Street_list WHERE CHAR_LENGTH(name) = 6;

SELECT DISTINCT name, COUNT(*) FROM Street_list
INNER JOIN Person_list ON Street_list.id = Person_list.id_street
GROUP BY name HAVING COUNT(*)>3 ORDER BY name ASC;