package org.bitbucket.handler;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ByteHandler {

    public static byte[] decode(byte[] arrByte) {
        boolean mask = (arrByte[1] & 0b10000000) != 0;
        int stringLength = arrByte[1] + 0b10000000;
        int offset;
        if(stringLength < 126) {
            offset = 2;
        } else {
            stringLength = ByteHandler.byteDecoder(arrByte[3], arrByte[2]);
            offset = 4;
        }

        if (stringLength == 0) {
            throw new NullPointerException("Message is null");
        } else if (mask) {
            byte[] decoded = new byte[stringLength];
            byte[] masks = new byte[]{arrByte[offset], arrByte[offset + 1], arrByte[offset + 2], arrByte[offset + 3]};
            offset += 4;

            for (int i = 0; i < stringLength; ++i) {
                decoded[i] = (byte) (arrByte[offset + i] ^ masks[i % 4]);
            }
            return decoded;
        } else {
            throw new NullPointerException("Mask is null");
        }
    }

    public static byte[] encode(String message) {
        byte[] result;
        int offset = 2;
        if (message.length() < 126) {
            result = new byte[message.length() + offset];
            result[1] = (byte) message.length();
        } else {
            offset = 4;
            result = new byte[message.length() + offset];
            result[1] = (byte) 126;
            result[2] = (byte)((message.length() >> 8) & 0xff);
            result[3] = (byte)(message.length() & 0xff);
        }
        result[0] = (byte) -127;
        for (int i = 0; i < message.length(); i++) {
            result[i + offset] = (byte) message.charAt(i);
        }
        return result;
    }

    public static short byteDecoder(byte first, byte second) {
        ByteBuffer bb = ByteBuffer.allocate(2);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        bb.put(first);
        bb.put(second);
        return bb.getShort(0);
    }
}
