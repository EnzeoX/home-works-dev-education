package org.bitbucket;

public class RunServer {

    public static void main(String[] args) {
        CustomWebSocket webSocket = new CustomWebSocket();
        webSocket.serverRun();
    }
}
