package org.bitbucket;

import java.util.Random;
import java.util.Scanner;

public class GuessNumber {
    public static void main(String[] args) {
        boolean start = true;
        int min = 1;
        int max = 100;
        int attempts = 5;
        while (start) {
            System.out.println("Привет, я загадал число от " + min + " до " + max + ". Попробуй угадать его за " + attempts + " попыток!”");
            start = false;
        }
        newGame(min, max, attempts);
    }

    static void newGame(int firstNum, int secondNum, int attempts) {
        String yesNo;
        System.out.println("Желаешь изменить диапазон отгадывания и количество попыток?\n Y - Да, меняем правила!\n N - Нет, играем так!");
        yesNo = UserStringInput();
        if (yesNo.equals("Y") || yesNo.equals("y")) {
            System.out.println("Введи первое число (от 1 до 200)");
            firstNum = addNums();
            System.out.println("Введи второе число (от 1 до 200)");
            secondNum = addNums();
            System.out.println("Введи количество попыток (от 1 до 15)");
            attempts = addAttempts();
        } else if (yesNo.equals("N") || yesNo.equals("n")) {
            System.out.println("Ну начнем!");
        }
        StartGame(firstNum, secondNum,attempts);
    }

    static void StartGame(int firstNum, int secondNum, int attempts){
        int attemptNum = 0;
        int userInpInt = 0;
        boolean continueGame = true;
        String out;
        int randNum = generateNumber(firstNum, secondNum);
        System.out.println(randNum + " - Вывод рандомного числа для легкой проверки программы");
        System.out.println("Случайное число сгенерировано, можно угадывать.\nЧто бы выйти, введи \"exit\"");
        while (continueGame) {
            attemptNum += 1;
            out = UserStringInput();
            if(out.equals("exit")){
                System.out.println("Всего хорошего!");
                System.exit(0);
            } else {
                try {
                    userInpInt = Integer.parseInt(out);
                } catch (NumberFormatException e) {
                    System.out.println("НЕВЕРНАЯ КОМАНДА!\nВведите либо число либо \"exit\"");
                    userInpInt = 0;
                    StartGame(firstNum, secondNum, attempts);
                }
            }
            howClose(userInpInt, randNum, attemptNum, attempts);
            if (userInpInt == randNum && attemptNum <= attempts) {
                playOrStop(firstNum, secondNum, attempts);
            } else if (userInpInt != randNum && attemptNum == attempts) {
                playOrStop(firstNum, secondNum, attempts);
            }
        }
    }

    static void howClose(int userInpNum, int randomNum, int attemptNum, int attempts) {
        if (attemptNum < attempts) {
            if (Math.abs((randomNum - userInpNum)) > 30) {
                System.out.println("Очень холодно, попробуй еще. Оставшееся количество попыток на отгадку - " + (attempts - attemptNum));
            } else if (Math.abs((randomNum - userInpNum)) < 30 && Math.abs((randomNum - userInpNum)) > 20) {
                System.out.println("Холодно, пробуй еще. Оставшееся количество попыток на отгадку - " + (attempts - attemptNum));
            } else if (Math.abs((randomNum - userInpNum)) <= 20 && Math.abs((randomNum - userInpNum)) > 10) {
                System.out.println("Тепло, но ты как пенсионер требуешь больше тепла, пробуй еще! Оставшееся количество попыток на отгадку - " + (attempts - attemptNum));
            } else if (Math.abs((randomNum - userInpNum)) <= 10 && Math.abs((randomNum - userInpNum)) > 5) {
                System.out.println("Жарко, цель близко! Оставшееся количество попыток на отгадку - " + (attempts - attemptNum));
            } else if (Math.abs((randomNum - userInpNum)) <= 5 && Math.abs((randomNum - userInpNum)) >= 1) {
                System.out.println("Цель максимально близко! Оставшееся количество попыток на отгадку - " + (attempts - attemptNum));
            } else if (userInpNum == randomNum) {
                System.out.println("Это именно то число, отлично! Количество затраченых попыток на отгадку - " + attemptNum);
            }
        } else if(attemptNum == attempts){
            if(userInpNum == randomNum){
                System.out.println("Это именно то число, отлично! Количество затраченых попыток на отгадку - " + attemptNum);
            } else {
                System.out.println("Попыток на отгадывание больше не осталось.");
            }
        }
    }

    static int generateNumber(int a, int b) {
        Random r = new Random();
        int result = 0;
        if (a > b) {
            result = r.nextInt(a - b) + b;
        } else if (b > a) {
            result = r.nextInt(b - a) + a;
        }
        return result;
    }

    static int addNums() {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int numForWrong1 = 0;
        if (a <= 0 || a > 200) {
            wrongNum(numForWrong1);
            addNums();
        }
        return a;
    }

    static int addAttempts() {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int numForWrong0 = 1;
        if (a <= 0 || a > 15) {
            wrongNum(numForWrong0);
            addAttempts();
        }
        return a;
    }

    static void wrongNum(int a) {
        if (a == 1) {
            System.out.println("Число должно быть целым и в пределах от 1 до 15!\nВведите число заново.");
        } else {
            System.out.println("Число должно быть целым и в пределах от 1 до 200!\nВведите число заново.");
        }
    }

    public static String UserStringInput() {
        Scanner str = new Scanner(System.in);
        return str.nextLine();
    }


    static void playOrStop(int firstNum, int secondNum, int attempts) {
        System.out.println("Сыграем еще раз?\nY - Да\nN - Нет");
        String word = UserStringInput();
        if (word.equals("Y") || word.equals("y")) {
            newGame(firstNum, secondNum, attempts);
        } else if (word.equals("N") || word.equals("n")) {
            System.out.println("Всего хорошего!");
            System.exit(0);
        } else {
            System.out.println("Неверная команда!");
            playOrStop(firstNum, secondNum, attempts);
        }

    }
}

