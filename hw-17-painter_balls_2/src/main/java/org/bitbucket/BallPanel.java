package org.bitbucket;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

public class BallPanel extends JPanel {

    int threadCount = 0;

    public static List<BallNext> getBalls() {
        return balls;
    }

    public static List<BallNext> balls = new ArrayList<>();

    public BallPanel(BallFrame ballFrame) {
        setLayout(null);
        setVisible(true);
        setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
                BallNext ballNext = new BallNext(e.getPoint(), ballFrame.ballPanel);
                add(ballNext);
                Thread thread = new Thread(ballNext);
                thread.start();
                threadCount++;
                balls.add(ballNext);
                ballFrame.setText("Ball count: " + threadCount);
            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                ballFrame.coordinates.setText("x - " + e.getPoint().x + ",y - " + e.getPoint().y);
            }
        });
    }
}
