package org.bitbucket;

import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {
        CustomHandler handler = new CustomHandler(8080);
        Thread thread = new Thread(handler, "TestServer1");
        thread.start();
        System.out.println(thread.getName() + " is running");
        try {
            TimeUnit.SECONDS.sleep(5);
            handler.serverClose();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
