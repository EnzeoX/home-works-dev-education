package org.bitbucket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class CustomHandler implements Runnable {

    private final int port;

    private ServerSocket serverSocket;

    private boolean serverActive = true;

    public void serverClose() {
        this.serverActive = false;
    }

    public CustomHandler(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        try {
            this.serverSocket = new ServerSocket(this.port);
            this.serverSocket.setSoTimeout(10000);
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (this.serverActive) {
            try {
                Socket socket = serverSocket.accept();
                SimpleHttpResponse response = new SimpleHttpResponse();
                SimpleHttpRequest request = new SimpleHttpRequest();
                request.readRequest(socket);
                ActionSelector.selectAction(request, response);
                response.sendResponse(socket);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void doGet(SimpleHttpRequest request, SimpleHttpResponse response) {
        String stringMsg = "<h1>GET method</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setCode(200);
        response.setStatusText("Ok");
        response.putHeaders("Content-Type", "text/html");
        response.putHeaders("Content-Length", "" + stringMsg.length());
        response.setBody(stringMsg);
    }

    public static void doPut(SimpleHttpRequest request, SimpleHttpResponse response) {
        String stringMsg = "<h1>PUT method</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setCode(200);
        response.setStatusText("Ok");
        response.putHeaders("Content-Type", "text/html");
        response.putHeaders("Content-Length", "" + stringMsg.length());
        response.setBody(stringMsg);
    }

    public static void doPost(SimpleHttpRequest request, SimpleHttpResponse response) {
        String stringMsg = "<h1>POST method</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setCode(200);
        response.setStatusText("Ok");
        response.putHeaders("Content-Type", "text/html");
        response.putHeaders("Content-Length", "" + stringMsg.length());
        response.setBody(stringMsg);
    }

    public static void doConnect(SimpleHttpRequest request, SimpleHttpResponse response) {
        String stringMsg = "<h1>CONNECT method</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setCode(200);
        response.setStatusText("Ok");
        response.putHeaders("Content-Type", "text/html");
        response.putHeaders("Content-Length", "" + stringMsg.length());
        response.setBody(stringMsg);
    }

    public static void doDelete(SimpleHttpRequest request, SimpleHttpResponse response) {
        String stringMsg = "<h1>DELETE method</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setCode(200);
        response.setStatusText("Ok");
        response.putHeaders("Content-Type", "text/html");
        response.putHeaders("Content-Length", "" + stringMsg.length());
        response.setBody(stringMsg);
    }

}
