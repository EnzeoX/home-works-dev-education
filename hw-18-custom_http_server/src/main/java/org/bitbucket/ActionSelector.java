package org.bitbucket;

public class ActionSelector {

    public static void selectAction(SimpleHttpRequest request, SimpleHttpResponse response) {
        switch(request.getMethod()) {
            case "GET":
                CustomHandler.doGet(request, response);
                break;
            case "PUT":
                CustomHandler.doPut(request, response);
                break;
            case "POST":
                CustomHandler.doPost(request, response);
                break;
            case "CONNECT":
                CustomHandler.doConnect(request, response);
                break;
            case "DELETE":
                CustomHandler.doDelete(request, response);
                break;
            default:
                throw new IllegalArgumentException(request.getMethod() + "unavailable");
        }
    }
}
