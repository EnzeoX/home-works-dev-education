package org.bitbucket;
import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TestClass {
    JFrame frame;
    TestClass(){
        frame = new JFrame();
        frame.setResizable(false);
        frame.setSize(490, 490);

        JButton button = new JButton("Жмякни");
        button.setBorder(BorderFactory.createEmptyBorder());
        button.setContentAreaFilled(false);
        button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/bitbucket/icon/yodanormal.png")));
        button.setBounds(0,0,490, 490);


        frame.add(button);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        button.addMouseListener(new MouseAdapter() {

            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/bitbucket/icon/yodapunched.png")));
            }
        });
        button.addMouseListener(new MouseAdapter() {

            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/bitbucket/icon/yodanormal.png")));
            }
        });
    }

    public static void main(String[] args) {
        new TestClass();
    }
}
