import org.bitbucket.ArrayList;
import org.bitbucket.IList;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArrayTest {

    private final IList list = new ArrayList();

    //********************* Clear test ******************************

    @Test
    public void clearArrayMany() {
        this.list.init(MockData.getArrayContainsMany());
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearArrayOne() {
        this.list.init(MockData.getArrayContainsOne());
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearArrayTwo() {
        this.list.init(MockData.getArrayContainsTwo());
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearArrayZero() {
        this.list.init(MockData.getArrayContainsZero());
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearArrayNull() {
        this.list.init(MockData.getArrayContainsNull());
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //******************* Array size test *************************

    @Test
    public void sizeArrayMany() {
        this.list.init(MockData.getArrayContainsMany());
        int act = this.list.size();
        int exp = 10;
        assertEquals(exp, act);
    }

    @Test
    public void sizeArrayOne() {
        this.list.init(MockData.getArrayContainsOne());
        int act = this.list.size();
        int exp = 1;
        assertEquals(exp, act);
    }

    @Test
    public void sizeArrayTwo() {
        this.list.init(MockData.getArrayContainsTwo());
        int act = this.list.size();
        int exp = 2;
        assertEquals(exp, act);
    }

    @Test
    public void sizeArrayDefault() {
        int act = this.list.size();
        int exp = 0;
        assertEquals(exp, act);
    }

    @Test
    public void sizeArrayZero() {
        this.list.init(MockData.getArrayContainsZero());
        int act = this.list.size();
        int exp = 0;
        assertEquals(exp, act);
    }

    @Test
    public void sizeArrayNull() {
        this.list.init(MockData.getArrayContainsNull());
        int exp = 0;
        int act = this.list.size();
    }

    //************** Add element to start *************************

    @Test
    public void addToStartOne() {
        this.list.init(MockData.getArrayContainsTwo());
        this.list.addStart(33);
        int[] exp = {33, 54, 98};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addToStartTwo() {
        this.list.init(MockData.getArrayContainsTwo());
        this.list.addStart(33);
        this.list.addStart(49);
        int[] exp = {49, 33, 54, 98};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addToStartMany() {
        this.list.init(MockData.getArrayContainsTwo());
        for (int i = 0; i < MockData.elementsToAdd.length; i++) {
            this.list.addStart(MockData.elementsToAdd[i]);
        }
        int[] exp = {10, 1, 6, 4, 54, 98};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //************** Add elements to end **************************

    @Test
    public void addToEndOne() {
        this.list.init(MockData.getArrayContainsTwo());
        this.list.addEnd(44);
        int[] exp = {54, 98, 44};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addToEndTwo() {
        this.list.init(MockData.getArrayContainsTwo());
        this.list.addEnd(44);
        this.list.addEnd(90);
        int[] exp = {54, 98, 44, 90};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addToEndMany() {
        this.list.init(MockData.getArrayContainsTwo());
        for (int i = 0; i < MockData.elementsToAdd.length; i++) {
            this.list.addEnd(MockData.elementsToAdd[i]);
        }
        int[] exp = {54, 98, 4, 6, 1, 10};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addToEndUntilExpand() {
        this.list.init(MockData.getArrayContainsTwo());
        for (int i = 0; i < MockData.elementsToAdd.length; i++) {
            this.list.addEnd(MockData.elementsToAdd[i]);
        }
        for (int i = 0; i < MockData.elementsToAdd.length; i++) {
            this.list.addEnd(MockData.elementsToAdd[i]);
        }
        int[] exp = {54, 98, 4, 6, 1, 10, 4, 6, 1, 10};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }


    //************** Get element by index test ********************

    @Test
    public void getIndexTestMany() {
        this.list.init(MockData.getArrayContainsMany());
        int act = this.list.get(3);
        int exp = 345;
        assertEquals(exp, act);
    }

    @Test
    public void getIndexTestOne() {
        this.list.init(MockData.getArrayContainsOne());
        int act = this.list.get(0);
        int exp = 69;
        assertEquals(exp, act);
    }

    @Test
    public void getIndexTestTwo() {
        this.list.init(MockData.getArrayContainsTwo());
        int act = this.list.get(1);
        int exp = 98;
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void getIndexTestZero() {
        this.list.init(MockData.getArrayContainsZero());
        int act = this.list.get(0);
        int exp = 69;
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void getIndexTestNull() {
        this.list.init(MockData.getArrayContainsNull());
        int act = this.list.get(0);
        int exp = 50;
        assertEquals(exp, act);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void getIndexTestDefault() {
        int act = this.list.get(4);
        int exp = 0;
        assertEquals(exp, act);
    }

    //********** Add element test by index ********************

    @Test
    public void addByPosOne() {
        this.list.init(MockData.getArrayContainsTwo());
        this.list.addByPos(1, 3);
        int[] exp = {54, 3, 98};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addByPosTwo() {
        this.list.init(MockData.getArrayContainsTwo());
        this.list.addByPos(0, 6);
        this.list.addByPos(1, 3);
        int[] exp = {6, 3, 54, 98};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addByPosMany() {
        this.list.init(MockData.getArrayContainsTwo());
        for (int i = 0; i < MockData.elementsToAdd.length; i++) {
            this.list.addByPos(i, MockData.elementsToAdd[i]);
        }
        int[] exp = {4, 6, 1, 10, 54, 98};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addByPosDefaultArray() {
        this.list.addByPos(2, 69);
        int[] exp = {0, 69};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void addByPosNullArray() {
        this.list.init(MockData.getArrayContainsNull());
        this.list.addByPos(1, 69);
        int[] exp = {0, 69};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void addByPosZeroArray() {
        this.list.init(MockData.getArrayContainsZero());
        this.list.addByPos(1, 69);
        int[] exp = {0, 69, 0, 0, 0, 0, 0, 0, 0, 0};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //********** Remove element from start ********************

    @Test
    public void removeFromStartOne() {
        this.list.init(MockData.getArrayContainsMany());
        this.list.removeStart();
        int[] exp = {55, 69, 345, 2, 43, 90, 48, 20, 99};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeFromStartTwo() {
        this.list.init(MockData.getArrayContainsMany());
        this.list.removeStart();
        this.list.removeStart();
        int[] exp = {69, 345, 2, 43, 90, 48, 20, 99};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeFromStartMany() {
        this.list.init(MockData.getArrayContainsMany());
        for (int i = 0; i < 5; i++) {
            this.list.removeStart();
        }
        int[] exp = {43, 90, 48, 20, 99};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void removeFromStartOutOfBounds() {
        this.list.init(MockData.getArrayContainsTwo());
        this.list.removeStart();
        this.list.removeStart();
        this.list.removeStart();
        int[] exp = {0, 0};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void removeFromStartZeroArray() {
        this.list.init(MockData.getArrayContainsZero());
        this.list.removeStart();
    }

    @Test(expected = NullPointerException.class)
    public void removeFromStartNullArray() {
        this.list.init(MockData.getArrayContainsNull());
        this.list.removeStart();
    }

    @Test(expected = NullPointerException.class)
    public void removeFromStartDefaultList() {
        this.list.removeStart();
    }

    //********** Remove from end element ********************

    @Test
    public void removeFromEndOne() {
        this.list.init(MockData.getArrayContainsMany());
        this.list.removeEnd();
        int[] exp = {1, 55, 69, 345, 2, 43, 90, 48, 20};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeFromEndTwo() {
        this.list.init(MockData.getArrayContainsMany());
        this.list.removeEnd();
        this.list.removeEnd();
        int[] exp = {1, 55, 69, 345, 2, 43, 90, 48};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeFromEndMany() {
        this.list.init(MockData.getArrayContainsMany());
        int i = 5;
        while (i > 0) {
            this.list.removeEnd();
            i--;
        }
        int[] exp = {1, 55, 69, 345, 2};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void removeFromEndOutOfBounds() {
        this.list.init(MockData.getArrayContainsOne());
        this.list.removeEnd();
        this.list.removeEnd();
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void removeFromEndZero() {
        this.list.init(MockData.getArrayContainsZero());
        this.list.removeEnd();
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void removeFromEndNull() {
        this.list.init(MockData.getArrayContainsNull());
        this.list.removeEnd();
    }

    //********** Remove element by index  ********************

    @Test
    public void removeByPosOne() {
        this.list.init(MockData.getArrayContainsMany());
        this.list.removeByPos(5);
        int[] exp = {1, 55, 69, 345, 2, 90, 48, 20, 99};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeByPosTwo() {
        this.list.init(MockData.getArrayContainsMany());
        this.list.removeByPos(5);
        this.list.removeByPos(3);
        int[] exp = {1, 55, 69, 2, 90, 48, 20, 99};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeByPosMany() {
        this.list.init(MockData.getArrayContainsMany());
        this.list.removeByPos(5);
        this.list.removeByPos(3);
        this.list.removeByPos(1);
        this.list.removeByPos(6);
        int[] exp = {1, 69, 2, 90, 48, 20};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void removeByIndexOutOfBounds() {
        this.list.init(MockData.getArrayContainsMany());
        this.list.removeByPos(11);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void removeByIndexOneOutOfBounds() {
        this.list.init(MockData.getArrayContainsOne());
        this.list.removeByPos(1);
        this.list.removeByPos(1);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void removeByPosZero() {
        this.list.init(MockData.getArrayContainsZero());
        this.list.removeByPos(1);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void removeByPosDefaultArray() {
        this.list.removeByPos(1);
    }

    //*********** Get max element in array ****************

    @Test
    public void getMaxElement() {
        this.list.init(MockData.getArrayContainsMany());
        int exp = 345;
        int act = this.list.max();
        assertEquals(exp, act);
    }

    @Test
    public void getMaxElementTwo() {
        this.list.init(MockData.getArrayContainsTwo());
        int exp = 98;
        int act = this.list.max();
        assertEquals(exp, act);
    }

    @Test
    public void getMaxElementOne() {
        this.list.init(MockData.getArrayContainsOne());
        int exp = 69;
        int act = this.list.max();
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void getMaxArrayNull() {
        this.list.init(MockData.getArrayContainsNull());
        this.list.max();
    }

    @Test(expected = NullPointerException.class)
    public void getMaxArrayZero() {
        this.list.init(MockData.getArrayContainsZero());
        this.list.max();
    }

    @Test(expected = NullPointerException.class)
    public void getMaxArrayDefault() {
        this.list.max();
    }

    //********** Get min element in array *********************


    @Test
    public void getMinElement() {
        this.list.init(MockData.getArrayContainsMany());
        int exp = 1;
        int act = this.list.min();
        assertEquals(exp, act);
    }

    @Test
    public void getMinElementTwo() {
        this.list.init(MockData.getArrayContainsTwo());
        int exp = 54;
        int act = this.list.min();
        assertEquals(exp, act);
    }

    @Test
    public void getMinElementOne() {
        this.list.init(MockData.getArrayContainsOne());
        int exp = 69;
        int act = this.list.min();
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void getMinArrayNull() {
        this.list.init(MockData.getArrayContainsNull());
        this.list.min();
    }

    @Test(expected = NullPointerException.class)
    public void getMinArrayZero() {
        this.list.init(MockData.getArrayContainsZero());
        this.list.min();
    }

    @Test(expected = NullPointerException.class)
    public void getMinArrayDefault() {
        this.list.min();
    }

    //************* Get max element position ************

    @Test
    public void getMaxElementPosition() {
        this.list.init(MockData.getArrayContainsMany());
        int exp = 3;
        int act = this.list.maxPos();
        assertEquals(exp, act);
    }

    @Test
    public void getMaxElementPositionTwo() {
        this.list.init(MockData.getArrayContainsTwo());
        int exp = 1;
        int act = this.list.maxPos();
        assertEquals(exp, act);
    }

    @Test
    public void getMaxElementPositionOne() {
        this.list.init(MockData.getArrayContainsOne());
        int exp = 0;
        int act = this.list.maxPos();
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void getMaxElementPositionNull() {
        this.list.init(MockData.getArrayContainsNull());
        this.list.maxPos();
    }

    @Test(expected = NullPointerException.class)
    public void getMaxElementPositionZero() {
        this.list.init(MockData.getArrayContainsZero());
        this.list.maxPos();
    }

    @Test(expected = NullPointerException.class)
    public void getMaxElementDefaultArray() {
        this.list.maxPos();
    }

    //************* Get max element position ************

    @Test
    public void getMinElementPosition() {
        this.list.init(MockData.getArrayContainsMany());
        int exp = 0;
        int act = this.list.minPos();
        assertEquals(exp, act);
    }

    @Test
    public void getMinElementPositionTwo() {
        this.list.init(MockData.getArrayContainsTwo());
        int exp = 0;
        int act = this.list.minPos();
        assertEquals(exp, act);
    }

    @Test
    public void getMinElementPositionOne() {
        this.list.init(MockData.getArrayContainsOne());
        int exp = 0;
        int act = this.list.minPos();
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void getMinElementPositionNull() {
        this.list.init(MockData.getArrayContainsNull());
        this.list.minPos();
    }

    @Test(expected = NullPointerException.class)
    public void getMinElementPositionZero() {
        this.list.init(MockData.getArrayContainsZero());
        this.list.minPos();
    }

    //********** Sort array *********************

    @Test
    public void sortArrayMany() {
        this.list.init(MockData.getArrayContainsMany());
        int[] exp = {1, 2, 20, 43, 48, 55, 69, 90, 99, 345};
        int[] act = this.list.sort();
        assertArrayEquals(exp, act);
    }

    @Test
    public void sortArrayTwo() {
        this.list.init(MockData.getArrayContainsTwo());
        int[] exp = {54, 98};
        int[] act = this.list.sort();
        assertArrayEquals(exp, act);
    }

    @Test
    public void sortArrayOne() {
        this.list.init(MockData.getArrayContainsOne());
        int[] exp = {69};
        int[] act = this.list.sort();
        assertArrayEquals(exp, act);
    }

    @Test
    public void sortArrayNull() {
        this.list.init(MockData.getArrayContainsNull());
        int[] exp = {};
        int[] act = this.list.sort();
        assertArrayEquals(exp, act);
    }

    @Test
    public void sortArrayZero() {
        this.list.init(MockData.getArrayContainsZero());
        int[] exp = {};
        int[] act = this.list.sort();
        assertArrayEquals(exp, act);
    }

    @Test
    public void sortDefaultArray() {
        int[] exp = {};
        int[] act = this.list.sort();
        assertArrayEquals(exp, act);
    }

    //********** Get element by pos ***************

    @Test
    public void getElementByPosManyFirst() {
        this.list.init(MockData.getArrayContainsMany());
        int exp = 69;
        int act = this.list.get(2);
        assertEquals(exp, act);
    }

    @Test
    public void getElementByPosManySecond() {
        this.list.init(MockData.getArrayContainsMany());
        int exp = 345;
        int act = this.list.get(3);
        assertEquals(exp, act);
    }

    @Test
    public void getElementByPosTwo() {
        this.list.init(MockData.getArrayContainsTwo());
        int exp = 98;
        int act = this.list.get(1);
        assertEquals(exp, act);
    }

    @Test
    public void getElementByPosOne() {
        this.list.init(MockData.getArrayContainsOne());
        int exp = 69;
        int act = this.list.get(0);
        assertEquals(exp, act);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void getElementByPosManyOutOfBounds() {
        this.list.init(MockData.getArrayContainsMany());
        int exp = 0;
        int act = this.list.get(12);
        assertEquals(exp, act);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void getElementByPosTwoOutOfBounds() {
        this.list.init(MockData.getArrayContainsTwo());
        int exp = 0;
        int act = this.list.get(5);
        assertEquals(exp, act);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void getElementByPosNull() {
        this.list.init(MockData.getArrayContainsNull());
        int exp = 0;
        int act = this.list.get(4);
        assertEquals(exp, act);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void getElementByPosZero() {
        this.list.init(MockData.getArrayContainsZero());
        int exp = 0;
        int act = this.list.get(4);
        assertEquals(exp, act);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void getElementByPosDefaultArray() {
        int exp = 0;
        int act = this.list.get(4);
        assertEquals(exp, act);
    }

    //************ Reverse array ***********************

    @Test
    public void halfReversMany() {
        this.list.init(MockData.getArrayContainsMany());
        int[] exp = {43, 90, 48, 20, 99, 1, 55, 69, 345, 2};
        int[] act = this.list.halfRevers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversOne() {
        this.list.init(MockData.getArrayContainsOne());
        int[] exp = {69};
        int[] act = this.list.halfRevers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversTwo() {
        this.list.init(MockData.getArrayContainsTwo());
        int[] exp = {98, 54};
        int[] act = this.list.halfRevers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversSeven() {
        this.list.init(MockData.getArrayContainsSevenElements());
        int[] exp = {1, 5, 9, 3, 8, 4, 2};
        int[] act = this.list.halfRevers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReverseNull() {
        this.list.init(MockData.getArrayContainsNull());
        int[] exp = {};
        int[] act = this.list.halfRevers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReverseZero() {
        this.list.init(MockData.getArrayContainsZero());
        int[] exp = {};
        int[] act = this.list.halfRevers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReverseDefaultArray() {
        int[] exp = {};
        int[] act = this.list.halfRevers();
        assertArrayEquals(exp, act);
    }

    //************ Full reverse array ***********************

    @Test
    public void fullReversMany() {
        this.list.init(MockData.getArrayContainsMany());
        int[] exp = {99, 20, 48, 90, 43, 2, 345, 69, 55, 1};
        int[] act = this.list.fullRevers();
        assertArrayEquals(exp, act);
    }

    //{1, 55, 69, 345, 2, 43, 90, 48, 20, 99};
    @Test
    public void fullReverseTwo() {
        this.list.init(MockData.getArrayContainsTwo());
        int[] exp = {98, 54};
        int[] act = this.list.fullRevers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void fullReverseOne() {
        this.list.init(MockData.getArrayContainsOne());
        int[] exp = {69};
        int[] act = this.list.fullRevers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void fullReverseSeven() {
        this.list.init(MockData.getArrayContainsSevenElements());
        int[] exp = {3, 9, 5, 1, 2, 4, 8};
        int[] act = this.list.fullRevers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void fullReverseZero() {
        this.list.init(MockData.getArrayContainsZero());
        int[] exp = {};
        int[] act = this.list.fullRevers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void fullReverseNull() {
        this.list.init(MockData.getArrayContainsNull());
        int[] exp = {};
        int[] act = this.list.fullRevers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void fullReversDefaultArray() {
        int[] exp = {};
        int[] act = this.list.fullRevers();
        assertArrayEquals(exp, act);
    }

    //************ Set element in array ****************

    @Test
    public void setElementOne() {
        this.list.init(MockData.getArrayContainsMany());
        this.list.set(4, 89);
        int[] exp = {1, 55, 69, 345, 89, 43, 90, 48, 20, 99};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void setElementTwo() {
        this.list.init(MockData.getArrayContainsMany());
        this.list.set(1, 89);
        this.list.set(4, 52);
        int[] exp = {1, 89, 69, 345, 52, 43, 90, 48, 20, 99};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void setElementMany() {
        this.list.init(MockData.getArrayContainsMany());
        this.list.set(1, 89);
        this.list.set(3, 88);
        this.list.set(6, 70);
        this.list.set(8, 9);
        int[] exp = {1, 89, 69, 88, 2, 43, 70, 48, 9, 99};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void setElementZero() {
        this.list.init(MockData.getArrayContainsMany());
        this.list.set(14, 2);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void setElementZeroArray() {
        this.list.init(MockData.getArrayContainsZero());
        this.list.set(2, 10);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void setElementDefaultArray() {
        this.list.set(3, 10);
    }
}
