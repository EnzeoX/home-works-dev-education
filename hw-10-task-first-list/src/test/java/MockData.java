public class MockData {

    public static int[] arrayContainsMany = {1, 55, 69, 345, 2, 43, 90, 48, 20, 99};

    public static int[] arrayContainsSevenElements = {8, 4, 2, 1, 5, 9, 3};

    public static int[] arrayContainsOne = {69};

    public static int[] arrayContainsTwo = {54, 98};

    public static int[] arrayContainsZero = {};

    public static int[] arrayContainsNull = null;

    public static int[] elementsToAdd = {4, 6, 1, 10};

    public static int[] getArrayContainsMany() {
        return arrayContainsMany;
    }

    public static int[] getArrayContainsOne() {
        return arrayContainsOne;
    }

    public static int[] getArrayContainsTwo() {
        return arrayContainsTwo;
    }

    public static int[] getArrayContainsZero() {
        return arrayContainsZero;
    }

    public static int[] getArrayContainsNull() {
        return arrayContainsNull;
    }

    public static int[] getArrayContainsSevenElements() {
        return arrayContainsSevenElements;
    }
}
