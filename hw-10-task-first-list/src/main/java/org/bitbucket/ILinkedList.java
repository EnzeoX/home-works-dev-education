package org.bitbucket;

public interface ILinkedList {

    void init(int[] init);

    void clear();

    int size();

    int[] toArray();

    void addStart(int value);

    void addEnd(int value);

    void addByPos(int pos, int value);

    int removeStart();

    int removeEnd();

    int removeByPos(int value);

    int max();

    int min();

    int maxPos();

    int minPos();

    int[] sort();

    int get(int pos);

    void halfRevers();

    void fullRevers();

    void set(int pos, int value);
}
