package org.bitbucket;

import java.util.Objects;

public class OneWayLinked implements ILinkedList {

    private Node head;

    private Node last;

    public static class Node {

        int value;

        Node next = null;
    }

    @Override
    public void init(int[] init) {
        this.clear();
        if (Objects.nonNull(init)) {
            for (int i = 0; i < init.length; i++) {
                addEnd(init[i]);
            }
        } else {
            throw new NullPointerException("Array is null");
        }
    }

    @Override
    public void clear() {
        this.head = this.last = null;
    }

    @Override
    public int size() {
        int size = 0;
        Node buffer = this.head;
        while (buffer != null) {
            buffer = buffer.next;
            size++;
        }
        return size;
    }

    @Override
    public int[] toArray() {
        int[] toArray = new int[size()];
        int step = 0;
        Node buffer = this.head;
        while (buffer != null) {
            toArray[step++] = buffer.value;
            buffer = buffer.next;
        }
        return toArray;
    }

    @Override
    public void addStart(int value) {
        Node n = new Node();
        n.value = value;
        if (this.head == null) {
            this.head = n;
        } else {
            n.next = this.head;
            this.head = n;
        }
    }

    @Override
    public void addEnd(int value) {
        Node n = new Node();
        n.value = value;
        if (Objects.isNull(this.head)) {
            this.head = n;
        } else {
            Node buffer = this.head;
            while (buffer.next != null) {
                buffer = buffer.next;
            }
            buffer.next = n;
        }
    }

    @Override
    public void addByPos(int pos, int value) {
        Node buffer = this.head;
        Node newNode = new Node();
        newNode.value = value;
        if (pos == 0) {
            addStart(value);
        } else {
            for (int i = 0; i < pos - 1; i++) {
                buffer = buffer.next;
            }
            newNode.next = buffer.next;
            buffer.next = newNode;
        }
    }

    @Override
    public int removeStart() {
        int value = this.head.value;
        this.head = this.head.next;
        return value;
    }

    @Override
    public int removeEnd() {
        int value;
        Node buffer = this.head;
        Node bufferNode = null;
        while (buffer.next != null) {
            bufferNode = buffer;
            buffer = buffer.next;
        }
        buffer = bufferNode;
        value = buffer.value;
        buffer.next = null;
        return value;
    }

    @Override
    public int removeByPos(int value) {
        int deletedValue;
        if (value == 0) {
            deletedValue = removeStart();
        } else {
            Node buffer = this.head;
            Node nextBuffer = null;
            for (int i = 0; i < value - 1; i++) {
                buffer = buffer.next;
            }
            nextBuffer = buffer.next;
            deletedValue = buffer.next.value;
            buffer.next = nextBuffer.next;

        }
        return deletedValue;
    }

    @Override
    public int max() {
        int value = this.head.value;
        Node buffer = this.head;
        while (buffer.next != null) {
            buffer = buffer.next;
            if (value < buffer.value) {
                value = buffer.value;
            }
        }
        return value;
    }

    @Override
    public int min() {
        int value = this.head.value;
        Node buffer = this.head;
        while (buffer.next != null) {
            buffer = buffer.next;
            if (value > buffer.value) {
                value = buffer.value;
            }
        }
        return value;
    }

    @Override
    public int maxPos() {
        int position = 0;
        int bufferPos = 0;
        int value = this.head.value;
        Node buffer = this.head;
        while (buffer.next != null) {
            bufferPos++;
            buffer = buffer.next;
            if (value < buffer.value) {
                value = buffer.value;
                position = bufferPos;
            }
        }
        return position;
    }

    @Override
    public int minPos() {
        int position = 0;
        int bufferPos = 0;
        int value = this.head.value;
        Node buffer = this.head;
        while (buffer.next != null) {
            bufferPos++;
            buffer = buffer.next;
            if (value > buffer.value) {
                value = buffer.value;
                position = bufferPos;
            }
        }
        return position;
    }

    @Override
    public int[] sort() {

        return new int[0];
    }

    @Override
    public int get(int pos) {
        int count = 0;
        Node buffer = this.head;
        while (count != pos) {
            buffer = buffer.next;
            count++;
        }
        return buffer.value;
    }

    @Override
    public void halfRevers() {

    }

    @Override
    public void fullRevers() {
        if (this.head == null) {
            throw new ArrayIndexOutOfBoundsException("List is null");
        } else {
            Node prev = null;
            Node current = this.head;
            Node next = null;
            while (current != null) {
                next = current.next;
                current.next = prev;
                prev = current;
                current = next;
            }
            this.head = prev;
        }

    }

    @Override
    public void set(int pos, int value) {
        if (this.head == null) {
            this.head.value = value;
        } else {
            Node buffer = this.head;
            for (int i = 0; i < pos; i++) {
                buffer = buffer.next;
            }
            buffer.value = value;
        }
    }
}
