package org.bitbucket;

public class ArrayList implements IList {

    private int[] array = new int[10];

    private int capacity = 10;

    private int quantity;

    private void expandArray() {
        int newCapacity;
        if (this.capacity < 10) {
            newCapacity = 10;
        } else {
            newCapacity = (int) (this.capacity + Math.ceil(this.capacity * 0.7));
        }
        this.capacity = newCapacity;
        int[] newArray = new int[newCapacity];
        for (int i = 0; i < this.array.length; i++) {
            newArray[i] = this.array[i];
        }
        this.array = newArray;
    }

    @Override
    public void init(int[] init) {
        this.clear();
        if (init == null || init.length == 0) {
            this.capacity = 0;
        } else {
            if (this.quantity < init.length) {
                this.capacity = init.length;
            }
            for (int i = 0; i < capacity; i++) {
                this.array[i] = init[i];
                this.quantity++;
            }
        }
    }

    @Override
    public void clear() {
        this.capacity = 10;
        this.quantity = 0;
        this.array = new int[capacity];
    }

    @Override
    public int size() {
        return this.quantity;
    }

    @Override
    public int[] toArray() {
        int[] result = new int[this.quantity];
        for (int i = 0; i < this.quantity; i++) {
            result[i] = this.array[i];
        }
        return result;
    }

    @Override
    public void addStart(int value) {
        if (this.quantity + 1 >= (int) Math.ceil(this.capacity * 0.7)) {
            expandArray();
        }
        for (int i = this.quantity; i > 0; i--) {
            this.array[i] = this.array[i - 1];
        }
        this.array[0] = value;
        this.quantity++;
    }

    @Override
    public void addEnd(int value) {
        if (this.quantity + 1 >= (int) Math.ceil(this.capacity * 0.7)) {
            expandArray();
        }
        this.array[quantity] = value;
        this.quantity++;
    }

    @Override
    public void addByPos(int pos, int value) {
        if ( this.array == null) {
            throw new ArrayIndexOutOfBoundsException("Array is null");
        } else if (pos > this.capacity) {
            throw new ArrayIndexOutOfBoundsException("Position is out of bounds");
        } else {
            if (this.quantity + 1 >= (int) Math.ceil(this.capacity * 0.7)) {
                expandArray();
            }
            for (int i = this.quantity; i > pos; i--) {
                this.array[i] = this.array[i - 1];
            }
            this.array[pos] = value;
            this.quantity++;
        }
    }

    @Override
    public int removeStart() {
        if(this.quantity == 0)
            throw new NullPointerException("Array is null");
        int removedValue = array[0];
        for (int i = 0; i < quantity; i++) {
            if(i + 1 != this.quantity) {
                this.array[i] = this.array[i + 1];
            } else {
                this.array[i] = 0;
            }
        }
        this.quantity--;
        return removedValue;
    }

    @Override
    public int removeEnd() {
        if (this.array == null || this.capacity == 0 || this.quantity == 0) {
            throw new ArrayIndexOutOfBoundsException("Array is null");
        }
        int removedValue = this.array[quantity - 1];
        this.array[quantity - 1] = 0;
        this.quantity--;
        return removedValue;
    }

    @Override
    public int removeByPos(int index) {
        if (index > this.quantity) {
            throw new ArrayIndexOutOfBoundsException("Index is out of bounds");
        }
        int removedValue = this.array[index];
        for (int i = index; i < quantity; i++) {
            if (i + 1 == quantity) {
                this.array[i] = 0;
                break;
            }
            this.array[i] = this.array[i + 1];
        }
        this.quantity--;
        return removedValue;
    }

    @Override
    public int max() {
        if (this.quantity == 0) {
            throw new NullPointerException("Array is null");
        }
        int maxValue = array[0];
        for (int i = 0; i < this.capacity; i++) {
            if (maxValue < array[i]) {
                maxValue = array[i];
            }
        }
        return maxValue;
    }

    @Override
    public int min() {
        if (this.quantity == 0) {
            throw new NullPointerException("Array is null");
        }
        int minValue = array[0];
        for (int i = 0; i < this.capacity; i++) {
            if (minValue > array[i]) {
                minValue = array[i];
            }
        }
        return minValue;
    }

    @Override
    public int maxPos() {
        if (this.quantity == 0) {
            throw new NullPointerException("Array is null");
        }
        int position = 0;
        int maxValue = array[0];
        for (int i = 0; i < this.capacity; i++) {
            if (maxValue < array[i]) {
                maxValue = array[i];
                position = i;
            }
        }
        return position;
    }

    @Override
    public int minPos() {
        if (this.quantity == 0) {
            throw new NullPointerException("Array is null");
        }
        int position = 0;
        int minValue = array[0];
        for (int i = 0; i < this.capacity; i++) {
            if (minValue > array[i]) {
                minValue = array[i];
                position = i;
            }
        }
        return position;
    }

    @Override
    public int[] sort() {
        if (this.array == null || this.quantity == 0 || this.capacity == 0) {
            return this.toArray();
        }
        quickSort(this.array, 0, this.quantity - 1);
        return this.toArray();
    }

    @Override
    public int get(int pos) {
        if (pos > this.quantity) {
            throw new ArrayIndexOutOfBoundsException("Position out of bounds");
        } else if (this.capacity == 0) {
            throw new NullPointerException("Array is null");
        }
        return this.array[pos];
    }

    @Override
    public int[] halfRevers() {
        if (this.quantity <= 1 || this.capacity == 0 || this.array.length == 0)
            return this.toArray();
        int[] reversArray = new int[capacity];
        int evenNum = 1;
        if (quantity % 2 == 0) {
            evenNum = 0;
        }
        for (int i = 0; i < this.quantity / 2 + evenNum; i++) {
            reversArray[i] = this.array[this.quantity / 2 + i];

        }
        for (int j = 0; j < this.quantity / 2; j++) {
            reversArray[this.quantity / 2 + j + evenNum] = this.array[j];
        }
        return reversArray;
    }

    @Override
    public int[] fullRevers() {
        if (this.quantity == 0 || this.quantity == 1) {
            return this.toArray();
        }
        int[] reversArray = new int[capacity];
        for (int i = 0; i < quantity; i++) {
            reversArray[i] = this.array[quantity - i - 1];
        }
        return reversArray;
    }

    @Override
    public void set(int pos, int value) {
        if (pos > quantity) {
            throw new ArrayIndexOutOfBoundsException("Position to set is out of bounds");
        }
        this.array[pos] = value;
    }

    private static void quickSort(int[] array, int low, int high) {
        if (array.length == 0 || array.equals(null))
            return;
        if (low >= high)
            return;
        int middle = low + (high - low) / 2;
        int pivot = array[middle];

        int i = low, j = high;
        while (i <= j) {
            while (array[i] < pivot) {
                i++;
            }
            while (array[j] > pivot) {
                j--;
            }
            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }
        if (low < j)
            quickSort(array, low, j);
        if (high > i)
            quickSort(array, i, high);
    }
}
