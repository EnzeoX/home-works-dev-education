package org.bitbucket.desktopcalculator;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class SwingCalculator {
    public static void main(String[] args) throws IOException {
        JFrame frame = new JFrame("Калькулятор");
        frame.setSize(320, 315);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        JPanel panel = new JPanel();
        frame.add(panel);
        placeComponents(panel);
        frame.setVisible(true);
    }

    private static void placeComponents(JPanel panel) throws IOException {

        panel.setBorder((BorderFactory.createStrokeBorder(new BasicStroke(1.0f))));
        panel.setBackground(Color.decode("#DDE2CD"));
        panel.setLayout(null);

        JLabel firstNumber = new JLabel("Число 1", SwingConstants.RIGHT);
        firstNumber.setFont(new Font("Times New Roman",Font.PLAIN, 16));
        firstNumber.setBounds(10,30,80,25);
        panel.add(firstNumber);

        JTextField inputFirstNumber = new JTextField(20);
        inputFirstNumber.setHorizontalAlignment(JTextField.CENTER);
        inputFirstNumber.setBounds(100,25,180,35);
        inputFirstNumber.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
        panel.add(inputFirstNumber);

        JLabel secondNumber = new JLabel("Число 2", SwingConstants.RIGHT);
        secondNumber.setFont(new Font("Times New Roman",Font.PLAIN, 16));
        secondNumber.setBounds(10,75,80,25);
        panel.add(secondNumber);

        JTextField inputSecondNumber = new JTextField(20);
        inputSecondNumber.setHorizontalAlignment(JTextField.CENTER);
        inputSecondNumber.setBounds(100,70,180,35);
        inputSecondNumber.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
        panel.add(inputSecondNumber);

        JLabel operator = new JLabel("Операция", SwingConstants.RIGHT);
        operator.setFont(new Font("Times New Roman",Font.PLAIN, 16));
        operator.setBounds(10,120,80,25);
        panel.add(operator);

        JTextField inputOperator = new JTextField(20);
        inputOperator.setHorizontalAlignment(JTextField.CENTER);
        inputOperator.setBounds(100,115,180,35);
        inputOperator.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
        panel.add(inputOperator);

        JButton resultButton = new JButton("Посчитать");
        resultButton.setBorder(BorderFactory.createEmptyBorder());
        resultButton.setContentAreaFilled(false);
        resultButton.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
        resultButton.setBackground(Color.decode("#DDE2CD"));
        resultButton.setBounds(50, 165, 230, 45);
        panel.add(resultButton);

        JLabel result = new JLabel("Результат", SwingConstants.RIGHT);
        result.setFont(new Font("Times New Roman",Font.PLAIN, 16));
        result.setBounds(10,225,80,25);
        panel.add(result);

        JTextField resultField = new JTextField(20);
        resultField.setHorizontalAlignment(JTextField.CENTER);
        resultField.setBounds(100,220,180,35);
        resultField.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
        panel.add(resultField);

        resultButton.addActionListener(e -> {
            double firstNum;
            double secondNum;
            String operatorString;
            double resultNum;
            try{
                firstNum = Double.parseDouble(inputFirstNumber.getText());
                secondNum = Double.parseDouble(inputSecondNumber.getText());
                operatorString = inputOperator.getText();
                switch (operatorString) {
                    case ("+"):
                        resultNum = firstNum + secondNum;
                        resultField.setText(String.valueOf(resultNum));
                        break;
                    case ("-"):
                        resultNum = firstNum - secondNum;
                        resultField.setText(String.valueOf(resultNum));
                        break;
                    case ("*"):
                        resultNum = firstNum * secondNum;
                        resultField.setText(String.valueOf(resultNum));
                        break;
                    case ("/"):
                        if (firstNum == 0) {
                            resultField.setText("Вселенная свернулась");
                        } else if (secondNum == 0) {
                            resultField.setText("Вселенная взорвалась");
                        } else {
                            resultNum = firstNum / secondNum;
                            resultField.setText(String.valueOf(resultNum));
                        }
                        break;
                    default:
                        resultField.setText("Введите операцию");
                        break;
                }
            } catch (Exception Ignore) {
                resultField.setText("Введите оба числа!");
            }
        });
    }
}