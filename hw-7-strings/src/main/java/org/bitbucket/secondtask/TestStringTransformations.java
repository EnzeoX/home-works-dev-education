package org.bitbucket.secondtask;

import org.junit.Assert;
import org.junit.Test;

public class TestStringTransformations {

    @Test
    public void numToString() {
        int toConvert = 333444555;
        String exp = "333444555";
        String act = StringTransformations.numToString(toConvert);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void doubleToString() {
        double toConvert = 69.69;
        String exp = "69.69";
        String act = StringTransformations.doubleToString(toConvert);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void stringToNumber() {
        String toConvert = "69696969";
        int exp = 69696969;
        int act = StringTransformations.stringToNumber(toConvert);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void stringToNumberNull() {
        String toConvert = "Will be null";
        int exp = 0;
        int act = StringTransformations.stringToNumber(toConvert);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void stringToDouble() {
        String toConvert = "69.69";
        double exp = 69.69;
        double act = StringTransformations.stringToDouble(toConvert);
        double delta = 0.01;
        Assert.assertEquals(exp, act, delta);
    }

    @Test
    public void stringToDoubleNull() {
        String toConvert = "Will be null";
        double exp = 0;
        double act = StringTransformations.stringToDouble(toConvert);
        double delta = 0.01;
        Assert.fail();
    }
}
