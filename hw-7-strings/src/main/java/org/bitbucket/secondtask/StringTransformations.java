package org.bitbucket.secondtask;

public class StringTransformations {

    public static String numToString(int number) {
        return Integer.toString(number);
    }

    public static String doubleToString(double number) {
        return Double.toString(number);
    }

    public static int stringToNumber(String strNumber) {
        try{
            return Integer.parseInt(strNumber);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static double stringToDouble(String strNumber) {
        try{
            return Double.parseDouble(strNumber);
        } catch (NumberFormatException e) {
            return 0;
        }
    }
}
