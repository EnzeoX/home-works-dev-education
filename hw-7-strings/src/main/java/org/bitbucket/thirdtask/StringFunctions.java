package org.bitbucket.thirdtask;

public class StringFunctions {

    public static String stringMinWordLength(String string) {
        if(string == null || "".equals(string)) {
            throw new NullPointerException("String is null");
        }
        String bufferStr = string.replaceAll("[,.!?/]", "");
        String[] splitedString = bufferStr.split("[\\s]");
        String smallestWord = splitedString[0];
        for(String p: splitedString) {
            if(p.length() <= smallestWord.length() && !" ".equals(p)) {
                smallestWord = p;
            }
        }
        return smallestWord;
    }

    public static String[] changeSymbols(String[] arrayOfWords, int length) {
        if(arrayOfWords == null) {
            throw new NullPointerException("String is null");
        }
        for(int i = 0; i < arrayOfWords.length; i++) {
            if(arrayOfWords[i].length() == length) {
                arrayOfWords[i] = arrayOfWords[i].replaceAll(".{3}$","\\$\\$\\$");
            }
        }
        return arrayOfWords;
    }

    public static String addSpaceAfterPunctuation(String string) {
        if(string == null || "".equals(string)) {
            throw new NullPointerException("String is null");
        }
        return string.replaceAll("(?<=\\p{Punct})(?=\\w)", " ");
    }

    public static String deleteDuplicatesElements(String string) {
        if(string == null || "".equals(string)) {
            throw new NullPointerException("String is null");
        }
        char[] arrayOfString = string.toCharArray();
        String charArray = "";
        for (int i = 0; i < arrayOfString.length; i++) {
            if(charArray.indexOf(arrayOfString[i]) == -1) {
                charArray = charArray + arrayOfString[i];
            }
        }
        return charArray;
    }

    public static int numberOfWordsInString(String string) {
        if (string == null || "".equals(string)) {
            throw new NullPointerException("String is null");
        }
        String[] arrayOfString = string.split(" ");
        return arrayOfString.length;
    }

    public static String deleteSomeStringPart(String string, int startPosition, int endPosition) {
        if(string == null || "".equals(string)) {
            throw new NullPointerException("String is null");
        }
        return string.replace((string.substring(startPosition, endPosition + 1)), "");
    }

    public static String reverseString(String string){
        if(string == null || "".equals(string)) {
            throw new NullPointerException("String is null");
        }
        char[] stringToChar = string.toCharArray();
        for(int i = 0; i < stringToChar.length/2; i++) {
            char buffer = stringToChar[i];
            stringToChar[i] = stringToChar[stringToChar.length - i - 1];
            stringToChar[stringToChar.length - i - 1] = buffer;
        }
        return new String(stringToChar);
    }

    public static String deleteLastWord(String string){
        if(string == null || "".equals(string)) {
            throw new NullPointerException("String is null");
        }
        return string.substring(0, string.lastIndexOf(" "));
    }
}
