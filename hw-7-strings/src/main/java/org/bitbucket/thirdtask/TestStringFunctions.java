package org.bitbucket.thirdtask;

import org.junit.Assert;
import org.junit.Test;

public class TestStringFunctions {

    @Test(expected = NullPointerException.class)
    public void smallestWord(){
        String data = "This string contains word, which length is two symbols";
        String nullData = "";
        String exp = "is";
        String act = StringFunctions.stringMinWordLength(data);
        String actNull = StringFunctions.stringMinWordLength(nullData);
        Assert.assertEquals(exp, act);
        Assert.assertEquals(exp, actNull);
    }

    @Test(expected = NullPointerException.class)
    public void changeSymbols() {
        String[] data = {"We", "have", "some", "words", "with", "length", "of", "four", "Change", "them"};
        String[] nullData = null;
        String[] exp =  {"We", "h$$$", "s$$$", "words", "w$$$", "length", "of", "f$$$", "Change", "t$$$"};
        String[] act = StringFunctions.changeSymbols(data, 4);
        String[] actNull = StringFunctions.changeSymbols(nullData, 0);
        Assert.assertArrayEquals(exp, act);
        Assert.assertArrayEquals(nullData, actNull);
    }

    @Test(expected = NullPointerException.class)
    public void spaceAfterPunctuation(){
        String data = "Add space after, punctuation symbols,if there is none.";
        String nullData = "";
        String act = StringFunctions.addSpaceAfterPunctuation(data);
        String actNull = StringFunctions.addSpaceAfterPunctuation(nullData);
        String exp = "Add space after, punctuation symbols, if there is none.";
        Assert.assertEquals(exp, act);
        Assert.assertEquals(exp, actNull);
    }

    @Test(expected = NullPointerException.class)
    public void removeDuplicates() {
        String data = "aaabbbbccccxfff";
        String nullData = "";
        String act = StringFunctions.deleteDuplicatesElements(data);
        String actNull = StringFunctions.deleteDuplicatesElements(nullData);
        String exp = "abcxf";
        Assert.assertEquals(exp, act);
        Assert.assertEquals(exp, actNull);
    }

    @Test(expected = NullPointerException.class)
    public void quantityOfWordsInString() {
        String nullData = "";
        String data = "There is only seven words. Trust me";
        int exp = 7;
        int act = StringFunctions.numberOfWordsInString(data);
        int actNull = StringFunctions.numberOfWordsInString(nullData);
        Assert.assertEquals(exp, act);
        Assert.assertEquals(exp, actNull);
    }

    @Test(expected = NullPointerException.class)
    public void deleteSomeStringPart() {
        String nullData = "";
        String data = "From five position delete you must to here.Yoda";
        String exp = "From Yoda";
        String act = StringFunctions.deleteSomeStringPart(data, 5, 42);
        String actNull = StringFunctions.deleteSomeStringPart(nullData, 1, 22);
        Assert.assertEquals(exp,act);
        Assert.assertEquals(exp,actNull);
    }

    @Test(expected = NullPointerException.class)
    public void reverseString() {
        String nullData = "";
        String data = "a b c d e f";
        String exp = "f e d c b a";
        String act = StringFunctions.reverseString(data);
        String actNull = StringFunctions.reverseString(nullData);
        Assert.assertEquals(exp, act);
        Assert.assertEquals(exp, actNull);
    }

    @Test(expected = NullPointerException.class)
    public void deleteLastWord() {
        String data = "First mid last";
        String nullData = "";
        String act = StringFunctions.deleteLastWord(data);
        String actNull = StringFunctions.deleteLastWord(nullData);
        String exp = "First mid";
        Assert.assertEquals(exp, act);
        Assert.assertEquals(exp, actNull);
    }
}
