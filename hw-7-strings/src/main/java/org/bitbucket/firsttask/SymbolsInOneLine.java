package org.bitbucket.firsttask;

public class SymbolsInOneLine {
    public static void engAlphabetUpperCase() {
        char c;
        for(c = 'A'; c <= 'Z'; c++) {
            System.out.print(c + " ");
        }
        System.out.println();
    }

    public static void engAlphabetLowerCase() {
        char c;
        for(c = 'a'; c <= 'z'; c++) {
            System.out.print(c + " ");
        }
        System.out.println();
    }

    public static void rusAlphabetLowerCaseUnicode() {
        char c;
        for(c = '\u0430'; c <= '\u044f'; c++) {
            System.out.print(c + " ");
        }
        System.out.println();

    }

    public static void rusAlphabetLowerCase() {
        char c;
        for(c = 'а'; c <= 'я'; c++) {
            System.out.print(c + " ");
        }
        System.out.println();

    }

    public static void numbersOut() {
        char c;
        for(c = '0'; c <= '9'; c++) {
            System.out.print(c + " ");
        }
        System.out.println();
    }

    public static void asciiSymbols() {
        char c;
        for(c = '\u0030'; c <= '\u007e'; c++) {
            System.out.print(c + " ");
        }
        System.out.println();
    }
}
