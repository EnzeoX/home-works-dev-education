package org.bitbucket.firsttask;

public class Start {
    public static void main(String[] args) {
        SymbolsInOneLine.engAlphabetUpperCase();
        SymbolsInOneLine.engAlphabetLowerCase();
        SymbolsInOneLine.rusAlphabetLowerCase();
        SymbolsInOneLine.rusAlphabetLowerCaseUnicode();
        SymbolsInOneLine.numbersOut();
        SymbolsInOneLine.asciiSymbols();

    }

}
