package org.bitbucket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bitbucket.exceptions.NumberLowerThanFive;
import org.bitbucket.randnumber.RandomNumber;

public class Work {

    static final Logger log = LogManager.getLogger("Work");

    private int randomNum;

    public int randomNumWithLog() throws NumberLowerThanFive {
        randomNum = RandomNumber.randomNumberGenerator();
        if(randomNum > 5) {
            log.info("Приложение успешно запущено");
            return randomNum;
        } else {
            log.error(new NumberLowerThanFive("Сгенерированное число - " + randomNum));
            throw new NumberLowerThanFive("Сгенерированное число - " + randomNum);
        }
    }
}
