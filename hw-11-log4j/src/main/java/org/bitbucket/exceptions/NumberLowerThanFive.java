package org.bitbucket.exceptions;

public class NumberLowerThanFive extends Exception {
    public NumberLowerThanFive(String errorMsg) {
        super(errorMsg);
    }
}
