package org.bitbucket.randnumber;

import java.security.SecureRandom;

public class RandomNumber {

    public static int randomNumberGenerator() {
        SecureRandom random = new SecureRandom();
        return random.nextInt(11);
    }
}
