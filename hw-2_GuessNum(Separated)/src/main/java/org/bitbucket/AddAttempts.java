package org.bitbucket;

import java.util.Scanner;

public class AddAttempts {
    static int addAttempts() {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int numForWrong0 = 1;
        if (a <= 0 || a > 15) {
            WrongNumber.wrongNum(numForWrong0);
            addAttempts();
        }
        return a;
    }
}
