package org.bitbucket;

public class StartGame {
    static void StartGame(int firstNum, int secondNum, int attempts){
        int attemptNum = 0;
        int userInpInt = 0;
        boolean continueGame = true;
        String out;
        int randNum = GenerateNumber.generateNumber(firstNum, secondNum);
        System.out.println(randNum + " - Вывод рандомного числа для легкой проверки программы");
        System.out.println("Случайное число сгенерировано, можно угадывать.\nЧто бы выйти, введи \"exit\"");
        while (continueGame) {
            attemptNum += 1;
            out = StringInput.UserStringInput();
            if(out.equals("exit")){
                System.out.println("Всего хорошего!");
                System.exit(0);
            } else {
                try {
                    userInpInt = Integer.parseInt(out);
                } catch (NumberFormatException e) {
                    System.out.println("НЕВЕРНАЯ КОМАНДА!\nВведите либо число либо \"exit\"");
                    userInpInt = 0;
                    StartGame(firstNum, secondNum, attempts);
                }
            }
            HowClose.howClose(userInpInt, randNum, attemptNum, attempts);
            if (userInpInt == randNum && attemptNum <= attempts) {
                PlayOrStop.playOrStop(firstNum, secondNum, attempts);
            } else if (userInpInt != randNum && attemptNum == attempts) {
                PlayOrStop.playOrStop(firstNum, secondNum, attempts);
            }
        }
    }
}
