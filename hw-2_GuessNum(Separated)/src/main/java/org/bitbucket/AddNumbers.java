package org.bitbucket;

import java.util.Scanner;

public class AddNumbers {
    static int addNums() {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int numForWrong1 = 0;
        if (a <= 0 || a > 200) {
            WrongNumber.wrongNum(numForWrong1);
            addNums();
        }
        return a;
    }
}
