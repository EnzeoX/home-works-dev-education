package org.bitbucket;

public class NewGame {
    static void newGame(int firstNum, int secondNum, int attempts) {
        String yesNo;
        System.out.println("Желаешь изменить диапазон отгадывания и количество попыток?\n Y - Да, меняем правила!\n N - Нет, играем так!");
        yesNo = StringInput.UserStringInput();
        if (yesNo.equals("Y") || yesNo.equals("y")) {
            System.out.println("Введи первое число (от 1 до 200)");
            firstNum = AddNumbers.addNums();
            System.out.println("Введи второе число (от 1 до 200)");
            secondNum = AddNumbers.addNums();
            System.out.println("Введи количество попыток (от 1 до 15)");
            attempts = AddAttempts.addAttempts();
        } else if (yesNo.equals("N") || yesNo.equals("n")) {
            System.out.println("Ну начнем!");
        }
        StartGame.StartGame(firstNum, secondNum,attempts);
    }
}
