package org.bitbucket;

public class GuessNumberMain {
    public static void main(String[] args) {
        boolean start = true;
        int min = 1;
        int max = 100;
        int attempts = 5;
        while (start) {
            System.out.println("Привет, я загадал число от " + min + " до " + max + ". Попробуй угадать его за " + attempts + " попыток!”");
            start = false;
        }
        NewGame.newGame(min, max, attempts);
    }
}
