package org.bitbucket;

public class WrongNumber {
    static void wrongNum(int a) {
        if (a == 1) {
            System.out.println("Число должно быть целым и в пределах от 1 до 15!\nВведите число заново.");
        } else {
            System.out.println("Число должно быть целым и в пределах от 1 до 200!\nВведите число заново.");
        }
    }
}
