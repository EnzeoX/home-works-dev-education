package org.bitbucket;

import java.util.Random;

public class GenerateNumber {
    static int generateNumber(int a, int b) {
        Random r = new Random();
        int result = 0;
        if (a > b) {
            result = r.nextInt(a - b) + b;
        } else if (b > a) {
            result = r.nextInt(b - a) + a;
        }
        return result;
    }
}
