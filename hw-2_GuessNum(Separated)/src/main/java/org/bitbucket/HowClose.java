package org.bitbucket;

public class HowClose {
    static void howClose(int userInpNum, int randomNum, int attemptNum, int attempts) {
        if (attemptNum < attempts) {
            if (Math.abs((randomNum - userInpNum)) > 30) {
                System.out.println("Очень холодно, попробуй еще. Оставшееся количество попыток на отгадку - " + (attempts - attemptNum));
            } else if (Math.abs((randomNum - userInpNum)) < 30 && Math.abs((randomNum - userInpNum)) > 20) {
                System.out.println("Холодно, пробуй еще. Оставшееся количество попыток на отгадку - " + (attempts - attemptNum));
            } else if (Math.abs((randomNum - userInpNum)) <= 20 && Math.abs((randomNum - userInpNum)) > 10) {
                System.out.println("Тепло, но ты как пенсионер требуешь больше тепла, пробуй еще! Оставшееся количество попыток на отгадку - " + (attempts - attemptNum));
            } else if (Math.abs((randomNum - userInpNum)) <= 10 && Math.abs((randomNum - userInpNum)) > 5) {
                System.out.println("Жарко, цель близко! Оставшееся количество попыток на отгадку - " + (attempts - attemptNum));
            } else if (Math.abs((randomNum - userInpNum)) <= 5 && Math.abs((randomNum - userInpNum)) >= 1) {
                System.out.println("Цель максимально близко! Оставшееся количество попыток на отгадку - " + (attempts - attemptNum));
            } else if (userInpNum == randomNum) {
                System.out.println("Это именно то число, отлично! Количество затраченых попыток на отгадку - " + attemptNum);
            }
        } else if(attemptNum == attempts){
            if(userInpNum == randomNum){
                System.out.println("Это именно то число, отлично! Количество затраченых попыток на отгадку - " + attemptNum);
            } else {
                System.out.println("Попыток на отгадывание больше не осталось.");
            }
        }
    }
}
