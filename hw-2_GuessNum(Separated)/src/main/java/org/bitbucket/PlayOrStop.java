package org.bitbucket;

public class PlayOrStop {
    static void playOrStop(int firstNum, int secondNum, int attempts) {
        System.out.println("Сыграем еще раз?\nY - Да\nN - Нет");
        String word = StringInput.UserStringInput();
        if (word.equals("Y") || word.equals("y")) {
            NewGame.newGame(firstNum, secondNum, attempts);
        } else if (word.equals("N") || word.equals("n")) {
            System.out.println("Всего хорошего!");
            System.exit(0);
        } else {
            System.out.println("Неверная команда!");
            playOrStop(firstNum, secondNum, attempts);
        }
    }
}
