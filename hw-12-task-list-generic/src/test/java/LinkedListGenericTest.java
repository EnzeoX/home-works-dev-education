import org.bitbucket.ILinkedListGeneric;
import org.bitbucket.LinkedArrayGeneric;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class LinkedListGenericTest {

    private final ILinkedListGeneric list = new LinkedArrayGeneric();

    //********************* Init test *******************************

    @Test
    public void initTestOne() {
        this.list.init(MockDataGeneric.getArrayContainsOne());
        Object[] exp = {69};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void initTestTwo() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        Object[] exp = {54, 98};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void initTestMany() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        Object[] exp = {1, 55, 69, 345, 2, 43, 90, 48, 20, 99};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void initTestArrayZero() {
        this.list.init(MockDataGeneric.getArrayContainsZero());
        Object[] exp = {};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void initTestArrayNull() {
        this.list.init(MockDataGeneric.getArrayContainsNull());
        Object[] exp = {};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //********************* Clear test ******************************

    @Test
    public void clearArrayMany() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        this.list.clear();
        Object[] exp = {};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearArrayOne() {
        this.list.init(MockDataGeneric.getArrayContainsOne());
        this.list.clear();
        Object[] exp = {};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearArrayTwo() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        this.list.clear();
        Object[] exp = {};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearArrayZero() {
        this.list.init(MockDataGeneric.getArrayContainsZero());
        this.list.clear();
        Object[] exp = {};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void clearArrayNull() {
        this.list.init(MockDataGeneric.getArrayContainsNull());
        this.list.clear();
        Object[] exp = {};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //******************* Array size test *************************

    @Test
    public void sizeArrayMany() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        Object act = this.list.size();
        Object exp = 10;
        assertEquals(exp, act);
    }

    @Test
    public void sizeArrayOne() {
        this.list.init(MockDataGeneric.getArrayContainsOne());
        Object act = this.list.size();
        Object exp = 1;
        assertEquals(exp, act);
    }

    @Test
    public void sizeArrayTwo() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        Object act = this.list.size();
        Object exp = 2;
        assertEquals(exp, act);
    }

    @Test
    public void sizeArrayDefault() {
        Object act = this.list.size();
        Object exp = 0;
        assertEquals(exp, act);
    }

    @Test
    public void sizeArrayZero() {
        this.list.init(MockDataGeneric.getArrayContainsZero());
        Object act = this.list.size();
        Object exp = 0;
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void sizeArrayNull() {
        this.list.init(MockDataGeneric.getArrayContainsNull());
        Object act = this.list.size();
    }

    //************** Add element to start *************************

    @Test
    public void addToStartOne() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        this.list.addStart(33);
        Object[] exp = {33, 54, 98};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addToStartTwo() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        this.list.addStart(33);
        this.list.addStart(49);
        Object[] exp = {49, 33, 54, 98};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addToStartMany() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        for (int i = 0; i < MockDataGeneric.elementsToAdd.length; i++) {
            this.list.addStart(MockDataGeneric.elementsToAdd[i]);
        }
        Object[] exp = {10, 1, 6, 4, 54, 98};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //************** Add elements to end **************************

    @Test
    public void addToEndOne() {
        this.list.clear();
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        this.list.addEnd(44);
        Object[] exp = {54, 98, 44};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addToEndTwo() {
        this.list.clear();
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        this.list.addEnd(44);
        this.list.addEnd(90);
        Object[] exp = {54, 98, 44, 90};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addToEndMany() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        for (int i = 0; i < MockDataGeneric.elementsToAdd.length; i++) {
            this.list.addEnd(MockDataGeneric.elementsToAdd[i]);
        }
        Object[] exp = {54, 98, 4, 6, 1, 10};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addToEndUntilExpand() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        for (int i = 0; i < MockDataGeneric.elementsToAdd.length; i++) {
            this.list.addEnd(MockDataGeneric.elementsToAdd[i]);
        }
        for (int i = 0; i < MockDataGeneric.elementsToAdd.length; i++) {
            this.list.addEnd(MockDataGeneric.elementsToAdd[i]);
        }
        Object[] exp = {54, 98, 4, 6, 1, 10, 4, 6, 1, 10};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //********** Add element test by index ********************

    @Test
    public void addByPosOne() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        this.list.addByPos(1, 3);
        Object[] exp = {54, 3, 98};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addByPosTwo() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        this.list.addByPos(0, 6);
        this.list.addByPos(1, 3);
        Object[] exp = {6, 3, 54, 98};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addByPosMany() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        for (int i = 0; i < MockDataGeneric.elementsToAdd.length; i++) {
            this.list.addByPos(i, MockDataGeneric.elementsToAdd[i]);
        }
        Object[] exp = {4, 6, 1, 10, 54, 98};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void addByPosDefaultArray() {
        this.list.addByPos(2, 69);
        Object[] exp = {0, 0, 69, 0, 0, 0, 0, 0, 0, 0};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void addByPosNullArray() {
        this.list.init(MockDataGeneric.getArrayContainsNull());
        this.list.addByPos(1, 69);
        Object[] exp = {0, 69, 0, 0, 0, 0, 0, 0, 0, 0};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void addByPosZeroArray() {
        this.list.init(MockDataGeneric.getArrayContainsZero());
        this.list.addByPos(1, 69);
    }

    //********** Remove element from start ********************

    @Test
    public void removeFromStartGetValue() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        Object act = this.list.removeStart();
        Object exp = 1;
        assertEquals(exp ,act);
    }

    @Test
    public void removeFromStartOne() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        this.list.removeStart();
        Object[] exp = {55, 69, 345, 2, 43, 90, 48, 20, 99};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeFromStartTwo() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        this.list.removeStart();
        this.list.removeStart();
        Object[] exp = {69, 345, 2, 43, 90, 48, 20, 99};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeFromStartMany() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        for (int i = 0; i < 5; i++) {
            this.list.removeStart();
        }
        Object[] exp = {43, 90, 48, 20, 99};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void removeFromStartOutOfBounds() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        this.list.removeStart();
        this.list.removeStart();
        this.list.removeStart();
        Object[] exp = {0, 0};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void removeFromStartZeroArray() {
        this.list.init(MockDataGeneric.getArrayContainsZero());
        this.list.removeStart();
    }

    @Test(expected = NullPointerException.class)
    public void removeFromStartNullArray() {
        this.list.init(MockDataGeneric.getArrayContainsNull());
        this.list.removeStart();
    }

    @Test(expected = NullPointerException.class)
    public void removeFromStartDefaultList() {
        this.list.removeStart();
    }

    //********** Remove from end element ********************

    @Test
    public void removeFromEndOne() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        this.list.removeEnd();
        Object[] exp = {1, 55, 69, 345, 2, 43, 90, 48, 20};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeFromEndTwo() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        this.list.removeEnd();
        this.list.removeEnd();
        Object[] exp = {1, 55, 69, 345, 2, 43, 90, 48};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeFromEndMany() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        int i = 5;
        while (i > 0) {
            this.list.removeEnd();
            i--;
        }
        Object[] exp = {1, 55, 69, 345, 2};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void removeFromEndOutOfBounds() {
        this.list.init(MockDataGeneric.getArrayContainsOne());
        this.list.removeEnd();
        this.list.removeEnd();
    }

    @Test(expected = NullPointerException.class)
    public void removeFromEndZero() {
        this.list.init(MockDataGeneric.getArrayContainsZero());
        this.list.removeEnd();
    }

    @Test(expected = NullPointerException.class)
    public void removeFromEndNull() {
        this.list.init(MockDataGeneric.getArrayContainsNull());
        this.list.removeEnd();
    }

    //********** Remove element by index  ********************

    @Test
    public void removeByPoseGetValue() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        Object act = this.list.removeByPos(5);
        Object exp = 43;
        assertEquals(exp, act);
    }

    @Test
    public void removeByPosOne() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        this.list.removeByPos(5);
        Object[] exp = {1, 55, 69, 345, 2, 90, 48, 20, 99};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeByPosTwo() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        this.list.removeByPos(5);
        this.list.removeByPos(3);
        Object[] exp = {1, 55, 69, 2, 90, 48, 20, 99};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeByPosMany() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        this.list.removeByPos(5);
        this.list.removeByPos(3);
        this.list.removeByPos(1);
        this.list.removeByPos(6);
        Object[] exp = {1, 69, 2, 90, 48, 20};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void removeByIndexOutOfBounds() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        this.list.removeByPos(11);
    }

    @Test(expected = NullPointerException.class)
    public void removeByIndexOneOutOfBounds() {
        this.list.init(MockDataGeneric.getArrayContainsOne());
        this.list.removeByPos(1);
        this.list.removeByPos(1);
    }

    @Test(expected = NullPointerException.class)
    public void removeByPosZero() {
        this.list.init(MockDataGeneric.getArrayContainsZero());
        this.list.removeByPos(1);
    }

    @Test(expected = NullPointerException.class)
    public void removeByPosDefaultArray() {
        this.list.removeByPos(1);
    }

    //*********** Get max element in array ****************

    @Test
    public void getMaxElement() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        Object exp = 345;
        Object act = this.list.max();
        assertEquals(exp, act);
    }

    @Test
    public void getMaxElementTwo() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        Object exp = 98;
        Object act = this.list.max();
        assertEquals(exp, act);
    }

    @Test
    public void getMaxElementOne() {
        this.list.init(MockDataGeneric.getArrayContainsOne());
        Object exp = 69;
        Object act = this.list.max();
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void getMaxArrayNull() {
        this.list.init(MockDataGeneric.getArrayContainsNull());
        this.list.max();
    }

    @Test(expected = NullPointerException.class)
    public void getMaxArrayZero() {
        this.list.init(MockDataGeneric.getArrayContainsZero());
        this.list.max();
    }

    @Test(expected = NullPointerException.class)
    public void getMaxArrayDefault() {
        this.list.max();
    }

    //********** Get min element in array *********************


    @Test
    public void getMinElement() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        Object exp = 1;
        Object act = this.list.min();
        assertEquals(exp, act);
    }

    @Test
    public void getMinElementTwo() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        Object exp = 54;
        Object act = this.list.min();
        assertEquals(exp, act);
    }

    @Test
    public void getMinElementOne() {
        this.list.init(MockDataGeneric.getArrayContainsOne());
        Object exp = 69;
        Object act = this.list.min();
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void getMinArrayNull() {
        this.list.init(MockDataGeneric.getArrayContainsNull());
        this.list.min();
    }

    @Test(expected = NullPointerException.class)
    public void getMinArrayZero() {
        this.list.init(MockDataGeneric.getArrayContainsZero());
        this.list.min();
    }

    @Test(expected = NullPointerException.class)
    public void getMinArrayDefault() {
        this.list.min();
    }

    //************* Get max element position ************

    @Test
    public void getMaxElementPosition() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        Object exp = 3;
        Object act = this.list.maxPos();
        assertEquals(exp, act);
    }

    @Test
    public void getMaxElementPositionTwo() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        Object exp = 1;
        Object act = this.list.maxPos();
        assertEquals(exp, act);
    }

    @Test
    public void getMaxElementPositionOne() {
        this.list.init(MockDataGeneric.getArrayContainsOne());
        Object exp = 0;
        Object act = this.list.maxPos();
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void getMaxElementPositionNull() {
        this.list.init(MockDataGeneric.getArrayContainsNull());
        this.list.maxPos();
    }

    @Test(expected = NullPointerException.class)
    public void getMaxElementPositionZero() {
        this.list.init(MockDataGeneric.getArrayContainsZero());
        this.list.maxPos();
    }

    @Test(expected = NullPointerException.class)
    public void getMaxElementDefaultArray() {
        this.list.maxPos();
    }

    //************* Get max element position ************

    @Test
    public void getMinElementPosition() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        Object exp = 0;
        Object act = this.list.minPos();
        assertEquals(exp, act);
    }

    @Test
    public void getMinElementPositionTwo() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        Object exp = 0;
        Object act = this.list.minPos();
        assertEquals(exp, act);
    }

    @Test
    public void getMinElementPositionOne() {
        this.list.init(MockDataGeneric.getArrayContainsOne());
        Object exp = 0;
        Object act = this.list.minPos();
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void getMinElementPositionNull() {
        this.list.init(MockDataGeneric.getArrayContainsNull());
        this.list.minPos();
    }

    @Test(expected = NullPointerException.class)
    public void getMinElementPositionZero() {
        this.list.init(MockDataGeneric.getArrayContainsZero());
        this.list.minPos();
    }

    //********** Sort array *********************

    @Test
    public void sortArrayMany() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        Object[] exp = {1, 2, 20, 43, 48, 55, 69, 90, 99, 345};
        Object[] act = this.list.sort();
        assertArrayEquals(exp, act);
    }

    @Test
    public void sortArrayTwo() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        Object[] exp = {54, 98};
        Object[] act = this.list.sort();
        assertArrayEquals(exp, act);
    }

    @Test
    public void sortArrayOne() {
        this.list.init(MockDataGeneric.getArrayContainsOne());
        Object[] exp = {69};
        Object[] act = this.list.sort();
        assertArrayEquals(exp, act);
    }

    @Test
    public void sortArrayNull() {
        this.list.init(MockDataGeneric.getArrayContainsNull());
        Object[] exp = {};
        Object[] act = this.list.sort();
        assertArrayEquals(exp, act);
    }

    @Test
    public void sortArrayZero() {
        this.list.init(MockDataGeneric.getArrayContainsZero());
        Object[] exp = {};
        Object[] act = this.list.sort();
        assertArrayEquals(exp, act);
    }

    @Test
    public void sortDefaultArray() {
        Object[] exp = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        Object[] act = this.list.sort();
        assertArrayEquals(exp, act);
    }

    //********** Get element by pos ***************

    @Test
    public void getElementByPosManyFirst() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        Object exp = 69;
        Object act = this.list.get(2);
        assertEquals(exp, act);
    }

    @Test
    public void getElementByPosManySecond() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        Object exp = 345;
        Object act = this.list.get(3);
        assertEquals(exp, act);
    }

    @Test
    public void getElementByPosTwo() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        Object exp = 98;
        Object act = this.list.get(1);
        assertEquals(exp, act);
    }

    @Test
    public void getElementByPosOne() {
        this.list.init(MockDataGeneric.getArrayContainsOne());
        Object exp = 69;
        Object act = this.list.get(0);
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void getElementByPosManyOutOfBounds() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        Object exp = 0;
        Object act = this.list.get(12);
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void getElementByPosTwoOutOfBounds() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        Object exp = 0;
        Object act = this.list.get(5);
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void getElementByPosNull() {
        this.list.init(MockDataGeneric.getArrayContainsNull());
        Object exp = 0;
        Object act = this.list.get(4);
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void getElementByPosZero() {
        this.list.init(MockDataGeneric.getArrayContainsZero());
        Object exp = 0;
        Object act = this.list.get(4);
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void getElementByPosDefaultArray() {
        Object exp = 0;
        Object act = this.list.get(4);
        assertEquals(exp, act);
    }

    //************ Reverse array ***********************

    @Test
    public void halfReversMany() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        Object[] exp = {43, 90, 48, 20, 99, 1, 55, 69, 345, 2};
        this.list.halfRevers();
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversOne() {
        this.list.init(MockDataGeneric.getArrayContainsOne());
        Object[] exp = {69};
        this.list.halfRevers();
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversTwo() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        Object[] exp = {98, 54};
        this.list.halfRevers();
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversSeven() {
        this.list.init(MockDataGeneric.getArrayContainsSevenElements());
        Object[] exp = {1, 5, 9, 3, 8, 4, 2};
        this.list.halfRevers();
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReverseNull() {
        this.list.init(MockDataGeneric.getArrayContainsNull());
        Object[] exp = {};
        this.list.halfRevers();
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReverseZero() {
        this.list.init(MockDataGeneric.getArrayContainsZero());
        Object[] exp = {};
        this.list.halfRevers();
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReverseDefaultArray() {
        Object[] exp = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0,};
        this.list.halfRevers();
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //************ Full reverse array ***********************

    @Test
    public void fullReversMany() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        Object[] exp = {99, 20, 48, 90, 43, 2, 345, 69, 55, 1};
        this.list.fullRevers();
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //{1, 55, 69, 345, 2, 43, 90, 48, 20, 99};
    @Test
    public void fullReverseTwo() {
        this.list.init(MockDataGeneric.getArrayContainsTwo());
        Object[] exp = {98, 54};
        this.list.fullRevers();
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void fullReverseOne() {
        this.list.init(MockDataGeneric.getArrayContainsOne());
        Object[] exp = {69};
        this.list.fullRevers();
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void fullReverseSeven() {
        this.list.init(MockDataGeneric.getArrayContainsSevenElements());
        Object[] exp = {3, 9, 5, 1, 2, 4, 8};
        this.list.fullRevers();
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void fullReverseZero() {
        this.list.init(MockDataGeneric.getArrayContainsZero());
        Object[] exp = {};
        this.list.fullRevers();
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void fullReverseNull() {
        this.list.init(MockDataGeneric.getArrayContainsNull());
        Object[] exp = {};
        this.list.fullRevers();
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void fullReversDefaultArray() {
        Object[] exp = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        this.list.fullRevers();
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //************ Set element in array ****************

    @Test
    public void setElementOne() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        this.list.set(4, 89);
        Object[] exp = {1, 55, 69, 345, 89, 43, 90, 48, 20, 99};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void setElementTwo() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        this.list.set(1, 89);
        this.list.set(4, 52);
        Object[] exp = {1, 89, 69, 345, 52, 43, 90, 48, 20, 99};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void setElementMany() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        this.list.set(1, 89);
        this.list.set(3, 88);
        this.list.set(6, 70);
        this.list.set(8, 9);
        Object[] exp = {1, 89, 69, 88, 2, 43, 70, 48, 9, 99};
        Object[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void setElementZero() {
        this.list.init(MockDataGeneric.getArrayContainsMany());
        this.list.set(14, 2);
    }

    @Test(expected = NullPointerException.class)
    public void setElementZeroArray() {
        this.list.init(MockDataGeneric.getArrayContainsZero());
        this.list.set(2, 10);
    }

    @Test(expected = NullPointerException.class)
    public void setElementDefaultArray() {
        this.list.set(3, 10);
    }
}
