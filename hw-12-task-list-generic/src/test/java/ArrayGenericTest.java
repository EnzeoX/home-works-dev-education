import org.bitbucket.ArrayGeneric;
import org.bitbucket.IListGeneric;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ArrayGenericTest {

    private final IListGeneric<Integer> iListGeneric = new ArrayGeneric();

    // *************** Init test *********************

    @Test
    public void initArrayMany() {
        iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        Object[] exp = {1, 55, 69, 345, 2, 43, 90, 48, 20, 99};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void initArrayTwo() {
        iListGeneric.init(MockDataGeneric.getArrayContainsTwo());
        Object[] exp = {54, 98};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void initArrayOne() {
        iListGeneric.init(MockDataGeneric.getArrayContainsOne());
        Object[] exp = {69};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void initArrayZero() {
        iListGeneric.init(MockDataGeneric.getArrayContainsZero());
        Object[] exp = {};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void initArrayNull() {
        iListGeneric.init(MockDataGeneric.getArrayContainsNull());
        Object[] exp = {};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void initArrayDefault() {
        Object[] exp = {};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    //****************** Clear test ************************

    @Test
    public void clearArrayMany() {
        iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        Object[] exp = {};
        this.iListGeneric.clear();
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearArrayTwo() {
        iListGeneric.init(MockDataGeneric.getArrayContainsTwo());
        Object[] exp = {};
        this.iListGeneric.clear();
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearArrayOne() {
        iListGeneric.init(MockDataGeneric.getArrayContainsOne());
        Object[] exp = {};
        this.iListGeneric.clear();
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearArrayZero() {
        iListGeneric.init(MockDataGeneric.getArrayContainsZero());
        Object[] exp = {};
        this.iListGeneric.clear();
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearArrayNull() {
        iListGeneric.init(MockDataGeneric.getArrayContainsNull());
        Object[] exp = {};
        this.iListGeneric.clear();
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearArrayDefault() {
        Object[] exp = {};
        this.iListGeneric.clear();
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    //******************* Array size ******************************

    @Test
    public void sizeArrayMany() {
        iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        int exp = 10;
        int act = this.iListGeneric.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeArrayTwo() {
        iListGeneric.init(MockDataGeneric.getArrayContainsTwo());
        int exp = 2;
        int act = this.iListGeneric.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeArrayOne() {
        iListGeneric.init(MockDataGeneric.getArrayContainsOne());
        int exp = 1;
        int act = this.iListGeneric.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeArrayZero() {
        iListGeneric.init(MockDataGeneric.getArrayContainsZero());
        int exp = 0;
        int act = this.iListGeneric.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeArrayNull() {
        iListGeneric.init(MockDataGeneric.getArrayContainsNull());
        int exp = 0;
        int act = this.iListGeneric.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeArrayDefault() {
        int exp = 0;
        int act = this.iListGeneric.size();
        assertEquals(exp, act);
    }

    //***************** Add value to start *************************

    @Test
    public void addStartArrayMany() {
        iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        iListGeneric.addStart(9);
        iListGeneric.addStart(44);
        iListGeneric.addStart(11);
        iListGeneric.addStart(22);
        Object[] exp = {22, 11, 44, 9, 1, 55, 69, 345, 2, 43, 90, 48, 20, 99};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addStartArrayTwo() {
        iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        iListGeneric.addStart(5);
        iListGeneric.addStart(8);
        Object[] exp = {8, 5, 1, 55, 69, 345, 2, 43, 90, 48, 20, 99};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addStartArrayOne() {
        iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        iListGeneric.addStart(91);
        Object[] exp = {91, 1, 55, 69, 345, 2, 43, 90, 48, 20, 99};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addStartToArrayNull() {
        iListGeneric.init(MockDataGeneric.getArrayContainsNull());
        iListGeneric.addStart(3);
        Object[] exp = {3};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addStartToArrayZero() {
        iListGeneric.init(MockDataGeneric.getArrayContainsZero());
        iListGeneric.addStart(3);
        Object[] exp = {3};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addStartToArrayDefault() {
        iListGeneric.addStart(3);
        Object[] exp = {3};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    //******************* Add value to end **************************

    @Test
    public void addEndToArrayMany() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        this.iListGeneric.addEnd(3);
        this.iListGeneric.addEnd(4);
        this.iListGeneric.addEnd(5);
        this.iListGeneric.addEnd(6);
        Object[] exp = {1, 55, 69, 345, 2, 43, 90, 48, 20, 99, 3, 4, 5, 6};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addEndToArrayTwo() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        this.iListGeneric.addEnd(3);
        this.iListGeneric.addEnd(9);
        Object[] exp = {1, 55, 69, 345, 2, 43, 90, 48, 20, 99, 3, 9};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addEndToArrayOne() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        this.iListGeneric.addEnd(2);
        Object[] exp = {1, 55, 69, 345, 2, 43, 90, 48, 20, 99, 2};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addEndToArrayZero() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsZero());
        this.iListGeneric.addEnd(1);
        Object[] exp = {1};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addEndToArrayNull() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsNull());
        this.iListGeneric.addEnd(2);
        Object[] exp = {2};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addEndToArrayDefault() {
        this.iListGeneric.addEnd(3);
        Object[] exp = {3};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    //***************** Add value by position **************************

    @Test
    public void addValueByPosMany() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsTwo());
        this.iListGeneric.addByPos(1, 12);
        this.iListGeneric.addByPos(1, 14);
        this.iListGeneric.addByPos(2, 15);
        this.iListGeneric.addByPos(4, 16);
        Object[] exp = {54, 14, 15, 12, 16, 98};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addValueByPosTwo() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsTwo());
        this.iListGeneric.addByPos(1, 10);
        this.iListGeneric.addByPos(2, 9);
        Object[] exp = {54, 10, 9, 98};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addValueByPosOne() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsTwo());
        this.iListGeneric.addByPos(2, 10);
        Object[] exp = {54, 98, 10};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void addValueByPosArrayZero() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsZero());
        this.iListGeneric.addByPos(1, 10);
        Object[] exp = {10};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addValueByPosArrayNull() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsNull());
        this.iListGeneric.addByPos(0, 10);
        Object[] exp = {10};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void addValueByPosDefaultArray() {
        this.iListGeneric.addByPos(1, 10);
        Object[] exp = {10};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void addValueByPosOutOfBounds() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsTwo());
        this.iListGeneric.addByPos(12, 3);
        Object[] exp = {};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    //******************* Remove value from start *********************

    @Test
    public void removeValueFromStartMany() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        this.iListGeneric.removeStart();
        this.iListGeneric.removeStart();
        this.iListGeneric.removeStart();
        this.iListGeneric.removeStart();
        Object[] exp = {2, 43, 90, 48, 20, 99};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeValueFromStartTwo() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        this.iListGeneric.removeStart();
        this.iListGeneric.removeStart();
        Object[] exp = {69, 345, 2, 43, 90, 48, 20, 99};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeValueFromStartOne() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        this.iListGeneric.removeStart();
        Object[] exp = {55, 69, 345, 2, 43, 90, 48, 20, 99};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void removeValueFromStartZeroArray() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsZero());
        this.iListGeneric.removeStart();
    }

    @Test(expected = NullPointerException.class)
    public void removeValueFromStartNullArray() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsNull());
        this.iListGeneric.removeStart();
    }

    @Test(expected = NullPointerException.class)
    public void removeValueFromStartDefaultArray() {
        this.iListGeneric.removeStart();
    }

    @Test(expected = NullPointerException.class)
    public void removeValueFromStartUntilException() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsTwo());
        this.iListGeneric.removeStart();
        this.iListGeneric.removeStart();
        this.iListGeneric.removeStart();
    }

    //***************** Remove from end value *************************

    @Test
    public void removeFromEndMany() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        this.iListGeneric.removeEnd();
        this.iListGeneric.removeEnd();
        this.iListGeneric.removeEnd();
        this.iListGeneric.removeEnd();
        Object[] exp = {1, 55, 69, 345, 2, 43};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeFromEndTwo() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        this.iListGeneric.removeEnd();
        this.iListGeneric.removeEnd();
        Object[] exp = {1, 55, 69, 345, 2, 43, 90, 48};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeFromEndOne() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        this.iListGeneric.removeEnd();
        Object[] exp = {1, 55, 69, 345, 2, 43, 90, 48, 20};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void removeFromEndZeroArray() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsZero());
        this.iListGeneric.removeEnd();
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void removeFromEndNullArray() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsNull());
        this.iListGeneric.removeEnd();
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void removeFromEndDefaultArray() {
        this.iListGeneric.removeEnd();
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void removeFromEndUntilException() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsTwo());
        this.iListGeneric.removeEnd();
        this.iListGeneric.removeEnd();
        this.iListGeneric.removeEnd();
    }

    //*********************** Remove by position ************************

    @Test
    public void removeByPosMany() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        this.iListGeneric.removeByPos(2);
        this.iListGeneric.removeByPos(4);
        this.iListGeneric.removeByPos(3);
        this.iListGeneric.removeByPos(1);
        Object[] exp = {1, 345, 90, 48, 20, 99};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeByPosTwo() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        this.iListGeneric.removeByPos(2);
        this.iListGeneric.removeByPos(4);
        Object[] exp = {1, 55, 345, 2, 90, 48, 20, 99};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeByPosOne() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        this.iListGeneric.removeByPos(2);
        Object[] exp = {1, 55, 345, 2, 43, 90, 48, 20, 99};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void removeByPosArrayZero() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsZero());
        this.iListGeneric.removeByPos(2);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void removeByPosArrayNull() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsNull());
        this.iListGeneric.removeByPos(2);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void removeByPosArrayDefault() {
        this.iListGeneric.removeByPos(2);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void removeByPosUntilException() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsTwo());
        this.iListGeneric.removeByPos(0);
        this.iListGeneric.removeByPos(0);
        this.iListGeneric.removeByPos(0);
        this.iListGeneric.removeByPos(0);
    }

    //************************* Max value ****************************

    @Test
    public void maxValueMany() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        Object act = this.iListGeneric.max();
        Object exp = 345;
        assertEquals(exp, act);
    }

    @Test
    public void maxValueTwo() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsTwo());
        Object act = this.iListGeneric.max();
        Object exp = 98;
        assertEquals(exp, act);
    }

    @Test
    public void maxValueOne() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsOne());
        Object act = this.iListGeneric.max();
        Object exp = 69;
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void maxValueArrayZero() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsZero());
        Object act = this.iListGeneric.max();
    }

    @Test(expected = NullPointerException.class)
    public void maxValueArrayNull() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsNull());
        Object act = this.iListGeneric.max();
    }

    @Test(expected = NullPointerException.class)
    public void maxValueArrayDefault() {
        Object act = this.iListGeneric.max();
    }

    //********************* Min value **********************************

    @Test
    public void minValueMany() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        Object act = this.iListGeneric.min();
        Object exp = 1;
        assertEquals(exp, act);
    }

    @Test
    public void minValueTwo() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsTwo());
        Object act = this.iListGeneric.min();
        Object exp = 54;
        assertEquals(exp, act);
    }

    @Test
    public void minValueOne() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsOne());
        Object act = this.iListGeneric.min();
        Object exp = 69;
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void minValueArrayZero() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsZero());
        Object act = this.iListGeneric.min();
    }

    @Test(expected = NullPointerException.class)
    public void minValueArrayNull() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsNull());
        Object act = this.iListGeneric.min();
    }

    @Test(expected = NullPointerException.class)
    public void minValueArrayDefault() {
        Object act = this.iListGeneric.min();
    }

    //******************** Get pos of max element **********************

    @Test
    public void getPosMaxElementMany() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        Object act = this.iListGeneric.maxPos();
        Object exp = 3;
        assertEquals(exp, act);
    }

    @Test
    public void getPosMaxElementTwo() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsTwo());
        Object act = this.iListGeneric.maxPos();
        Object exp = 1;
        assertEquals(exp, act);
    }

    @Test
    public void getPosMaxElementOne() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsOne());
        Object act = this.iListGeneric.maxPos();
        Object exp = 0;
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void getPosMaxElementArrayZero() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsZero());
        Object act = this.iListGeneric.maxPos();
    }

    @Test(expected = NullPointerException.class)
    public void getPosMaxElementArrayNull() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsNull());
        Object act = this.iListGeneric.maxPos();
    }

    @Test(expected = NullPointerException.class)
    public void getPosMaxElementArrayDefault() {
        Object act = this.iListGeneric.maxPos();
    }

    //**************** get pos of min element ************************

    @Test
    public void getMinPoseMany() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        Object act = this.iListGeneric.minPos();
        Object exp = 0;
        assertEquals(exp, act);
    }

    @Test
    public void getMinPoseTwo() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsTwo());
        Object act = this.iListGeneric.minPos();
        Object exp = 0;
        assertEquals(exp, act);
    }

    @Test
    public void getMinPoseOne() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsOne());
        Object act = this.iListGeneric.minPos();
        Object exp = 0;
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void getMinPoseArrayZero() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsZero());
        Object act = this.iListGeneric.minPos();
    }

    @Test(expected = NullPointerException.class)
    public void getMinPoseArrayNull() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsNull());
        Object act = this.iListGeneric.minPos();
    }

    @Test(expected = NullPointerException.class)
    public void getMinPoseArrayDefault() {
        Object act = this.iListGeneric.minPos();
    }

    //******************* Sort array *********************************

    @Test
    public void sortArrayMany() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        this.iListGeneric.sort();
        Object[] exp = {1, 2, 20, 43, 48, 55, 69, 90, 99, 345};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void sortArrayTwo() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsTwo());
        this.iListGeneric.sort();
        Object[] exp = {54, 98};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void sortArrayOne() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsOne());
        this.iListGeneric.sort();
        Object[] exp = {69};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void sortArrayNull() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsNull());
        Object[] exp = {};
        Object[] act = this.iListGeneric.sort();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void sortArrayZero() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsZero());
        Object[] exp = {};
        Object[] act = this.iListGeneric.sort();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void sortDefaultArray() {
        Object[] exp = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        Object[] act = this.iListGeneric.sort();
        assertArrayEquals(exp, act);
    }

    //********************* Get element by pos *************************

    @Test
    public void getElementByPosArrayMany() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        Object exp = 69;
        Object act = this.iListGeneric.get(2);
        assertEquals(exp ,act);
    }

    @Test
    public void getElementByPosArrayTwo() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsTwo());
        Object exp = 54;
        Object act = this.iListGeneric.get(0);
        assertEquals(exp ,act);
    }

    @Test
    public void getElementByPosArrayOne() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsOne());
        Object exp = 69;
        Object act = this.iListGeneric.get(0);
        assertEquals(exp ,act);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void getElementByPosArrayZero() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsZero());
        Object act = this.iListGeneric.get(2);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void getElementByPosArrayNull() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsNull());
        Object act = this.iListGeneric.get(2);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void getElementByPosArrayDefault() {
        Object act = this.iListGeneric.get(2);
    }

    //********************** Half reverse array **********************

    @Test
    public void halfReverseArrayOne() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsOne());
        this.iListGeneric.halfRevers();
        Object[] exp = {69};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp ,act);
    }

    @Test
    public void halfReverseArrayTwo() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsTwo());
        this.iListGeneric.halfRevers();
        Object[] exp = {98, 54};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp ,act);
    }

    @Test
    public void halfReverseArrayMany() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        Object[] exp = {43, 90, 48, 20, 99, 1, 55, 69, 345, 2};
        Object[] act = this.iListGeneric.halfRevers();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void halfReverseArrayZero() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsZero());
        this.iListGeneric.halfRevers();
    }

    @Test(expected = NullPointerException.class)
    public void halfReverseArrayNull() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsNull());
        this.iListGeneric.halfRevers();
    }

    @Test(expected = NullPointerException.class)
    public void halfReverseArrayDefault() {
        this.iListGeneric.halfRevers();
    }

    //********************* Full reverse array *************************

    @Test
    public void fullReverseArrayMany() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        Object[] exp = {99, 20, 48, 90, 43, 2, 345, 69, 55, 1};
        Object[] act = this.iListGeneric.fullRevers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void fullReverseArrayTwo() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsTwo());
        Object[] exp = {98, 54};
        Object[] act = this.iListGeneric.fullRevers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void fullReverseOne() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsOne());
        Object[] exp = {69};
        Object[] act = this.iListGeneric.fullRevers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void fullReverseSeven() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsSevenElements());
        Object[] exp = {3, 9, 5, 1, 2, 4, 8};
        Object[] act = this.iListGeneric.fullRevers();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void fullReverseZero() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsZero());
        Object[] exp = {};
        Object[] act = this.iListGeneric.fullRevers();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void fullReverseNull() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsNull());
        Object[] exp = {};
        Object[] act = this.iListGeneric.fullRevers();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void fullReversDefaultArray() {
        Object[] exp = {};
        Object[] act = this.iListGeneric.fullRevers();
        assertArrayEquals(exp, act);
    }

    //****************** Set element by position ***********************

    @Test
    public void setElementByPosArrayMany() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        this.iListGeneric.set(3, 51);
        this.iListGeneric.set(2, 39);
        this.iListGeneric.set(5, 5);
        this.iListGeneric.set(1, 4);
        Object[] exp = {1, 4, 39, 51, 2, 5, 90, 48, 20, 99};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp ,act);
    }

    @Test
    public void setElementByPosArrayTwo() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        this.iListGeneric.set(3, 51);
        this.iListGeneric.set(2, 39);
        Object[] exp = {1, 55, 39, 51, 2, 43, 90, 48, 20, 99};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp ,act);
    }

    @Test
    public void setElementByPosArrayOne() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsMany());
        this.iListGeneric.set(3, 51);
        Object[] exp = {1, 55, 69, 51, 2, 43, 90, 48, 20, 99};
        Object[] act = this.iListGeneric.toArray();
        assertArrayEquals(exp ,act);
    }

    @Test(expected = NullPointerException.class)
    public void setElementArrayZero() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsZero());
        this.iListGeneric.set(4, 51);
    }

    @Test(expected = NullPointerException.class)
    public void setElementArrayNull() {
        this.iListGeneric.init(MockDataGeneric.getArrayContainsNull());
        this.iListGeneric.set(4, 51);
    }

    @Test(expected = NullPointerException.class)
    public void setElementArrayDefault() {
        this.iListGeneric.set(4, 51);
    }

}
