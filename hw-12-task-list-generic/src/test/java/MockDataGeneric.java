public class MockDataGeneric {

    public static Integer[] arrayContainsMany = {1, 55, 69, 345, 2, 43, 90, 48, 20, 99};

    public static Integer[] arrayContainsSevenElements = {8, 4, 2, 1, 5, 9, 3};

    public static Integer[] arrayContainsOne = {69};

    public static Integer[] arrayContainsTwo = {54, 98};

    public static Integer[] arrayContainsZero = {};

    public static Integer[] arrayContainsNull = null;

    public static Integer[] elementsToAdd = {4, 6, 1, 10};

    public static Integer[] getArrayContainsMany() {
        return arrayContainsMany;
    }

    public static Integer[] getArrayContainsOne() {
        return arrayContainsOne;
    }

    public static Integer[] getArrayContainsTwo() {
        return arrayContainsTwo;
    }

    public static Integer[] getArrayContainsZero() {
        return arrayContainsZero;
    }

    public static Integer[] getArrayContainsNull() {
        return arrayContainsNull;
    }

    public static Integer[] getArrayContainsSevenElements() {
        return arrayContainsSevenElements;
    }
}
