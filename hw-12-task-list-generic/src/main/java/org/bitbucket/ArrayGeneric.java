package org.bitbucket;

public class ArrayGeneric<T extends Comparable<T>> implements IListGeneric<T> {

    private int capacity = 10;

    private Object[] array = new Object[capacity];

    private int index;

    private void expandArray() {
        int newCapacity;
        if (this.capacity < 10) {
            newCapacity = 10;
        } else {
            newCapacity = (int) (this.capacity + Math.ceil(this.capacity * 0.7));
        }
        this.capacity = newCapacity;
        Object[] newArray = new Object[newCapacity];
        for (int i = 0; i < this.array.length; i++) {
            newArray[i] = this.array[i];
        }
        this.array = newArray;
    }

    @Override
    public void init(T[] init) {
        if (init == null) {
            this.index = 0;
        } else {
            this.index = init.length;
            for (int i = 0; i < this.index; i++) {
                this.array[i] = init[i];
            }
        }
    }

    @Override
    public void clear() {
        this.index = 0;
    }

    @Override
    public int size() {
        return this.index;
    }

    @Override
    public <T> T[] toArray() {
        Object[] newArray = new Object[this.index];
        for (int i = 0; i < this.index; i++) {
            newArray[i] = array[i];
        }
        return (T[]) newArray;
    }

    @Override
    public void addStart(int value) {
        if (this.index + 1 >= (int) Math.ceil(this.capacity * 0.7)) {
            expandArray();
        }
        for (int i = this.index; i > 0; i--) {
            this.array[i] = this.array[i - 1];
        }
        this.array[0] = value;
        this.index++;
    }

    @Override
    public void addEnd(int value) {
        if (this.index + 1 >= (int) Math.ceil(this.capacity * 0.7)) {
            expandArray();
        }
        this.array[index] = value;
        this.index++;
    }

    @Override
    public void addByPos(int pos, int value) {
        if (this.array == null) {
            throw new ArrayIndexOutOfBoundsException("Array is null");
        } else if (pos > this.index) {
            throw new ArrayIndexOutOfBoundsException("Position is out of bounds");
        } else {
            if (this.index + 1 >= (int) Math.ceil(this.capacity * 0.7)) {
                expandArray();
            }
            for (int i = this.index; i > pos; i--) {
                this.array[i] = this.array[i - 1];
            }
            this.array[pos] = value;
            this.index++;
        }
    }

    @Override
    public T removeStart() {
        if (this.index == 0)
            throw new NullPointerException("Array is null");
        Object removedValue = array[0];
        for (int i = 0; i < index; i++) {
            if (i + 1 != this.index) {
                this.array[i] = this.array[i + 1];
            } else {
                this.array[i] = 0;
            }
        }
        this.index--;
        return (T) removedValue;
    }

    @Override
    public T removeEnd() {
        if (this.array == null || this.capacity == 0 || this.index == 0) {
            throw new ArrayIndexOutOfBoundsException("Array is null");
        }
        Object removedValue = this.array[index - 1];
        this.array[index - 1] = 0;
        this.index--;
        return (T) removedValue;
    }

    @Override
    public T removeByPos(int value) {
        if (value > this.index) {
            throw new ArrayIndexOutOfBoundsException("Index is out of bounds");
        }
        Object removedValue = this.array[value];
        for (int i = value; i < this.index; i++) {
            if (i + 1 == this.index) {
                this.array[i] = 0;
                break;
            }
            this.array[i] = this.array[i + 1];
        }
        this.index--;
        return (T) removedValue;
    }

    @Override
    public T max() {
        if (this.index == 0) {
            throw new NullPointerException("Array is null");
        }
        Object maxValue = array[0];
        for (int i = 0; i < this.index; i++) {
            if ((Integer) maxValue < (Integer) array[i]) {
                maxValue = array[i];
            }
        }
        return (T) maxValue;
    }

    @Override
    public T min() {
        if (this.index == 0) {
            throw new NullPointerException("Array is null");
        }
        Object minValue = array[0];
        for (int i = 0; i < this.index; i++) {
            if ((Integer) minValue > (Integer) array[i]) {
                minValue = array[i];
            }
        }
        return (T) minValue;
    }

    @Override
    public T maxPos() {
        if (this.index == 0) {
            throw new NullPointerException("Array is null");
        }
        Object position = 0;
        Object maxValue = array[0];
        for (int i = 0; i < this.index; i++) {
            if ((Integer) maxValue < (Integer) array[i]) {
                maxValue = array[i];
                position = i;
            }
        }
        return (T) position;
    }

    @Override
    public T minPos() {
        if (this.index == 0) {
            throw new NullPointerException("Array is null");
        }
        Object position = 0;
        Object minValue = array[0];
        for (int i = 0; i < this.index; i++) {
            if ((Integer) minValue > (Integer) array[i]) {
                minValue = array[i];
                position = i;
            }
        }
        return (T) position;
    }

    @Override
    public <T> T[] sort() {
        if (this.array == null || this.index == 0 || this.capacity == 0) {
            throw new NullPointerException("Array is null");
        }
        quickSort(this.array, 0, this.index - 1);
        return (T[]) this.array;
    }

    @Override
    public T get(int pos) {
        if (pos > this.index) {
            throw new ArrayIndexOutOfBoundsException("Position out of bounds");
        } else if (this.capacity == 0) {
            throw new NullPointerException("Array is null");
        }
        return (T) this.array[pos];
    }

    @Override
    public <T> T[] halfRevers() {
        if (this.index == 1)
            return this.toArray();
        if (this.index == 0)
            throw new NullPointerException("Array is null");
        Object[] reversArray = new Object[index];
        int evenNum = 1;
        if (this.index % 2 == 0) {
            evenNum = 0;
        }
        for (int i = 0; i < this.index / 2 + evenNum; i++) {
            reversArray[i] = this.array[this.index / 2 + i];
        }
        for (int j = 0; j < this.index / 2; j++) {
            reversArray[this.index / 2 + j + evenNum] = this.array[j];
        }
        return (T[]) reversArray;
    }

    @Override
    public <T> T[] fullRevers() {
        if (this.index == 1) {
            return this.toArray();
        }
        if(this.index == 0 || this.capacity == 0) {
            throw new NullPointerException("Array is null");
        }
        Object[] reversArray = new Object[index];
        for (int i = 0; i < index; i++) {
            reversArray[i] = this.array[index - i - 1];
        }
        return (T[]) reversArray;
    }

    @Override
    public void set(int pos, int value) {
        if(this.index == 0)
            throw new NullPointerException("Array is null");
        array[pos] = value;
    }

    private static void quickSort(Object[] array, int low, int high) {
        if (array.length == 0)
            return;
        if (low >= high)
            return;
        int middle = low + (high - low) / 2;
        int pivot = (Integer) array[middle];

        int i = low, j = high;
        while (i <= j) {
            while ((Integer) array[i] < pivot) {
                i++;
            }
            while ((Integer) array[j] > pivot) {
                j--;
            }
            if (i <= j) {
                Object temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }
        if (low < j)
            quickSort(array, low, j);
        if (high > i)
            quickSort(array, i, high);
    }
}
