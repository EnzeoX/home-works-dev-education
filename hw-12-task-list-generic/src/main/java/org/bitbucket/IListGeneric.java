package org.bitbucket;

public interface IListGeneric<T extends Comparable<T>> {

    void init(T[] init);

    void clear();

    int size();

    <T> T[] toArray();

    void addStart(int value);

    void addEnd(int value);

    void addByPos(int pos, int value);

    T removeStart();

    T removeEnd();

    T removeByPos(int value);

    T max();

    T min();

    T maxPos();

    T minPos();

    <T> T[] sort();

    T get(int pos);

    <T> T[] halfRevers();

    <T> T[] fullRevers();

    void set(int pos, int value);
}
