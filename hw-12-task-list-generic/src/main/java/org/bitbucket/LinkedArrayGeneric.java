package org.bitbucket;

import java.util.Objects;

public class LinkedArrayGeneric<T extends Comparable<T>> implements ILinkedListGeneric {

    private Node<T> head;

    private Node<T> last;

    public static class Node<T> {

        T value;

        Node<T> next = null;

        Node<T> previous = null;
    }

    @Override
    public void init(Comparable[] init) {
        if(Objects.nonNull(init)) {
            for (int i = 0; i < init.length; i++) {
                addEnd(init[i]);
            }
        } else {
            throw new NullPointerException("Array is null");
        }
    }

    @Override
    public void clear() {
        this.head = null;
    }

    @Override
    public int size() {
        int size = 0;
        Node<T> buffer = this.head;
        while (buffer != null) {
            buffer = buffer.next;
            size++;
        }
        return size;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void addStart(Comparable value) {
        Node<T> n = new Node<T>();
        n.value = (T)value;
        if (this.head == null) {
            this.head = n;
        } else {
            n.next = this.head;
            this.head = n;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void addEnd(Comparable value) {
        Node<T> n = new Node<T>();
        n.value = (T) value;
        if (Objects.isNull(this.head)) {
            this.head = n;
        } else {
            Node<T> buffer = this.head;
            while (buffer.next != null) {
                buffer = buffer.next;
            }
            buffer.next = n;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void addByPos(int pos, Comparable value) {
        Node<T> buffer = this.head;
        Node<T> newNode = new Node<T>();
        newNode.value = (T)value;
        if (pos == 0) {
            addStart(value);
        } else {
            for (int i = 0; i < pos - 1; i++) {
                buffer = buffer.next;
            }
            newNode.next = buffer.next;
            buffer.next = newNode;
        }
    }

    @Override
    public Comparable removeStart() {
        T value = this.head.value;
        this.head = this.head.next;
        return value;
    }


    @Override
    public Comparable removeEnd() {
        T value = null;
        Node<T> buffer = this.head;
        Node<T> bufferNode = null;
        while (buffer.next != null) {
            bufferNode = buffer;
            buffer = buffer.next;
        }
        buffer = bufferNode;
        value = buffer.value;
        buffer.next = null;
        return value;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Comparable removeByPos(int pos) {
        T deletedValue;
        if (pos == 0) {
            deletedValue = (T)removeStart();
        } else {
            Node<T> buffer = this.head;
            Node<T> nextBuffer = null;
            for (int i = 0; i < pos - 1; i++) {
                buffer = buffer.next;
            }
            nextBuffer = buffer.next;
            deletedValue = buffer.next.value;
            buffer.next = nextBuffer.next;

        }
        return deletedValue;
    }

    @Override
    public Comparable max() {
        T value = this.head.value;
        Node<T> buffer = this.head;
        while (buffer.next != null) {
            buffer = buffer.next;
            if (value.compareTo(buffer.value) < 0) {
                value = buffer.value;
            }
        }
        return value;
    }

    @Override
    public Comparable min() {
        T value = this.head.value;
        Node<T> buffer = this.head;
        while (buffer.next != null) {
            buffer = buffer.next;
            if (value.compareTo(buffer.value) > 0) {
                value = buffer.value;
            }
        }
        return value;
    }

    @Override
    public Comparable maxPos() {
        int position = 0;
        int bufferPos = 0;
        T value = this.head.value;
        Node<T> buffer = this.head;
        while (buffer.next != null) {
            bufferPos++;
            buffer = buffer.next;
            if (value.compareTo(buffer.value) < 0) {
                value = buffer.value;
                position = bufferPos;
            }
        }
        return position;
    }

    @Override
    public Comparable minPos() {
        int position = 0;
        int bufferPos = 0;
        T value = this.head.value;
        Node<T> buffer = this.head;
        while (buffer.next != null) {
            bufferPos++;
            buffer = buffer.next;
            if (value.compareTo(buffer.value) > 0) {
                value = buffer.value;
                position = bufferPos;
            }
        }
        return position;
    }

    @Override
    public Comparable get(int pos) {
        int count = 0;
        Node<T> buffer = this.head;
        while (count != pos) {
            buffer = buffer.next;
            count++;
        }
        return (T)buffer.value;
    }

    @Override
    public void set(int pos, Comparable value) {
        if (this.head == null) {
            this.head.value = (T)value;
        } else {
            Node<T> buffer = this.head;
            for (int i = 0; i < pos; i++) {
                buffer = buffer.next;
            }
            buffer.value = (T)value;
        }
    }

    @Override
    public Object[] fullRevers() {
        return new Object[0];
    }

    @Override
    public Object[] halfRevers() {
        return new Object[0];
    }

    @Override
    public Object[] sort() {
        return new Object[0];
    }

    @Override
    public Object[] toArray() {
        Object[] toArray = new Object[size()];
        int step = 0;
        Node<T> buffer = this.head;
        while (buffer != null) {
            toArray[step++] = buffer.value;
            buffer = buffer.next;
        }
        return toArray;
    }
}
