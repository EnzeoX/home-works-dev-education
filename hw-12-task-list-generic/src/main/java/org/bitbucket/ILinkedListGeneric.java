package org.bitbucket;

public interface ILinkedListGeneric<T extends Comparable<T>> {

    void init(T[] init);

    void clear();

    int size();

    <T> T[] toArray();

    void addStart(T value);

    void addEnd(T value);

    void addByPos(int pos, T value);

    T removeStart();

    T removeEnd();

    T removeByPos(int value);

    T max();

    T min();

    T maxPos();

    T minPos();

    <T> T[] sort();

    T get(int pos);

    <T> T[] halfRevers();

    <T> T[] fullRevers();

    void set(int pos, T value);
}
