package org.bitbucket;

import org.bitbucket.Processor;

public class ProcessorX86 extends Processor {

    final public static String architecture = "X86";

    public ProcessorX86() {

    }

    @Override
    public String toString() {
        return "ProcessorX86{" +
                "frequency=" + frequency +
                ", cache=" + cache +
                ", bitCapacity=" + bitCapacity +
                '}';
    }

    public ProcessorX86(int frequency, int cache, int bitCapacity) {
        this.frequency = frequency;
        this.cache = cache;
        this.bitCapacity = bitCapacity;
    }

    public ProcessorX86(int frequency, int cache, int bitCapacity, String architecture) {
        this.frequency = frequency;
        this.cache = cache;
        this.bitCapacity = bitCapacity;
    }

    @Override
    public String dataProcess(String data) {
        return data.toLowerCase();
    }

    @Override
    public String dataProcess(long data) {
        return Long.toString(data).toLowerCase();
    }

    public String getArchitecture() {
        return architecture;
    }
}
