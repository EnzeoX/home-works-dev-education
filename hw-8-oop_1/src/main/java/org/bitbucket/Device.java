package org.bitbucket;

import java.util.Arrays;

public class Device {

    Processor processor;

    Memory memory;

    public static String[] dataToMemory;

    public Device (Processor processor, Memory memory) {
        this.processor = processor;
        this.memory = memory;
    }

    public void save(String[] data) {
        dataToMemory = new String[data.length];
        for (int i = 0; i < data.length; i++) {
            dataToMemory[i] = data[i];
        }
    }

    public String[] readAll() {
        String[] getInfoFromMemory = this.memory.memoryCell;
        System.out.println(Arrays.toString(getInfoFromMemory));
        Arrays.fill(getInfoFromMemory, null);
        return getInfoFromMemory;
    }

    public void dataProcessing() {
        String[] memoryString = this.memory.memoryCell;
        for(String elem : memoryString) {
            elem = processor.dataProcess(elem);
        }
    }

    public static Device[] sortByArchitecture(Device[] devices, String architecture) {
        Device[] sortedArrayByArchitecture;
        int newArrayLength = 0;
        for (Device device : devices) {
            if (device.processor.getArchitecture().equals(architecture)) {
                newArrayLength++;
            }
        }
        sortedArrayByArchitecture = new Device[newArrayLength];
        int sortedArrayIndex = 0;
        for (int i = 0; i < devices.length; i++) {
            if (devices[i].processor.getArchitecture().equals(architecture)) {
                sortedArrayByArchitecture[sortedArrayIndex] = devices[i];
                sortedArrayIndex++;
            }
        }
        return sortedArrayByArchitecture;
    }

    public static Device[] sortByParametersFrequency(Device[] devices, double frequency) {
        Device[] sortedArrayByParameter;
        int newArrayLength = 0;
        for (Device device:devices) {
            if (device.processor.frequency == frequency) {
                newArrayLength++;
            }
        }
        sortedArrayByParameter = new Device[newArrayLength];
        int sortedArrayIndex = 0;
        for (Device device : devices) {
            if (device.processor.frequency == frequency) {
                sortedArrayByParameter[sortedArrayIndex] = device;
                sortedArrayIndex++;
            }
        }
        return sortedArrayByParameter;
    }

    public static Device[] sortByParameterCache(Device[] devices, double cache) {
        Device[] sortedArrayByParameter;
        int newArrayLength = 0;
        for (Device device:devices) {
            if (device.processor.cache == cache) {
                newArrayLength++;
            }
        }
        int sortedArrayIndex = 0;
        sortedArrayByParameter = new Device[newArrayLength];
        for (Device device : devices) {
            if (device.processor.cache == cache) {
                sortedArrayByParameter[sortedArrayIndex] = device;
                sortedArrayIndex++;
            }
        }
        return sortedArrayByParameter;
    }

    public static Device[] sortByParameterBitCapacity(Device[] devices, double bitCapacity) {
        Device[] sortedArrayByParameter;
        int newArrayLength = 0;
        for (Device device:devices) {
            if (device.processor.bitCapacity == bitCapacity) {
                newArrayLength++;
            }
        }
        int sortedArrayIndex = 0;
        sortedArrayByParameter = new Device[newArrayLength];
        for (Device device : devices) {
            if (device.processor.bitCapacity == bitCapacity) {
                sortedArrayByParameter[sortedArrayIndex] = device;
                sortedArrayIndex++;
            }
        }
        return sortedArrayByParameter;
    }

    public static Device[] sortByMaxSetMemory(Device[] devices, int setValue) {

        int newArrayLength = 0;
        for (Device device:devices) {
            Memory.MemoryInfoValues memory = Memory.getMemoryInfo(device.memory.memoryCell);
            if (memory.freeCellQuantity >= setValue) {
                newArrayLength++;
            }
        }
        int sortedArrayIndex = 0;
        Device[] sortedArrayByParameter = new Device[newArrayLength];
        for (Device device : devices) {
            Memory.MemoryInfoValues memory = Memory.getMemoryInfo(device.memory.memoryCell);
            if (memory.freeCellQuantity >= setValue) {
                sortedArrayByParameter[sortedArrayIndex] = device;
                sortedArrayIndex++;
            }
        }
        return sortedArrayByParameter;
    }

    public static Device[] sortByMinSetMemory(Device[] devices, int setValue) {

        int newArrayLength = 0;
        for (Device device:devices) {
            Memory.MemoryInfoValues memory = Memory.getMemoryInfo(device.memory.memoryCell);
            if (memory.freeCellQuantity <= setValue) {
                newArrayLength++;
            }
        }
        Device[] sortedArrayByParameter = new Device[newArrayLength];
        int sortedArrayIndex = 0;
        for (Device device : devices) {
            Memory.MemoryInfoValues memory = Memory.getMemoryInfo(device.memory.memoryCell);
            if (memory.freeCellQuantity <= setValue) {
                sortedArrayByParameter[sortedArrayIndex] = device;
                sortedArrayIndex++;
            }
        }
        return sortedArrayByParameter;
    }

    public static Device[] sortByTotalMemorySetMax(Device[] devices, int setValue) {

        int newArrayLength = 0;
        for (Device device:devices) {
            Memory.MemoryInfoValues memory = Memory.getMemoryInfo(device.memory.memoryCell);
            if (memory.cellQuantity >= setValue) {
                newArrayLength++;
            }
        }
        Device[] sortedArrayByParameter = new Device[newArrayLength];
        int sortedArrayIndex = 0;
        for (Device device : devices) {
            Memory.MemoryInfoValues memory = Memory.getMemoryInfo(device.memory.memoryCell);
            if (memory.cellQuantity >= setValue) {
                sortedArrayByParameter[sortedArrayIndex] = device;
                sortedArrayIndex++;
            }
        }
        return sortedArrayByParameter;
    }

    public static Device[] sortByTotalMemorySetMin(Device[] devices, int setValue) {

        int newArrayLength = 0;
        for (Device device:devices) {
            Memory.MemoryInfoValues memory = Memory.getMemoryInfo(device.memory.memoryCell);
            if (memory.cellQuantity < setValue) {
                newArrayLength++;
            }
        }
        Device[] sortedArrayByParameter = new Device[newArrayLength];
        int sortedArrayIndex = 0;
        for (int i = 0; i < devices.length; i++) {
            Memory.MemoryInfoValues memory = Memory.getMemoryInfo(devices[i].memory.memoryCell);
            if(memory.cellQuantity < setValue) {
                sortedArrayByParameter[sortedArrayIndex] = devices[i];
                sortedArrayIndex++;
            }
        }
        return sortedArrayByParameter;
    }

    @Override
    public String toString() {
        return "Device{" +
                "processor=" + processor +
                ", memory=" + memory +
                '}';
    }
}
