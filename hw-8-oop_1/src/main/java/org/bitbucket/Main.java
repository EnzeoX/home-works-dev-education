package org.bitbucket;

import org.bitbucket.processors.ProcessorArm;

public class Main {

    public static void main(String[] args) {
        Processor processor1 = new ProcessorX86(40, 8, 16, ProcessorX86.architecture);
        Processor processor2 = new ProcessorX86(3200, 16000, 64, ProcessorX86.architecture);
        Processor processor3 = new ProcessorArm(1500, 1024, 32, ProcessorArm.architecture);
        Processor processor4 = new ProcessorArm(2450, 2048, 64, ProcessorArm.architecture);
        Memory memory1 = new Memory(new String[]{"Texas Instruments", "OMAP", "4460"});
        Device device1 = new Device(processor3, memory1);
        Memory memory2 = new Memory(new String[]{"Qualcomm", "Snapdragon", "835"});
        Device device2 = new Device(processor4, memory2);
        Memory memory3 = new Memory(new String[]{"Intel", "80386", "i386"});
        Device device3 = new Device(processor1, memory3);
        Memory memory4 = new Memory(new String[]{"AMD", "Ryzen", "1600"});
        Device device4 = new Device(processor2, memory4);

        Device[] devices = new Device[10];
        devices[0] = device1;
        devices[1] = device2;
        devices[2] = device3;
        devices[3] = device4;

        for (int i = 0; i < devices.length - 1; i ++) {
            System.out.println(devices[i].toString());
        }
    }
}
