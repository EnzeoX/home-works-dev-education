package org.bitbucket;

import java.util.Arrays;

public class Memory {

    String[] memoryCell = null;

    public Memory(String[] memoryCell) {
        this.memoryCell = memoryCell;
    }

    public static String readLast(String[] memoryCell) {
        if(memoryCell.length == 0){
            throw new NullPointerException("Array is null");
        }
        String infoInCell = "";
        int length = memoryCell.length;
        for (int i = length; i >= 0; i--) {
            if(memoryCell[i - 1] != null){
                infoInCell = memoryCell[i - 1];
                break;
            }
        }
        return infoInCell;
    }

    public static String[] removeLast(String[] memoryCell) {
        for (int i = memoryCell.length - 1; i >= 0; i--) {
            if (memoryCell[i] != null) {
                memoryCell[i] = null;
                break;
            }
        }
        return memoryCell;
    }

    public static boolean save(String[] memoryCell, String info) {
        boolean trueOrFalse = true;
        if (memoryCell.length == 0) {
            throw new NullPointerException("Array is null");
        }
        for (int i = memoryCell.length - 1; i >= 0; i--) {
            if (memoryCell[i] != null) {
                trueOrFalse = false;
            } else if (memoryCell[i] == null) {
                memoryCell[i] = info;
                trueOrFalse = true;
                break;
            }
        }
        return trueOrFalse;
    }

    public String toString() {
        return Arrays.toString(memoryCell);
    }

    public static MemoryInfoValues getMemoryInfo(String[] memoryCell) {
        MemoryInfoValues obj = new MemoryInfoValues();
        obj.memoryOccupied = 0;
        int freeCell = 0;
        double memOccupied = 0;
        for (String s:memoryCell) {
            if(s == null) {
                freeCell++;
            }
        }
        memOccupied = (freeCell*100)/(double)memoryCell.length;
        obj.freeCellQuantity = freeCell;
        obj.memoryOccupied = memOccupied;
        obj.cellQuantity = memoryCell.length;
        return obj;
    }

    public static class MemoryInfoValues {
        double memoryOccupied;
        int freeCellQuantity;
        int cellQuantity;

        public MemoryInfoValues() {
        }

        @Override
        public String toString() {
            return "MemoryInfoValues{" +
                    "memoryOccupied=" + memoryOccupied +
                    ", freeCellQuantity=" + freeCellQuantity +
                    ", cellQuantity" + cellQuantity +
                    '}';
        }

        public MemoryInfoValues(double memoryOccupied, int freeCellQuantity, int cellQuantity) {
            this.freeCellQuantity = freeCellQuantity;
            this.memoryOccupied = memoryOccupied;
            this.cellQuantity = cellQuantity;
        }
    }
}


