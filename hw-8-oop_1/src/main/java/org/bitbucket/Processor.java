package org.bitbucket;

public abstract class Processor {

    public int frequency = 0;

    public int cache = 0;

    public int bitCapacity = 0;

    public Processor(int frequency, int cache, int bitCapacity) {
        this.bitCapacity = bitCapacity;
        this.cache = cache;
        this.frequency = frequency;
    }

    protected Processor() {
    }

    public abstract String dataProcess(String data);

    public abstract String dataProcess(long data);

    public abstract String getArchitecture();

    public double getFrequency() {
        return frequency;
    }

    public double getCache() {
        return cache;
    }

    public double getBitCapacity() {
        return bitCapacity;
    }

    @Override
    public String toString() {
        return "Processor{" +
                "frequency=" + frequency +
                ", cache=" + cache +
                ", bitCapacity=" + bitCapacity +
                '}';
    }

    public String getDetails() {
        return String.format("Frequency: %f\n" +
                             "Cache: %f\n" +
                             "BitCapacity: %f\n",
                getFrequency(),
                getCache(),
                getBitCapacity());
    }
}
