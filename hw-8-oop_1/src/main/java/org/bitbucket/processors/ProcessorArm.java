package org.bitbucket.processors;

import org.bitbucket.Processor;

public class ProcessorArm extends Processor {

    final public static String architecture = "ARM";

    public ProcessorArm(int frequency, int cache, int bitCapacity, String architecture) {
        this.frequency = frequency;
        this.cache = cache;
        this.bitCapacity = bitCapacity;
    }
    public ProcessorArm() {

    }

    @Override
    public String toString() {
        return "ProcessorArm{" +
                "frequency=" + frequency +
                ", cache=" + cache +
                ", bitCapacity=" + bitCapacity +
                '}';
    }

    @Override
    public String dataProcess(String data) {
        return data.toUpperCase();
    }

    @Override
    public String dataProcess(long data) {
        return Long.toString(data).toUpperCase();
    }

    public String getArchitecture() {
        return architecture;
    }
}
