package org.bitbucket.test;

import org.bitbucket.Device;
import org.bitbucket.Memory;
import org.bitbucket.ProcessorX86;
import org.bitbucket.processors.ProcessorArm;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class Testing {

    //Class Memory testing
    @Test
    public void saveInfo() {
        String[] falseData = {"full", "full", "full", "full", "full"};
        boolean exp = Memory.save(MockForTest.testDataWithNull, "newInfo");
        boolean falseExp = Memory.save(MockForTest.testData, "newInfo");
        Assert.assertTrue(exp);
        Assert.assertFalse(falseExp);
    }

    @Test
    public void getMemoryInfoFortyPercent() {
        Memory.MemoryInfoValues exp = new Memory.MemoryInfoValues(40,2, 5);
        Memory.MemoryInfoValues act = Memory.getMemoryInfo(MockForTest.doubleNullData);
        Assert.assertEquals(exp.toString(), act.toString());
    }

    @Test
    public void getMemoryInfoFiftyPercent() {
        Memory.MemoryInfoValues exp = new Memory.MemoryInfoValues(50,2 , 4);
        Memory.MemoryInfoValues act = Memory.getMemoryInfo(MockForTest.doubleNullDataNext);
        Assert.assertEquals(exp.toString(), act.toString());
    }

    @Test
    public void readLastTestFirst() {
        String exp = "simpleDataTest";
        String act = Memory.readLast(MockForTest.doubleNullData);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void readLastTestSecond() {
        String[] data = {"full", "full", null, "retardo", null};
        String exp = "retardo";
        String act = Memory.readLast(data);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void removeLastTestOne() {
        String[] act = Memory.removeLast(MockForTest.testData);
        String[] exp = {"simpleData", "simpleData", "simpleData", null};
        Assert.assertArrayEquals(exp, act);
    }

    //Device test

    @Test
    public void sortByArchitecture() {
        Device[] act = Device.sortByArchitecture(MockForTest.arrayOfDevices, "ARM");
        Device[] exp = MockForTest.expectedDevicesFirst;
        Assert.assertEquals(Arrays.toString(exp), Arrays.toString(act));
    }

    @Test
    public void sortByParametersFrequency() {
        Device[] act = Device.sortByParametersFrequency(MockForTest.arrayOfDevices, 3200.0);
        Device[] exp = MockForTest.deviceNumTwo;
        Assert.assertEquals(Arrays.toString(exp), Arrays.toString(act));
    }

    @Test
    public void sortByParametersCache() {
        Device[] act = Device.sortByParameterCache(MockForTest.arrayOfDevices, 2048.0);
        Device[] exp = MockForTest.deviceNumFour;
        Assert.assertEquals(Arrays.toString(exp), Arrays.toString(act));
    }

    @Test
    public void sortByParametersBitCapacity() {
        Device[] act = Device.sortByParameterBitCapacity(MockForTest.arrayOfDevices, 16);
        Device[] exp = MockForTest.deviceNumOne;
        Assert.assertEquals(Arrays.toString(exp), Arrays.toString(act));
    }

    @Test
    public void sortByMaxSetMemory() {
        Device[] act = Device.sortByMaxSetMemory(MockForTest.arrayOfDevices, 2);
        Device[] exp = {MockForTest.device1, MockForTest.device2};
        Assert.assertEquals(Arrays.toString(exp), Arrays.toString(act));
    }

    @Test
    public void sortByMinSetMemory() {
        Device[] act = Device.sortByMinSetMemory(MockForTest.arrayOfDevices, 2);
        Device[] exp = {MockForTest.device1, MockForTest.device3, MockForTest.device4};
        Assert.assertEquals(Arrays.toString(exp), Arrays.toString(act));
    }

    @Test
    public void sortByTotalMemorySetMax() {
        Device[] act = Device.sortByTotalMemorySetMax(MockForTest.arrayOfDevices, 5);
        Device[] exp = {MockForTest.device1, MockForTest.device2};
        Assert.assertEquals(Arrays.toString(exp), Arrays.toString(act));
    }

    @Test
    public void sortByTotalMemorySetMin() {
        Device[] act = Device.sortByTotalMemorySetMin(MockForTest.arrayOfDevices, 5);
        Device[] exp = {MockForTest.device3, MockForTest.device4};
        Assert.assertEquals(Arrays.toString(exp), Arrays.toString(act));
    }

    //Processors test
    @Test
    public void dataProcessArm() {
        ProcessorArm processor1 = (ProcessorArm) MockForTest.processor3;
        String data = processor1.toString();
        String act = processor1.dataProcess(data);
        String exp = "PROCESSORARM{FREQUENCY=1500, CACHE=1024, BITCAPACITY=32}";
        Assert.assertEquals(exp, act);
    }

    @Test
    public void dataProcessX86() {
        ProcessorX86 processor1 = (ProcessorX86) MockForTest.processor2;
        String data = processor1.toString();
        String act = processor1.dataProcess(data);
        String exp = "processorx86{frequency=3200, cache=16000, bitcapacity=64}";
        Assert.assertEquals(exp, act);
    }

    @Test
    public void dataProcessArmNull () {
        ProcessorArm processor1 = (ProcessorArm) MockForTest.processor5;
        String data = processor1.toString();
        String act = processor1.dataProcess(data);
        String exp = "PROCESSORARM{FREQUENCY=0, CACHE=0, BITCAPACITY=0}";
        Assert.assertEquals(exp, act);
    }

    @Test
    public void dataProcessX86Null () {
        ProcessorX86 processor1 = (ProcessorX86) MockForTest.processor6;
        String data = processor1.toString();
        String act = processor1.dataProcess(data);
        String exp = "processorx86{frequency=0, cache=0, bitcapacity=0}";
        Assert.assertEquals(exp, act);
    }

}
