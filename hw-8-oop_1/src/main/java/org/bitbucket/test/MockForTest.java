package org.bitbucket.test;

import org.bitbucket.Device;
import org.bitbucket.Memory;
import org.bitbucket.Processor;
import org.bitbucket.ProcessorX86;
import org.bitbucket.processors.ProcessorArm;

public class MockForTest {

    public static Processor processor1 = new ProcessorX86(40, 80, 16, ProcessorX86.architecture);
    public static Processor processor2 = new ProcessorX86(3200, 16000, 64, ProcessorX86.architecture);
    public static Processor processor3 = new ProcessorArm(1500, 1024, 32, ProcessorArm.architecture);
    public static Processor processor4 = new ProcessorArm(2450, 2048, 64, ProcessorArm.architecture);
    public static Processor processor5 = new ProcessorArm();
    public static Processor processor6 = new ProcessorX86();
    public static Memory memory1 = new Memory(new String[]{"Texas Instruments", "OMAP", "4460"});
    public static Memory memory2 = new Memory(new String[]{"Qualcomm", "Snapdragon", "835", null});
    public static Memory memory3 = new Memory(new String[]{"INTEL", "80386", "i386", null, null});
    public static Memory memory4 = new Memory(new String[]{"AMD", "Ryzen", "1600", null, null, null});
    public static Device device1 = new Device(processor1, memory3);
    public static Device device2 = new Device(processor2, memory4);
    public static Device device3 = new Device(processor3, memory1);
    public static Device device4 = new Device(processor4, memory2);

    public static String[] data = {"data"};
    public static String[] nullData = null;
    public static String[] emptyData = {};
    public static String[] testData = {"simpleData", "simpleData", "simpleData", "simpleData"};
    public static String[] testDataWithNull = {"simpleData", null, "simpleData", "simpleData"};
    public static String[] doubleNullData = {"simpleData", null, "simpleData", "simpleDataTest", null};
    public static String[] doubleNullDataNext = {"simpleData", null, "simpleData", null};

    public static Device[] arrayOfDevices = {device1, device2, device3, device4};
    public static Device[] arrayOfDevicesWithNull = {device1, device2, device3, device4, null, null};
    public static Device[] deviceNumOne = {device1};
    public static Device[] deviceNumThree = {device3};
    public static Device[] deviceNumTwo = {device2};
    public static Device[] deviceNumFour = {device4};
    public static Device[] twoDevices = {device1, device2};
    public static Device[] threeDevices = {device1, device2, device3};
    public static Device[] noDevice = null;
    public static Device[] noInfoDevice = {};
    public static Device[] expectedDevicesFirst = {device3, device4};
}
