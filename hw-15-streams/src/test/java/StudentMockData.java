import org.bitbucket.Student;

public class StudentMockData {

    public static final Student firstExample = new Student(1,
            "Test",
            "Test",
            1999,
            "TestAdress",
            "+380667172234",
            "TestFaculty",
            "TestCourse",
            "GroupOne");

    public static final Student secondExample = new Student(1,
            "Test1",
            "Test1",
            1996,
            "TestAdress1",
            "+380667172233",
            "TestFaculty1",
            "TestCourse1",
            "GroupOne");

    public static final Student thirdExample = new Student(1,
            "Test2",
            "Test2",
            1998,
            "TestAdress2",
            "+380667172235",
            "TestFaculty1",
            "TestCourse1",
            "GroupTwo");

    public static final Student fourthExample = new Student(1,
            "Test3",
            "Test3",
            1997,
            "TestAdress3",
            "+380667172236",
            "TestFaculty",
            "TestCourse1",
            "GroupTwo");

    public static final Student fifthExample = new Student(1,
            "Test4",
            "Test4",
            1994,
            "TestAdress4",
            "+380667172237",
            "TestFaculty2",
            "TestCourse2",
            "GroupThree");

    public static final Student changedStudentGroupOne = new Student(1,
            "Test",
            "Test",
            1999,
            "TestAdress",
            "+380667172234",
            "TestFaculty",
            "TestCourse",
            "NewGroup");

    public static final Student changedStudentGroupTwo = new Student(1,
            "Test",
            "Test",
            1999,
            "TestAdress",
            "+380667172234",
            "TestFaculty",
            "TestCourse",
            "NewGroup");

    public static final Student changedFacultyOnlyOne = new Student(1,
            "Test",
            "Test",
            1999,
            "TestAdress",
            "+380667172234",
            "NewFaculty",
            "TestCourse",
            "GroupOne");

    public static final Student changedFacultyOne = new Student(1,
            "Test1",
            "Test1",
            1996,
            "TestAdress1",
            "+380667172233",
            "NewFaculty",
            "TestCourse1",
            "GroupOne");

    public static final Student changedFacultyTwo = new Student(1,
            "Test2",
            "Test2",
            1998,
            "TestAdress2",
            "+380667172235",
            "NewFaculty",
            "TestCourse1",
            "GroupTwo");

    public static final Student[] listOfStudentsFull = {firstExample, secondExample, thirdExample, fourthExample, fifthExample};

    public static final Student[] listOfStudentsFour = {firstExample, secondExample, thirdExample, fourthExample};

    public static final Student[] listOfStudentsThree = {firstExample, secondExample, thirdExample};

    public static final Student[] listOfStudentsTwo = {firstExample, secondExample};

    public static final Student[] listOfStudentsOne = {firstExample};

    public static final Student[] listOfStudentsNull = null;

    public static final Student[] listOfStudentsZero = {};

    public static final Student[] sameFaculty = {secondExample, thirdExample};

    public static final Student[] sameGroup = {thirdExample, fourthExample};

    public static final Student[] facultyToChangeOfTwo = {secondExample, thirdExample};

    public static final Student[] listOfChangedFacultyOne = {changedFacultyOnlyOne};

    public static final Student[] listOfChangedFacultyTwo = {changedFacultyOne, changedFacultyTwo};

    public static final Student[] listOfChangedGroupOne = {changedStudentGroupOne};

    public static final Student[] listOfChangedGroupTwo = {changedStudentGroupOne, changedStudentGroupTwo};

    public static Student getFirstExample() {
        return firstExample;
    }

    public static Student getSecondExample() {
        return secondExample;
    }

    public static Student getThirdExample() {
        return thirdExample;
    }

    public static Student getFourthExample() {
        return fourthExample;
    }

    public static Student getFifthExample() {
        return fifthExample;
    }

    public static Student[] getListOfStudentsFull() {
        return listOfStudentsFull;
    }

    public static Student[] getListOfStudentsFour() {
        return listOfStudentsFour;
    }

    public static Student[] getListOfStudentsThree() {
        return listOfStudentsThree;
    }

    public static Student[] getListOfStudentsTwo() {
        return listOfStudentsTwo;
    }

    public static Student[] getListOfStudentsOne() {
        return listOfStudentsOne;
    }

    public static Student[] getListOfStudentsNull() {
        return listOfStudentsNull;
    }

    public static Student[] getListOfStudentsZero() {
        return listOfStudentsZero;
    }

    public static Student[] getSameFaculty() {
        return sameFaculty;
    }

    public static Student[] getSameGroup() {
        return sameGroup;
    }

    public static Student[] getListOfChangedFacultyOne() {
        return listOfChangedFacultyOne;
    }

    public static Student[] getChangedFacultyTwo() {
        return listOfChangedFacultyTwo;
    }

    public static Student[] getFacultyToChangeOfTwo() {
        return facultyToChangeOfTwo;
    }

    public static Student[] getListOfChangedGroupOne() {
        return listOfChangedGroupOne;
    }

    public static Student[] getListOfChangedGroupTwo() {
        return listOfChangedGroupTwo;
    }


}
