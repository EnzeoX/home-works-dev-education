package org.bitbucket;

import org.junit.Assert;
import org.junit.Test;

public class LoopsTest {

    // =========== ТЕСТ ЦИКЛОВ ===============================
    // =========== Первое задание ============================

    @Test
    public void testSumInRange(){
        int[] exp = {2450, 50};
        int[] act = Loops.sumOfNumbersInRange();
        Assert.assertArrayEquals(exp, act);
    }

    // =========== Второе задание ============================
    @Test
    public void testForCompositeNumber(){
        String exp = "Composite";
        String act = Loops.primeOrCompositeNumber(3);
        Assert.assertEquals(exp,act);
    }
    @Test
    public void testForPrimeNumber(){
        String exp = "Prime";
        String act = Loops.primeOrCompositeNumber(42);
        Assert.assertEquals(exp, act);
    }

    // =========== Тертье задание ============================
    @Test
    public void testRootOfNumber(){
        int exp = 8;
        int act = Loops.rootOfNumber(64);
        Assert.assertEquals(exp,act);
    }

    // =========== Четвертое задание =========================
    @Test
    public void testFactorialOfNum(){
        long exp = 5040;
        long act = Loops.factorialOfNum(7);
        Assert.assertEquals(exp, act);
    }

    // =========== Пятое задание =============================
    @Test
    public void testSummOfNumsInNumber(){
        int exp = 12;
        int act = Loops.summOfNumsInNumber(534);
        Assert.assertEquals(exp, act);
    }

    // =========== Шестое задание ============================
    @Test
    public void testReversedNumber(){
        int exp = 451;
        int act = Loops.reversedNumber(154);
        Assert.assertEquals(exp, act);
    }
}
