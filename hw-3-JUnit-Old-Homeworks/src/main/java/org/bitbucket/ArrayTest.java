package org.bitbucket;

import org.junit.Assert;
import org.junit.Test;

public class ArrayTest {

    // =========== ТЕСТЫ ОДНОМЕРНЫХ МАССИВОВ =================
    // =========== Первое задание ============================
    @Test
    public void testMinNumberInArray(){
        int[] arrayToTest = {4,6,8,10,2,15};
        int exp = 2;
        int act = Arrays.minNumOfArray(arrayToTest);
        Assert.assertEquals(exp,act);
    }

    // =========== Второе задание ============================
    @Test
    public void testMaxNumberInArray(){
        int[] arrayToTest = {-1,5,-20,40,10,41,39,52};
        int exp = 52;
        int act = Arrays.maxNumOfArray(arrayToTest);
        Assert.assertEquals(exp, act);
    }

    // =========== Третье задание ============================
    @Test
    public void testIndexOfMinElement(){
        int[] arrayToTest = {-1,5,-20,40,10,41,39,52};
        int exp = 2;
        int act = Arrays.indexOfMinElement(arrayToTest);
        Assert.assertEquals(exp, act);
    }

    // ============ Четвертое задание ========================
    @Test
    public void testIndexOfMaxElement(){
        int[] arrayToTest = {-1,5,-20,40,10,41,39,52};
        int exp = 7;
        int act = Arrays.indexOfMaxElement(arrayToTest);
        Assert.assertEquals(exp, act);
    }

    // ============ Пятое задание ============================
    @Test
    public void testSummOfOddIndexElements(){
        int[] arrayToTest = {0,1,0,1,0,1,0,1};
        int exp = 4;
        int act = Arrays.summOfOddIndexNumbers(arrayToTest);
        Assert.assertEquals(exp, act);
    }

    // ============ Шестое задание ===========================
    @Test
    public void testReversedArray(){
        int[] arrayToTest = {-1,5,-20,40,10,41,39,52};
        int[] exp = {52,39,41,10,40,-20,5,-1};
        int[] act = Arrays.arrayRevers(arrayToTest);
        Assert.assertArrayEquals(exp, act);
    }

    // ============ Седьмое задание ==========================
    @Test
    public void testSumOfOddNumbers(){
        int[] arrayToTest = {3,2,5,4,10,7,5,5};
        int exp = 5;
        int act = Arrays.summOfOddElemnts(arrayToTest);
        Assert.assertEquals(exp, act);
    }

    // ============ Восьмое задание ==========================
    @Test
    public void testChangeTwoSides(){
        int[] arrayToTest = {3,2,5,4,10,7,5,5};
        int[] exp = {10,7,5,5,3,2,5,4};
        int[] act = Arrays.changeTwoSides(arrayToTest);
        Assert.assertArrayEquals(exp,act);

    }

    // ============ Девятое задание (Сортировка Bubble) ==========================
    @Test
    public void testBubbleSort(){
        int[] arrayToTest = {3,5,4,1,2,6};
        int[] exp = {1,2,3,4,5,6};
        int[] act = Arrays.bubbleSort(arrayToTest);
        Assert.assertArrayEquals(exp,act);
    }

    // ============= Девятое задание (Сортировка Select) =========================
    @Test
    public void testSelectionSort(){
        int[] arrayToTest = {3,5,4,1,2,6};
        int[] exp = {1,2,3,4,5,6};
        int[] act = Arrays.bubbleSort(arrayToTest);
        Assert.assertArrayEquals(exp,act);
    }

    // ============= Девятое задание (Сортировка Insertion) ======================
    @Test
    public void testInsertionSort(){
        int[] arrayToTest = {3,5,4,1,2,6};
        int[] exp = {1,2,3,4,5,6};
        int[] act = Arrays.bubbleSort(arrayToTest);
        Assert.assertArrayEquals(exp,act);
    }

    // ============= Десятое задание (Сортировка Quick) ==========================
    @Test
    public void testQuickSort(){
        int[] arrayToTest = {3,5,4,1,2,6};
        int[] exp = {1,2,3,4,5,6};
        int[] act = Arrays.quickSortMain(arrayToTest);
        Assert.assertArrayEquals(exp,act);
    }

    // ============= Десятое задание (Сортировка Merge) ==========================
    @Test
    public void testMergeSort(){
        int[] arrayToTest = {3,5,4,1,2,6};
        int[] exp = {1,2,3,4,5,6};
        int[] act = Arrays.mergeSortMain(arrayToTest);
        Assert.assertArrayEquals(exp,act);
    }

    // ============== Десятое задание (Сортировка Shell) =========================
    @Test
    public void testShellSort(){
        int[] arrayToTest = {3,5,4,1,2,6};
        int[] exp = {1,2,3,4,5,6};
        int[] act = Arrays.shellSortMain(arrayToTest);
        Assert.assertArrayEquals(exp,act);
    }

    // ============== Десятое задание (Сортировка Heap) ==========================
    @Test
    public void testHeapSort(){
        int[] arrayToTest = {3,5,4,1,2,6};
        int[] exp = {1,2,3,4,5,6};
        int[] act = Arrays.shellSortMain(arrayToTest);
        Assert.assertArrayEquals(exp,act);
    }
}
