package org.bitbucket;

import java.util.Arrays;

public class Loops {
    static int quantityOfEven = 0;
    static int summOfEven = 0;
    //static int compositeNumber = 144;
    //static int numForFactorial = 6;

    // Первое задание - Найти сумму четных чисел и их количество в диапазоне от 1 до 99
    public static int[] sumOfNumbersInRange() {
        int[] arrayOfEvenSum = new int[2];
        for (int i = 0; i < 100; i++) {
            if (i % 2 == 0) {
                quantityOfEven += i;
                arrayOfEvenSum[0] = quantityOfEven;
                summOfEven++;
                arrayOfEvenSum[1] = summOfEven;
            }
        }
    return arrayOfEvenSum;
    }

    // Второе задание - Проверить простое ли число?
    public static String primeOrCompositeNumber(int compositeNumber){
        int i = 2;
        int j = 0;
        do {
            if ((compositeNumber % i == 0) && (compositeNumber !=2)){
                j = 1;
                i++;
            }else{
                i++;
            }
        } while ((i*i <= compositeNumber) && (j != 1));
            if(j == 1){
                return "Prime";
            } else {
                return "Composite";
            }
    }

    // Третье задание - Найти корень натурального числа с точностью до целого (рассмотреть вариант последовательного
    // подбора и метод бинарного поиска
    public static int rootOfNumber(int compositeNumber){
        int n = 1;
        while ((compositeNumber != n*n) && (n < compositeNumber)){
            n++;
        }
        if (n < compositeNumber) {
            return n;
        }
        return compositeNumber;
    }

    // Четвертое задание - Вычислить факториал числа n.
    public static long factorialOfNum(int numForFactorial){
        int n = 0;
        long factorialOfNum = 1;
        do {
            n++;
            factorialOfNum *= n;
        } while(n < numForFactorial);
        return factorialOfNum;
    }

    // Пятое задание - Посчитать сумму цифр заданного числа
    public static int summOfNumsInNumber(int compositeNumber){
        int y = compositeNumber;
        int sum = 0;
        while (compositeNumber > 0) {
            int k = compositeNumber % 10;
            sum += k;
            compositeNumber = compositeNumber / 10;
        }
        return sum;
    }

    // Шестое задание - Вывести число, которое является зеркальным отображением последовательности
    // цифр заданного числа, к примеру, задано число 123, вывести 321
    public static int reversedNumber(int compositeNumber){
        int bufferNum = 0;
        int reversedNum = 0;
        while (compositeNumber > 0){
            bufferNum = compositeNumber % 10;
            reversedNum = reversedNum*10+bufferNum;
            compositeNumber = (compositeNumber-bufferNum)/10;
        }
        return reversedNum;
    }
}
