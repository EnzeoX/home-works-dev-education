package org.bitbucket;

import org.junit.Assert;
import org.junit.Test;

public class FunctionsTest {

    // ============== ТЕСТЫ ФУНКЦИЙ ==============================================
    // ============== Первое задание =============================================
    @Test
    public void  testDayOfWeek(){
        String exp = "Friday";
        String act = Functions.dayOfWeekMain(5);
        Assert.assertEquals(exp, act);
    }

    // ============== Второе задание =============================================
    @Test
    public void testNumToString(){
        String exp = "Сто двадцать девять";
        String act = Functions.numToStringBody(129);
        Assert.assertEquals(exp,act);
    }

    // ============== Третье задание =============================================
    @Test
    public void testStringToNum(){
        int exp = 129;
        int act = Functions.stringToNumBody("One hundred twenty nine");
        Assert.assertEquals(exp, act);
    }

    // ============== Четвертое задание ==========================================
    @Test
    public void testDistanceBetweenTwoDots(){
        double exp = 1.9;
        double act = Functions.distanceBetweenTwoDots(6,4,7,3);
        double delta = 0.5;
        Assert.assertEquals(exp,act,delta);
    }

    // ============== Пятое задание ==============================================
    @Test
    public void testNumToStringBillion(){
        String exp = "Один миллиард двести двадцать девять миллионов триста пятьдесят шесть тысяч один";
        String act = Functions.numToStringBillionMain(1229356001);
        Assert.assertEquals(exp,act);
    }

    // ============== Шестое задание =============================================
    @Test
    public void tetsStringToNumBillion(){
        long exp = 1229356001;
        long act = Functions.stringToNumberBillion("One billion two hundred twenty nine million three hundred fifty six thousand one");
        Assert.assertEquals(exp,act);
    }
}
