package org.bitbucket;

import org.junit.Assert;
import org.junit.Test;

public class HomeworkTest {
    // ============ ТЕСТ УСЛОВНЫХ ОПЕРАТОРОВ =================
    // ============ Первое задание ===========================
    @Test
    public void caseOfSum(){

        int exp = 7;
        int act = Modules.sumOrMultiplication(3, 4);
        Assert.assertEquals(exp,act);

    }
    @Test
    public void caseOfMultiplicationCheck(){
        int exp2 = 8;
        int act2 = Modules.sumOrMultiplication(2, 4);
        Assert.assertEquals(exp2, act2);
    }

    // ============= Второе задание ==========================

    @Test
    public void coordinateSystemCheck(){
        String act = Modules.coordinateSystem(3,3);
        Assert.assertEquals("I", act);
    }

    // ============= Третье задание ==========================

    @Test
    public void sumOfThreeNumbers (){
        int exp = 3;
        int act = Modules.summOfPositiveNumbers(3,-4,-2);
        Assert.assertEquals(exp, act);
    }

    // ============ Четвертое задание ========================

    @Test
    public void testMaxOfSumOrMultiplication (){
        int exp =8;
        int act = Modules.maxOfSummOrMultiplication(3, 4,-2);
        Assert.assertEquals(exp, act);
    }

    // =========== Пятое задание =============================

    @Test
    public void testStudentMarkConvertion (){
        String exp = "C";
        String act = Modules.studentsMarkConverter(64);
        Assert.assertEquals(exp, act);
    }

    // =========== ТЕСТ ЦИКЛОВ ===============================
    // =========== Первое задание ============================

    @Test
    public void testSumInRange(){
        int[] exp = {2450, 50};
        int[] act = Loops.sumOfNumbersInRange();
        Assert.assertArrayEquals(exp, act);
    }

    // =========== Второе задание ============================
    @Test
    public void testForCompositeNumber(){
        String exp = "Composite";
        String act = Loops.primeOrCompositeNumber(3);
        Assert.assertEquals(exp,act);
    }
    @Test
    public void testForPrimeNumber(){
        String exp = "Prime";
        String act = Loops.primeOrCompositeNumber(42);
        Assert.assertEquals(exp, act);
    }

    // =========== Тертье задание ============================
    @Test
    public void testRootOfNumber(){
        int exp = 8;
        int act = Loops.rootOfNumber(64);
        Assert.assertEquals(exp,act);
    }

    // =========== Четвертое задание =========================
    @Test
    public void testFactorialOfNum(){
        long exp = 5040;
        long act = Loops.factorialOfNum(7);
        Assert.assertEquals(exp, act);
    }

    // =========== Пятое задание =============================
    @Test
    public void testSummOfNumsInNumber(){
        int exp = 12;
        int act = Loops.summOfNumsInNumber(534);
        Assert.assertEquals(exp, act);
    }

    // =========== Шестое задание ============================
    @Test
    public void testReversedNumber(){
        int exp = 451;
        int act = Loops.reversedNumber(154);
        Assert.assertEquals(exp, act);
    }

    // =========== ТЕСТЫ ОДНОМЕРНЫХ МАССИВОВ =================
    // =========== Первое задание ============================
    @Test
    public void testMinNumberInArray(){
        int[] arrayToTest = {4,6,8,10,2,15};
        int exp = 2;
        int act = Arrays.minNumOfArray(arrayToTest);
        Assert.assertEquals(exp,act);
    }

    // =========== Второе задание ============================
    @Test
    public void testMaxNumberInArray(){
        int[] arrayToTest = {-1,5,-20,40,10,41,39,52};
        int exp = 52;
        int act = Arrays.maxNumOfArray(arrayToTest);
        Assert.assertEquals(exp, act);
    }

    // =========== Третье задание ============================
    @Test
    public void testIndexOfMinElement(){
        int[] arrayToTest = {-1,5,-20,40,10,41,39,52};
        int exp = 2;
        int act = Arrays.indexOfMinElement(arrayToTest);
        Assert.assertEquals(exp, act);
    }

    // ============ Четвертое задание ========================
    @Test
    public void testIndexOfMaxElement(){
        int[] arrayToTest = {-1,5,-20,40,10,41,39,52};
        int exp = 7;
        int act = Arrays.indexOfMaxElement(arrayToTest);
        Assert.assertEquals(exp, act);
    }

    // ============ Пятое задание ============================
    @Test
    public void testSummOfOddIndexElements(){
        int[] arrayToTest = {0,1,0,1,0,1,0,1};
        int exp = 4;
        int act = Arrays.summOfOddIndexNumbers(arrayToTest);
        Assert.assertEquals(exp, act);
    }

    // ============ Шестое задание ===========================
    @Test
    public void testReversedArray(){
        int[] arrayToTest = {-1,5,-20,40,10,41,39,52};
        int[] exp = {52,39,41,10,40,-20,5,-1};
        int[] act = Arrays.arrayRevers(arrayToTest);
        Assert.assertArrayEquals(exp, act);
    }

    // ============ Седьмое задание ==========================
    @Test
    public void testSumOfOddNumbers(){
        int[] arrayToTest = {3,2,5,4,10,7,5,5};
        int exp = 5;
        int act = Arrays.summOfOddElemnts(arrayToTest);
        Assert.assertEquals(exp, act);
    }

    // ============ Восьмое задание ==========================
    @Test
    public void testChangeTwoSides(){
        int[] arrayToTest = {3,2,5,4,10,7,5,5};
        int[] exp = {10,7,5,5,3,2,5,4};
        int[] act = Arrays.changeTwoSides(arrayToTest);
        Assert.assertArrayEquals(exp,act);

    }

    // ============ Девятое задание (Сортировка Bubble) ==========================
    @Test
    public void testBubbleSort(){
        int[] arrayToTest = {3,5,4,1,2,6};
        int[] exp = {1,2,3,4,5,6};
        int[] act = Arrays.bubbleSort(arrayToTest);
        Assert.assertArrayEquals(exp,act);
    }

    // ============= Девятое задание (Сортировка Select) =========================
    @Test
    public void testSelectionSort(){
        int[] arrayToTest = {3,5,4,1,2,6};
        int[] exp = {1,2,3,4,5,6};
        int[] act = Arrays.bubbleSort(arrayToTest);
        Assert.assertArrayEquals(exp,act);
    }

    // ============= Девятое задание (Сортировка Insertion) ======================
    @Test
    public void testInsertionSort(){
        int[] arrayToTest = {3,5,4,1,2,6};
        int[] exp = {1,2,3,4,5,6};
        int[] act = Arrays.bubbleSort(arrayToTest);
        Assert.assertArrayEquals(exp,act);
    }

    // ============= Десятое задание (Сортировка Quick) ==========================
    @Test
    public void testQuickSort(){
        int[] arrayToTest = {3,5,4,1,2,6};
        int[] exp = {1,2,3,4,5,6};
        int[] act = Arrays.quickSortMain(arrayToTest);
        Assert.assertArrayEquals(exp,act);
    }

    // ============= Десятое задание (Сортировка Merge) ==========================
    @Test
    public void testMergeSort(){
        int[] arrayToTest = {3,5,4,1,2,6};
        int[] exp = {1,2,3,4,5,6};
        int[] act = Arrays.mergeSortMain(arrayToTest);
        Assert.assertArrayEquals(exp,act);
    }

    // ============== Десятое задание (Сортировка Shell) =========================
    @Test
    public void testShellSort(){
        int[] arrayToTest = {3,5,4,1,2,6};
        int[] exp = {1,2,3,4,5,6};
        int[] act = Arrays.shellSortMain(arrayToTest);
        Assert.assertArrayEquals(exp,act);
    }

    // ============== Десятое задание (Сортировка Heap) ==========================
    @Test
    public void testHeapSort(){
        int[] arrayToTest = {3,5,4,1,2,6};
        int[] exp = {1,2,3,4,5,6};
        int[] act = Arrays.shellSortMain(arrayToTest);
        Assert.assertArrayEquals(exp,act);
    }

    // ============== ТЕСТЫ ФУНКЦИЙ ==============================================
    // ============== Первое задание =============================================
    @Test
    public void  testDayOfWeek(){
        String exp = "Friday";
        String act = Functions.dayOfWeekMain(5);
        Assert.assertEquals(exp, act);
    }

    // ============== Второе задание =============================================
    @Test
    public void testNumToString(){
        String exp = "Сто двадцать девять";
        String act = Functions.numToStringBody(129);
        Assert.assertEquals(exp,act);
    }

    // ============== Третье задание =============================================
    @Test
    public void testStringToNum(){
        int exp = 129;
        int act = Functions.stringToNumBody("One hundred twenty nine");
        Assert.assertEquals(exp, act);
    }

    // ============== Четвертое задание ==========================================
    @Test
    public void testDistanceBetweenTwoDots(){
        double exp = 1.9;
        double act = Functions.distanceBetweenTwoDots(6,4,7,3);
        double delta = 0.5;
        Assert.assertEquals(exp,act,delta);
    }

    // ============== Пятое задание ==============================================
    @Test
    public void testNumToStringBillion(){
        String exp = "Один миллиард двести двадцать девять миллионов триста пятьдесят шесть тысяч один";
        String act = Functions.numToStringBillionMain(1229356001);
        Assert.assertEquals(exp,act);
    }

    // ============== Шестое задание =============================================
    @Test
    public void tetsStringToNumBillion(){
        long exp = 1229356001;
        long act = Functions.stringToNumberBillion("One billion two hundred twenty nine million three hundred fifty six thousand one");
        Assert.assertEquals(exp,act);
    }
}
