package org.bitbucket;

import org.junit.Assert;
import org.junit.Test;

public class ModulesTest {

    // ============ ТЕСТ УСЛОВНЫХ ОПЕРАТОРОВ =================
    // ============ Первое задание ===========================
    @Test
    public void caseOfSum(){

        int exp = 7;
        int act = Modules.sumOrMultiplication(3, 4);
        Assert.assertEquals(exp,act);

    }
    @Test
    public void caseOfMultiplicationCheck(){
        int exp2 = 8;
        int act2 = Modules.sumOrMultiplication(2, 4);
        Assert.assertEquals(exp2, act2);
    }

    // ============= Второе задание ==========================

    @Test
    public void coordinateSystemCheck(){
        String act = Modules.coordinateSystem(3,3);
        Assert.assertEquals("I", act);
    }

    // ============= Третье задание ==========================

    @Test
    public void sumOfThreeNumbers (){
        int exp = 3;
        int act = Modules.summOfPositiveNumbers(3,-4,-2);
        Assert.assertEquals(exp, act);
    }

    // ============ Четвертое задание ========================

    @Test
    public void testMaxOfSumOrMultiplication (){
        int exp =8;
        int act = Modules.maxOfSummOrMultiplication(3, 4,-2);
        Assert.assertEquals(exp, act);
    }

    // =========== Пятое задание =============================

    @Test
    public void testStudentMarkConvertion (){
        String exp = "C";
        String act = Modules.studentsMarkConverter(64);
        Assert.assertEquals(exp, act);
    }
}
