package org.bitbucket;

public class Arrays {

    // Первое задание - Найти минимальный элемент массива
    public static int minNumOfArray(int[] arrayOfNumbers){
        int minNumber = 0;
        for (int i =0; i < arrayOfNumbers.length; i++){
            if(i == 0){
                minNumber = arrayOfNumbers[i];
            }
            if(minNumber > arrayOfNumbers[i]){
                minNumber = arrayOfNumbers[i];
            }
        }
        return minNumber;
    }

    // Второе задание - Найти максимальный элемент массива
    public static int maxNumOfArray(int[] arrayOfNumbers){
        int maxNumber = 0;
        for (int i = 0; i < arrayOfNumbers.length; i++){
            if(arrayOfNumbers[i] > maxNumber){
                maxNumber = arrayOfNumbers[i];
            }
        }
        return maxNumber;
    }

    // Третье задание - Найти индекс минимального эелемента массива
    public static int indexOfMinElement(int[] arrayOfNumbers){
        int minNumber = 0;
        int indexOfElement = 0;
        for (int i =0; i < arrayOfNumbers.length; i++){
            if(i == 0){
                minNumber = arrayOfNumbers[i];
                indexOfElement = i;
            }
            if(minNumber >= arrayOfNumbers[i]){
                minNumber = arrayOfNumbers[i];
                indexOfElement = i;
            }
        }
        return indexOfElement;
    }

    // Четвертое задание - Найти индекс максимального числа в массиве
    public static int indexOfMaxElement(int [] arrayOfNumbers){
        int maxNumber = 0;
        int indexOfElemnt = 0;
        for (int i = 0; i < arrayOfNumbers.length; i++){
            if(arrayOfNumbers[i] > maxNumber){
                maxNumber = arrayOfNumbers[i];
                indexOfElemnt = i;
            }
        }
        return indexOfElemnt;
    }

    // Пятое задание - Посчитать сумму элементов массива с нечетными индексами
    public static int summOfOddIndexNumbers(int [] arrayOfNumbers){
        int summOfNums = 0;
        for (int i = 1; i < arrayOfNumbers.length; i += 2){
            summOfNums += arrayOfNumbers[i];
        }
        return summOfNums;
    }

    // Шестое задание - Сделать реверс массива
    public static int[] arrayRevers(int [] arrayOfNumbers){
        int arrayLength = arrayOfNumbers.length;
        int numToChange = arrayLength - 1;
        int bufferNum;
        for (int i = 0; i < (arrayLength/2); i++){
            bufferNum = arrayOfNumbers[i];
            arrayOfNumbers[i] = arrayOfNumbers[numToChange - i];
            arrayOfNumbers[numToChange - i] = bufferNum;
        }
        return arrayOfNumbers;
    }

    // Седьмое задание - Посчитать количество нечетных элементов массива
    public static int summOfOddElemnts(int[] arrayOfNumbers){
        int summOfOdd = 0;
        for(int i = 0; i < arrayOfNumbers.length; i++){
            if (arrayOfNumbers[i] % 2 != 0){
                summOfOdd ++;
            }
        }
        return summOfOdd;
    }

    // Восьмое задание - Поменять местами первую и вторую половину массива.
    public static int[] changeTwoSides(int[] arrayOfNumbers){
        int bufferNum;
        int arrayLength = arrayOfNumbers.length;
        int numToChange = arrayLength/2;
        for(int i = 0; i < arrayLength/2; i++){
            bufferNum = arrayOfNumbers[i];
            arrayOfNumbers[i] = arrayOfNumbers[numToChange + i];
            arrayOfNumbers[numToChange + i] = bufferNum;
        }
        return arrayOfNumbers;
    }

    // Девятое задание --- Сортировка Bubble
    public static int[] bubbleSort(int[] arrayOfNumbers){
        int bufferNum;
        int arrLength = arrayOfNumbers.length;
        for (int i = 0; i < arrLength; i++){
            for (int p = 1; p < arrLength; p++){
                if(arrayOfNumbers[p-1] > arrayOfNumbers[p]){
                    bufferNum = arrayOfNumbers[p];
                    arrayOfNumbers[p] = arrayOfNumbers[p-1];
                    arrayOfNumbers[p-1] = bufferNum;
                }
            }
        }
        return arrayOfNumbers;
    }

    // Девятое задание --- Сортировка Select
    public static int[] selectionSort(int [] arrayOfNumbers){
        int bufferNum;
        int bufferMin;
        int arrLength = arrayOfNumbers.length - 1;
        for (int i = 0; i < arrLength; i++){
            bufferMin = i;
            for (int k = i+1; k < arrLength; k++)
                if (arrayOfNumbers[k] < arrayOfNumbers[bufferMin])
                    bufferMin = k;
            bufferNum = arrayOfNumbers[bufferMin];
            arrayOfNumbers[bufferMin] = arrayOfNumbers[i];
            arrayOfNumbers[i] = bufferNum;
        }
        return arrayOfNumbers;
    }

    // Девятое задание --- Сортировка Insertion
    public static int[] insertionSort(int[] arrayOfNumbers){
        int bufferNum;
        int firstElem;
        int arrLength = arrayOfNumbers.length;
        for (int i = 1; i < arrLength; i++){
            firstElem = arrayOfNumbers[i];
            int previousElem = i - 1;
            while (previousElem >= 0 && arrayOfNumbers[previousElem] > firstElem){
                arrayOfNumbers[previousElem +1] = arrayOfNumbers[previousElem];
                previousElem = previousElem - 1;
            }
            arrayOfNumbers[previousElem + 1] = firstElem;
        }
        return arrayOfNumbers;
    }

    // Десятое задание --- Сортировка Quick
    public static int[] quickSortMain(int[] arrayOfNumbers){
        int firstNum = 0;
        int lastNum = arrayOfNumbers.length - 1;
        quickSortBody(arrayOfNumbers, firstNum, lastNum);
        return arrayOfNumbers;
    }
    public static void quickSortBody(int[] arrayOfNumbers, int firstNum, int lastNum){
        int bufferFirst = firstNum;
        int bufferLast = lastNum;
        int newArrLength = arrayOfNumbers[bufferFirst + (bufferLast - bufferFirst)/2];
        do {
            while(arrayOfNumbers[bufferFirst] < newArrLength)
                bufferFirst++;
            while(arrayOfNumbers[bufferLast] > newArrLength)
                bufferLast--;
            if(bufferFirst <= bufferLast){
                int temp = arrayOfNumbers[bufferFirst];
                arrayOfNumbers[bufferFirst] = arrayOfNumbers[bufferLast];
                arrayOfNumbers[bufferLast] = temp;
                bufferFirst++;
                bufferLast--;
            }
        } while(bufferFirst <= bufferLast);
        if (firstNum <  bufferLast) {
            quickSortBody(arrayOfNumbers, firstNum,  bufferLast);
        }
        if (bufferFirst < lastNum) {
            quickSortBody(arrayOfNumbers, bufferFirst, lastNum);
        }
    }

    // Десятое задание --- Сортировка Merge
    public static int[] mergeSortMain(int[] arrayOfNumbers){
        int[] arr = simpleSort(arrayOfNumbers);
        return arr;
    }
    // {3,5,4,1,2,6}
    public static int[] simpleSort(int[] arrayOfNumbers){
        int arrayLength = arrayOfNumbers.length;
        if (arrayLength <= 1){
            return arrayOfNumbers;
        }
        int mid = arrayLength / 2;
        int[] arrLeftSide = new int[mid];
        int[] arrRightSide;
        int[] sortArr  = new int[arrayLength];
        if (arrayLength % 2 == 0){
            arrRightSide = new int[mid];
        } else {
            arrRightSide = new int[mid+1];
        }
        for (int i = 0; i < mid; i++){
            arrLeftSide[i] = arrayOfNumbers[i];
        }
        for (int k = 0; k < arrRightSide.length; k++){
            arrRightSide[k] = arrayOfNumbers[mid + k];
        }
        arrLeftSide = simpleSort(arrLeftSide);
        arrRightSide = simpleSort(arrRightSide);
        sortArr = mergeSort(arrLeftSide, arrRightSide);
        return sortArr;
    }

    public static int[] mergeSort(int[] arrayLeftSide, int[] arrayRightSide){
        int[] sortArr = new int[arrayLeftSide.length + arrayRightSide.length];
        int numLeft = 0;
        int numRight = 0;
        int numSort = 0;
        int lengthOfLeft = arrayLeftSide.length;
        int lengthOfRight = arrayRightSide.length;
        while(numLeft < lengthOfLeft || numRight < lengthOfRight){
            if(numLeft < lengthOfLeft && numRight < lengthOfRight){
                if (arrayLeftSide[numLeft] < arrayRightSide[numRight]){
                    sortArr[numSort] = arrayLeftSide[numLeft];
                    numSort++;
                    numLeft++;
                }else{
                    sortArr[numSort] = arrayRightSide[numRight];
                    numSort++;
                    numRight++;
                }
            }else if(numLeft < lengthOfLeft){
                sortArr[numSort] = arrayLeftSide[numLeft];
                numSort++;
                numLeft++;
            }else if(numRight < lengthOfRight){
                sortArr[numSort] = arrayRightSide[numRight];
                numSort++;
                numRight++;
            }
        }
        return sortArr;
    }

    // Десятое задание --- Сортировка Shell
    public static int[] shellSortMain(int[] arrayOfNumbers){
        int[] arr = shellSortBody(arrayOfNumbers);
        return arr;
    }
    public static int[] shellSortBody(int[] arrayOfNumbers){
        int arrayLength = arrayOfNumbers.length;
        int gap = arrayLength/2;
        int k = 0;
        while(gap > 1){
            while(k < arrayLength){
                for (int i = 0+k; i < arrayLength; i = i + gap){
                    for(int j = gap+k; j < arrayLength; j = j + gap){
                        if(arrayOfNumbers[i] >= arrayOfNumbers[j]){
                            int temp = arrayOfNumbers[i];
                            arrayOfNumbers[i] = arrayOfNumbers[j];
                            arrayOfNumbers[j] = temp;
                        }
                    }
                }
                k++;
            }
            k = 0;
            gap = gap /2;
        }
        if(gap <= 1){
            for(int s = 0; s < arrayLength; s++){
                for(int p = s; p < arrayLength; p++){
                    if(arrayOfNumbers[s] >= arrayOfNumbers[p]){
                        int temp2 = arrayOfNumbers[s];
                        arrayOfNumbers[s] = arrayOfNumbers[p];
                        arrayOfNumbers[p] = temp2;
                    }
                }
            }
        }
        return arrayOfNumbers;
    }

    // Десятое задание --- Сортировка Heap
    public static int[] heapSortMain(int[] arrayOfNumbers){
        int[] arr = heapSortBody(arrayOfNumbers);
        return arr;
    }
    public static void arrayHeapify(int[] arrayOfNumbers, int b, int c){
        int large;
        int left = 2 * b + 1;
        int right = 2 * b + 2;
        if(left < c && arrayOfNumbers[left] > arrayOfNumbers[b]){
            large = left;
        }else{
            large = b;
        }
        if(right < c && arrayOfNumbers[right] > arrayOfNumbers[large]){
            large = right;
        }
        if(large != b){
            int temp = arrayOfNumbers[b];
            arrayOfNumbers[b] = arrayOfNumbers[large];
            arrayOfNumbers[large] = temp;
            arrayHeapify(arrayOfNumbers, large, c);
        }

    }
    public static int[] heapSortBody(int[] array){
        int len = array.length;
        for (int i = (len-1)/2; i >= 0; i--){
            arrayHeapify(array, i, (len-1));
        }
        for (int k = (len-1); k > 0; k--){
            int temp1 = array[0];
            array[0] = array[k];
            array[k] = temp1;
            len--;
            arrayHeapify(array, 0 , (len-1));
        }
        return array;
    }
}
