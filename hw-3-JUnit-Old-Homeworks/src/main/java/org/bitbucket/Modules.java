package org.bitbucket;


class Modules {

    // Первое задание - Eсли первое число четное - посчитать a*b, иначе a+b
    public static int sumOrMultiplication(int firstNumber, int secondNumber) {
        return firstNumber % 2 == 0 ? firstNumber * secondNumber : firstNumber + secondNumber;
    }

    // Второе задание - Определить какой четверти принадлежит точка с координатами x,y
    public static String coordinateSystem(int firstNumber, int secondNumber) {
        return firstNumber > 0 && secondNumber > 0 ? "I":
                (firstNumber < 0 && secondNumber > 0 ? "II":
                        (firstNumber < 0 && secondNumber < 0 ? "III":
                                (firstNumber > 0 && secondNumber < 0 ? "IV" : "Unexpected")));
    }

    // Третье задание - Найти суммы только положительных из трех чисел

    public static int summOfPositiveNumbers(int firstNumber, int secondNumber, int thirdNumber){
        int summ = 0;
        if(firstNumber >=0){
            summ = summ +firstNumber;
        }
        if(secondNumber >= 0){
            summ = summ +secondNumber;
        }
        if(thirdNumber >= 0){
            summ = summ +thirdNumber;
        }
        return summ;
    }

    // Четвертое задание - Посчтиать выражение max(a*b*c, a+b+c)+3

    public static int maxOfSummOrMultiplication (int firstNumber, int secondNumber, int thirdNumber){
        return Math.max((firstNumber*secondNumber*thirdNumber),(firstNumber+secondNumber+thirdNumber))+3;
    }

    // Пятое задание - Определение оценки студента по его рейтенгу, на основе правил.

    public static String studentsMarkConverter (int studentMark){
        return studentMark >= 0 && studentMark <= 19 ? "F" :
                (studentMark > 19 && studentMark <= 39 ? "E" :
                        (studentMark > 39 && studentMark <= 59 ? "D" :
                                (studentMark > 59 && studentMark <= 74 ? "C" :
                                        (studentMark > 74 && studentMark <= 89 ? "B" :
                                                (studentMark > 89 && studentMark <=100 ? "A" : "Wrong mark")))));
    }
}

