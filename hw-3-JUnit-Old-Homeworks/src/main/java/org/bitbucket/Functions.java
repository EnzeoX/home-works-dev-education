package org.bitbucket;

import java.util.LinkedHashMap;

public class Functions {

    // Первое задание - Получить строковое название дня неделт по номеру дня
    public static String dayOfWeekMain(int day) {
        return dayOfWeekBody(day);
    }
    public static String dayOfWeekBody(int day){
        return day == 1 ? "Monday":
                (day == 2 ? "Tuesday":
                        (day == 3 ? "Thursday":
                                (day == 4 ? "Thursday":
                                        (day == 5 ? "Friday":
                                                (day == 6 ? "Saturday":
                                                        (day == 7 ? "Sunday" : "No day of week with that number"))))));
    }

    // Второе задание - Вводим число (0-999), получаем строку с прописью числа
    public static String numToStringBody(int number){
        String convertedNum = numToWord(number);
        return convertedNum;
    }
    static String numToWord(int x){
        String[] str1 = {"","Сто", "Двести", "Триста", "Четыреста", "Пятьсот","Шестьсот", "Семьсот","Восемьсот","Девятьсот"};
        String[] str2 = {"","","двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
        String[] str3 = {"десять", "одинадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
        String[] str4 = {"ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять"};
        int lenOfNumber = String.valueOf(x).length();
        int [] bufferArray = new int[lenOfNumber];
        int y = x;
        int f;
        int k = 0;
        String word = "";
        for (int i = 0; i < lenOfNumber; i++){
            f = x % 10;
            bufferArray[(lenOfNumber-1)-i] = f;
            x = (x-f)/10;
        }
        for (int u = 0; u < lenOfNumber; u++){
            //________________ логика для 3-х значных чисел____________________________
            if (lenOfNumber == 3){
                if (u == 0){
                    word = word + str1[bufferArray[u]] + " ";
                }else if((u == 1)&&(bufferArray[u] == 1)){
                    word = word + str3[bufferArray[u+1]];
                    break;
                }else if(u == 1){
                    if(bufferArray[u] == 0){
                        continue;
                    }else{
                        word = word + str2[bufferArray[u]] + " ";
                    }
                }else if((u == 2) && (bufferArray[u] == 0)){
                    break;
                }else if(u == 2){
                    word = word + str4[bufferArray[u]];
                }
                //____________________ логика для 2-х значных чисел_____________________
            } else if (lenOfNumber == 2){
                if ((u == 0)&&(bufferArray[u] == 1)){
                    word = word + str3[u+bufferArray[u+1]];
                    break;
                } else if (u == 0){
                    word = word + str2[bufferArray[u]] + " ";
                } else if (u == 1){
                    if (bufferArray[u] == 0){
                        break;
                    }else{
                        word = word + str4[bufferArray[u]];
                    }
                }
                //____________________логика для числа с одним значением__________________
            } else if(lenOfNumber == 1){
                word = str4[bufferArray[u]];
            }
        }
        return word;
    }

    // Третье задание - Вводим строку, которая содержит число, написанное прописью (0-999). Получить само число.
    public static int stringToNumMain(String number){
        int wordToNum = stringToNumBody(number);
        return wordToNum;

    }
    public static int stringToNumBody(String number){
        int summ = 0;
        int caseFor = 0;
        String allLowerCase = number.toLowerCase();
        String [] wordInArr = allLowerCase.split(" ");
        int wordToNum = wordInArr.length;
        LinkedHashMap<String, Integer> alp = new LinkedHashMap<String, Integer>();
        alp.put("one" , 1);
        alp.put("two", 2);
        alp.put("three", 3);
        alp.put("four", 4);
        alp.put("five", 5);
        alp.put("six", 6);
        alp.put("seven", 7);
        alp.put("eight", 8);
        alp.put("nine", 9);
        alp.put("ten", 10);
        alp.put("eleven", 11);
        alp.put("twelve", 12);
        alp.put("thirteen", 13);
        alp.put("fourteen", 14);
        alp.put("fifteen", 15);
        alp.put("sixteen", 16);
        alp.put("seventeen", 17);
        alp.put("eighteen", 18);
        alp.put("nineteen", 19);
        alp.put("twenty", 20);
        alp.put("thirty", 30);
        alp.put("forty", 40);
        alp.put("fifty", 50);
        alp.put("sixty", 60);
        alp.put("seventy", 70);
        alp.put("eighty",80);
        alp.put("ninety",90);
        alp.put("hundred", 100);
        alp.put("zero", 0);
        for (int k = 0; k < wordToNum; k++){
            for (String i: alp.keySet()){
                if(wordInArr[k].equals(i)){
                    if (alp.get(i) < 100){
                        caseFor = 1;
                    }else if(alp.get(i) == 100){
                        caseFor = 2;
                    }else if(alp.get(i) == 0){
                        caseFor = 3;
                    }
                    switch(caseFor){
                        case 1:
                            summ += alp.get(i);
                            caseFor = 0;
                            break;
                        case 2:
                            summ = summ * 100;
                            caseFor = 0;
                            break;
                        case 3:
                            summ = 0;
                            break;
                    }
                }
            }
        }
        return summ;
    }

    // Четвертое задание - Найти расстоеяние между двумя точками в двумерном декартовом пространстве
    public static double distanceBetweenTwoDots(int x1,int y1, int x2, int y2){
        double distance = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));;
        return distance;
    }

    // Пятое задание - Вводим число (0-999999999999), получаем строку с прописью числа
    public static String numToStringBillionMain(long number){
        return numToWordBillionBody(number);
    }

    static String numToWordBillionBody(long x){
        int len = String.valueOf(x).length();
        long y = 0;
        long b = x;
        int lenForCase = 0;
        String word = "";
        while (len > 0){
            if((len > 9) && (len < 13)){
                lenForCase = 1;
            }else if((len>6) && (len < 10)){
                lenForCase = 2;
            }else if((len > 3) && (len < 7)){
                lenForCase = 3;
            }else if((len > 0) && (len < 4)){
                lenForCase = 4;
            }
            switch(lenForCase){
                case 1:
                    y = x/1000000000;
                    x = x - (y*1000000000);
                    int len2 = String.valueOf(x).length();
                    word = word + numsToWordCalc(y, len, lenForCase);
                    len = len - (len- len2);
                    y = 0;
                    break;
                case 2:
                    y = x/1000000;
                    x = x - (y*1000000);
                    len2 = String.valueOf(x).length();
                    word = word + numsToWordCalc(y, len, lenForCase);
                    len = len - (len- len2);
                    y = 0;
                    break;
                case 3:
                    y = x/1000;
                    x = x - (y*1000);
                    len2 = String.valueOf(x).length();
                    word = word + numsToWordCalc(y, len, lenForCase);
                    len = len - (len- len2);
                    y = 0;
                    break;
                case 4:
                    y = x/1;
                    x = x - (y*1);
                    len2 = String.valueOf(x).length();
                    word = word + numsToWordCalc(y, len, lenForCase);
                    len = 0;
                    y = 0;
                    break;
                default: break;
            }
        }
        return word.substring(0,1).toUpperCase() + word.substring(1);
    }
    public static String numsToWordCalc(long y, int len, int lenForCase){
        String[] str1 = {"ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять"};
        String[] str2 = {"десять", "одинадцать", "двенадцать", "тринадцать", "четырндацать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
        String[] str3 = {"","","двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
        String[] str4 = {"","сто", "двести", "триста", "четыреста", "пятьсот","шестьсот", "семьсот","восемьсот","девятьсот"};
        String[] str5 = {"тысяча", "тысячи", "тысяч"};
        String[] str6 = {"миллион", "миллиона", "миллионов"};
        String[] str7 = {"миллиард", "миллиарда", "миллиардов"};
        String[] special = {"одна тысяча", "две тысячи"};
        int yLen = String.valueOf(y).length();
        int[] arrInt = new int[yLen];
        int z;
        String res = "";
        int k = (int)y;
        for (int i = 0; i < yLen; i++){
            z = k % 10;
            arrInt[(yLen-1)-i] = z;
            k = (k-z)/10;
        }
        for (int u = 0; u < yLen; u++){
            //-------------------------------------------------------- логика для 3-х значных чисел начало
            if (yLen == 3){
                if (u == 0){
                    res += str4[arrInt[u]] + " ";
                }else if(u == 1){
                    if(arrInt[u] == 0){
                        continue;
                    }else if(arrInt[u] == 1){
                        if (lenForCase == 1){
                            res += str2[arrInt[u+1]] + " " + str7[2] + " ";
                        }else if(lenForCase == 2){
                            res += str2[arrInt[u+1]] + " " + str6[2] + " ";
                        }else if(lenForCase == 3){
                            res += str2[arrInt[u+1]] + " " + str5[2] + " ";
                        }else{
                            res += str2[arrInt[u+1]];
                        }
                        break;
                    }else if(arrInt[u] > 1){
                        if (lenForCase == 1){
                            res += str3[arrInt[u]] + " ";
                        }else if(lenForCase == 2){
                            res += str3[arrInt[u]] + " ";
                        }else if(lenForCase == 3){
                            res += str3[arrInt[u]] + " ";
                        }else{
                            res += str3[arrInt[0]] + " ";
                        }
                    }
                }else if((u == 2) && (arrInt[u] == 0)){
                    if (lenForCase == 1){
                        res += str7[2] + " ";
                    }else if(lenForCase == 2){
                        res += str6[2] + " ";
                    }else if(lenForCase == 3){
                        res += str5[2] + " ";
                    }else{
                        continue;
                    }
                    break;
                }else if(u == 2){
                    if(arrInt[u] == 1){
                        if(lenForCase == 1){
                            res +=str1[1] + " " + str7[0] + " ";
                        }else if(lenForCase == 2){
                            res +=str1[1] + " " + str6[0] + " ";
                        }else if(lenForCase == 3){
                            res +=special[0] + " ";
                        }else{
                            res += str1[arrInt[u]];
                        }
                    }else if(arrInt[u] == 2){
                        if(lenForCase == 1){
                            res +=str1[2] + " " + str7[1] + " ";
                        }else if(lenForCase == 2){
                            res +=str1[2] + " " + str6[1] + " ";
                        }else if(lenForCase == 3){
                            res +=special[1] + " ";
                        }else{
                            res += str1[arrInt[u]];
                        }
                    }else if((arrInt[u] > 2) && (arrInt[u] < 5)){
                        if(lenForCase == 1){
                            res += str1[arrInt[u]] + " " + str7[1] + " ";
                        }else if(lenForCase == 2){
                            res += str1[arrInt[u]] + " " + str6[1] + " ";
                        }else if(lenForCase == 3){
                            res += str1[arrInt[u]] + " " + str5[1] + " ";
                        }else{
                            res += str1[arrInt[u]];
                        }
                    }else if((arrInt[u] > 4) &&(arrInt[u] < 10)){
                        if(lenForCase == 1){
                            res += str1[arrInt[u]] + " " + str7[2] + " ";
                        }else if(lenForCase == 2){
                            res += str1[arrInt[u]] + " " + str6[2] + " ";
                        }else if(lenForCase == 3){
                            res += str1[arrInt[u]] + " " + str5[2] + " ";
                        }else{
                            res += str1[arrInt[u]];
                        }
                    }
                }
                // --------------------------------------------------------конец логики для 3-х значных чисел
                // --------------------------------------------------------логика для 2-х значных чисел
            }else if(yLen == 2){
                if(u == 0){
                    if(arrInt[u] == 1){
                        if (lenForCase == 1){
                            res += str2[u+arrInt[u+1]] + " " + str7[2] + " ";
                        }else if(lenForCase == 2){
                            res += str2[u+arrInt[u+1]] + " " + str6[2] + " ";
                        }else if(lenForCase == 3){
                            res += str2[u+arrInt[u+1]] + " " + str5[2] + " ";
                        }else{
                            res += str2[u+arrInt[u+1]];
                        }
                        break;
                    }else if(arrInt[u] > 1){
                        res += str3[arrInt[u]] + " ";
                    }
                }else if(u == 1){
                    if (arrInt[u] == 0){
                        if (lenForCase == 1){
                            res += str7[2] + " ";
                        }else if (lenForCase == 2){
                            res += str6[2] + " ";
                        }else if (lenForCase == 3){
                            res += str5[2] + " ";
                        }
                        break;
                    }else if(arrInt[u] == 1){
                        if(lenForCase == 1){
                            res +=str1[1] + " " + str7[0] + " ";
                        }else if(lenForCase == 2){
                            res +=str1[1] + " " + str6[0] + " ";
                        }else if(lenForCase == 3){
                            res +=special[0] + " ";
                        }else{
                            res += str1[arrInt[u]];
                        }
                    }else if(arrInt[u] == 2){
                        if(lenForCase == 1){
                            res +=str1[2] + " " + str7[1] + " ";
                        }else if(lenForCase == 2){
                            res +=str1[2] + " " + str6[1] + " ";
                        }else if(lenForCase == 3){
                            res +=special[1] + " ";
                        }else{
                            res += str1[arrInt[u]];
                        }
                    }else if((arrInt[u] > 2) && (arrInt[u] < 5)){
                        if(lenForCase == 1){
                            res += str1[arrInt[u]] + " " + str7[1] + " ";
                        }else if(lenForCase == 2){
                            res += str1[arrInt[u]] + " " + str6[1] + " ";
                        }else if(lenForCase == 3){
                            res += str1[arrInt[u]] + " " + str5[1] + " ";
                        }else{
                            res += str1[arrInt[u]];
                        }
                    }else if((arrInt[u] > 4) &&(arrInt[u] < 10)){
                        if(lenForCase == 1){
                            res += str1[arrInt[u]] + " " + str7[2] + " ";
                        }else if(lenForCase == 2){
                            res += str1[arrInt[u]] + " " + str6[2] + " ";
                        }else if(lenForCase == 3){
                            res += str1[arrInt[u]] + " " + str5[2] + " ";
                        }else{
                            res += str1[arrInt[u]];
                        }
                    }else{
                        res += str1[arrInt[u]];
                    }
                }
                // --------------------------------------------------------конец логики для 2-х значных чисел
                // --------------------------------------------------------логика для числа с одним значением
            }else if(yLen == 1){
                if(arrInt[u] == 1){
                    if(lenForCase == 1){
                        res +=str1[1] + " " + str7[0] + " ";
                    }else if(lenForCase == 2){
                        res +=str1[1] + " " + str6[0] + " ";
                    }else if(lenForCase == 3){
                        res +=special[0] + " ";
                    }else{
                        res += str1[arrInt[u]];
                    }
                }else if(arrInt[u] == 2){
                    if(lenForCase == 1){
                        res +=str1[2] + " " + str7[1] + " ";
                    }else if(lenForCase == 2){
                        res +=str1[2] + " " + str6[1] + " ";
                    }else if(lenForCase == 3){
                        res +=special[1] + " ";
                    }else{
                        res += str1[arrInt[u]];
                    }
                }else if((arrInt[u] > 2) && (arrInt[u] < 5)){
                    if(lenForCase == 1){
                        res += str1[arrInt[u]] + " " + str7[1] + " ";
                    }else if(lenForCase == 2){
                        res += str1[arrInt[u]] + " " + str6[1] + " ";
                    }else if(lenForCase == 3){
                        res += str1[arrInt[u]] + " " + str5[1] + " ";
                    }else{
                        res += str1[arrInt[u]];
                    }
                }else if((arrInt[u] > 4) &&(arrInt[u] < 10)){
                    if(lenForCase == 1){
                        res += str1[arrInt[u]] + " " + str7[2] + " ";
                    }else if(lenForCase == 2){
                        res += str1[arrInt[u]] + " " + str6[2] + " ";
                    }else if(lenForCase == 3){
                        res += str1[arrInt[u]] + " " + str5[2] + " ";
                    }else{
                        res += str1[arrInt[u]];
                    }
                }
            }
            //----------------------------------------------------------конец логики для числа с одним значением
        }
        return res;
    }

    // Шестое задание - Вводим строку, которая содержит число, написанное прописью (0-999999999999). Получить само число.
    public static long stringToNumberBillion(String wordToNum){
        return stringToNumerBillionBody(wordToNum);
    }
    public static long stringToNumerBillionBody(String number){
        long summ = 0;
        int caseFor = 0;
        long buffer = 0;
        long bufferPlus = 0;
        String allLowerCase = number.toLowerCase();
        String [] wordInArr = allLowerCase.split(" ");
        int w = wordInArr.length;
        LinkedHashMap<String, Integer> alp = new LinkedHashMap<String, Integer>();
        alp.put("one" , 1);
        alp.put("two", 2);
        alp.put("three", 3);
        alp.put("four", 4);
        alp.put("five", 5);
        alp.put("six", 6);
        alp.put("seven", 7);
        alp.put("eight", 8);
        alp.put("nine", 9);
        alp.put("ten", 10);
        alp.put("eleven", 11);
        alp.put("twelve", 12);
        alp.put("thirteen", 13);
        alp.put("fourteen", 14);
        alp.put("fifteen", 15);
        alp.put("sixteen", 16);
        alp.put("seventeen", 17);
        alp.put("eighteen", 18);
        alp.put("nineteen", 19);
        alp.put("twenty", 20);
        alp.put("thirty", 30);
        alp.put("forty", 40);
        alp.put("fifty", 50);
        alp.put("sixty", 60);
        alp.put("seventy", 70);
        alp.put("eighty",80);
        alp.put("ninety",90);
        alp.put("hundred", 100);
        alp.put("thousand", 1000);
        alp.put("million", 1000000);
        alp.put("billion", 1000000000);
        alp.put("zero", 0);
        for (int k = 0; k < w; k++){
            for (String i: alp.keySet()){
                if(wordInArr[k].equals(i)){
                    if (alp.get(i) < 100){
                        caseFor = 1;
                    }else if(alp.get(i) == 100){
                        caseFor = 2;
                    }else if(alp.get(i) == 0){
                        caseFor = 3;
                    }else if(alp.get(i) == 1000){
                        caseFor = 4;
                    }else if(alp.get(i) == 1000000){
                        caseFor = 5;
                    }else if(alp.get(i) == 1000000000){
                        caseFor = 6;
                    }
                    switch(caseFor){
                        case 1:
                            summ += alp.get(i);
                            buffer += alp.get(i);
                            bufferPlus += alp.get(i);
                            break;
                        case 2:
                            summ -= buffer;
                            buffer = buffer * 100;
                            summ += buffer;
                            bufferPlus = bufferPlus * 100;
                            buffer = 0;
                            break;
                        case 3:
                            summ = 0;
                            break;
                        case 4:
                            summ = summ - bufferPlus;
                            bufferPlus = bufferPlus * 1000;
                            summ += bufferPlus;
                            buffer = 0;
                            bufferPlus = 0;
                            break;
                        case 5:
                            summ = summ - bufferPlus;
                            bufferPlus = bufferPlus * 1000000;
                            summ += bufferPlus;
                            buffer = 0;
                            bufferPlus = 0;
                            break;
                        case 6:
                            summ = summ - buffer;
                            buffer = buffer * 1000000000;
                            summ = summ * 1000000000;
                            summ += buffer;
                            buffer = 0;
                            bufferPlus = 0;
                            break;
                    }
                }
            }
        }
        return summ;
    }
}
