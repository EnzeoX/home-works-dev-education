package org.bitbucket.figures;

public class Main {
    public static void main(String[] args) {
        System.out.println("Вывод заполненого квадрата");
        ChooseFigure.figureToOutSquare();
        System.out.println("=========================");
        System.out.println("Вывод пустого квадрата");
        ChooseFigure.figureToOutEmptySquare();
        System.out.println("=========================");
        System.out.println("Вывод треугольника углом в левый верхний угол");
        ChooseFigure.figureToOutTriangleTopLeft();
        System.out.println("=========================");
        System.out.println("Вывод треугольника углом в левый нижний угол");
        ChooseFigure.figureToOutTriangleBottomLeft();
        System.out.println("=========================");
        System.out.println("Вывод треугольника углом в правый нижний угол");
        ChooseFigure.figureToOutTriangleBottomRight();
        System.out.println("=========================");
        System.out.println("Вывод треугольника углом в правый верхний угол");
        ChooseFigure.figureToOutTriangleTopRight();
        System.out.println("=========================");
        System.out.println("Вывод крестика");
        ChooseFigure.figureToOutX();
        System.out.println("=========================");
        System.out.println("Вывод треугольника углом направлением с верха к центру");
        ChooseFigure.figureToOutTriangleFromTopToCentre();
        System.out.println("=========================");
        System.out.println("Вывод треугольника углом направлением с низа к центру");
        ChooseFigure.figureToOutTriangleFromBottomToCentre();
        System.out.println("=========================");
    }
}
