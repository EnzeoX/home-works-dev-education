package org.bitbucket.random;

public class TenRandomNumbersInRange {
    public static void main(String[] args) {
        System.out.println("Вывод случайных чисел");
        int firstNum = -999;
        int secondNum = 999;
        for (int i = 0; i <= 10; i++) {
            System.out.println(Randomizer.randomNum(firstNum, secondNum));
        }
    }
}
