package org.bitbucket.random;

import java.util.Random;

public class Randomizer {
    public static int randomNum(int minNum, int maxNum) {
        Random random = new Random();
        return random.nextInt(maxNum - minNum) + minNum;
    }
}
