package org.bitbucket.random;

public class TenRandomNumbers {
    public static void main(String[] args) {
        System.out.println("Вывод случайных чисел");
        int firstNum = 1;
        int secondNum = 100;
        for (int i = 0; i <= 10; i++) {
            System.out.println(Randomizer.randomNum(firstNum, secondNum));
        }
    }
}
