package org.bitbucket.random;

public class RandomInRangeTwentyToFifty {
    public static void main(String[] args) {
        System.out.println("Вывод случайных чисел от 20 до 50");
        int firstNum = 20;
        int secondNum = 50;
        for (int i = 0; i <= 10; i++) {
            System.out.println(Randomizer.randomNum(firstNum, secondNum));
        }
    }
}
