package org.bitbucket.random;

public class RandomInRangeMinusTenToTen {
    public static void main(String[] args) {
        System.out.println("Вывод случайных чисел от -10 до 10");
        int firstNum = -10;
        int secondNum = 10;
        for (int i = 0; i <= 10; i++) {
            System.out.println(Randomizer.randomNum(firstNum, secondNum));
        }
    }
}
