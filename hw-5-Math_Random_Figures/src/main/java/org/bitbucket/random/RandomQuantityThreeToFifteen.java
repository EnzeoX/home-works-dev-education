package org.bitbucket.random;

public class RandomQuantityThreeToFifteen {
    public static void main(String[] args) {
        System.out.println("Вывод случайных чисел в случайном количестве (от 3 до 15)\nДиапазон случайных чисел - от -10 до 35.");
        int firstNum = -10;
        int secondNum = 35;
        int minQuantity = 3;
        int maxQuantity = 15;

        for (int i = 0; i <= Randomizer.randomNum(minQuantity, maxQuantity); i++) {
            System.out.println(Randomizer.randomNum(firstNum, secondNum));
        }
    }
}
