package org.bitbucket.math;

import java.awt.*;

public class TrueOrFalse {
    public static boolean dotInsideObject(double pointX, double pointY) {
        boolean trueOrFalse;
        double numX = Math.abs(pointX);
        int[] firstPoint = {0,0, 2};
        int[] secondPoint = {0,-1,2};
        Polygon triangle = new Polygon(firstPoint, secondPoint, 3);
        trueOrFalse = triangle.contains(numX, pointY);
        return trueOrFalse;
    }
}

