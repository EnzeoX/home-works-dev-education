package org.bitbucket.math;

public class FlyDistance {

    public static String[] start(int gunDegreeNUmber, double gunRadianNumber, double gunVelocityNumber) {
        String[] arrayOfNums = new String[2];
        arrayOfNums[0] = Double.toString(distanceCalculationForDegree(gunDegreeNUmber, gunVelocityNumber));
        arrayOfNums[1] = Double.toString(distanceCalculationForRadian(gunRadianNumber, gunVelocityNumber));
        return arrayOfNums;
    }
    public static double distanceCalculationForDegree(int gunDegree, double muzzleVelocity) {
        return ((muzzleVelocity*muzzleVelocity)*Math.sin(2*(gunDegree*(Math.PI/180))))/9.81;
    }

    public static double distanceCalculationForRadian(double gunRadian, double muzzleVelocity) {
        return ((muzzleVelocity*muzzleVelocity)*Math.sin(2*gunRadian))/9.81;
    }
}
