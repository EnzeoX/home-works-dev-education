package org.bitbucket.math;

public class Main {
    public static void main(String[] args) {
        System.out.println("Первое задание: Стрельба из гаубицы.");
        String[] arr = FlyDistance.start(45,0.785,10);
        System.out.println("Дистанция полета составляет (расчет для угла) - " + arr[0] +"\n"
                         + "Дистанция полета составялет (расчет для радианов) - " + arr[1] + "\n"
                         + "========================================================\n");
        System.out.println("Второе задание. Найти расстояние между двумя автомобилями");
        double distanceBetweenCars = DistanceBetweenCars.calculateDistanceBetweenCars(60, 80, 1,5);
        System.out.println("Расстояние между двумя автомобилями составляет - " + distanceBetweenCars+" км\n=================================\n");
        System.out.println("Третье задание: Логическое выражение, которое принимает значение 1 если точка лежит внутри заштрихованной области, 0 если нет."+
                           "\nПринятое значение - " + TrueOrFalse.dotInsideObject(0,-0.7) + "\n=================================\n");
        System.out.println("Четвертое задание: Вычислить выражение\nРезультат вычисления - " + FormulaCalculation.formulaCalculate(25));
    }
}
