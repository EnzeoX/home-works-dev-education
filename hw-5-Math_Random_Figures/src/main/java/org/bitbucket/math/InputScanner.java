package org.bitbucket.math;

import java.util.Scanner;

public class InputScanner {
    static Scanner SCANNER = new Scanner(System.in);
    private static int inputInteger = SCANNER.nextInt();
    public static int getInputInteger() {
        return inputInteger;
    }
}
