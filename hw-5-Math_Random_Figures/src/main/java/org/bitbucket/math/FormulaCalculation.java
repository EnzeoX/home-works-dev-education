package org.bitbucket.math;

public class FormulaCalculation {
    public static double formulaCalculate(int number) {
        int x = number;
        double numerator = (6*Math.log(Math.sqrt((Math.exp(x+1) + 2*Math.exp(x) * Math.cos(x)))));
        double denominator = Math.log(x - Math.exp(x + 1) * Math.sin(x));
        double cosX = Math.cos(x);
        double expOfSinX = Math.exp(Math.sin(x));
        return (numerator)/(denominator) + Math.abs(cosX/expOfSinX);
    }
}
