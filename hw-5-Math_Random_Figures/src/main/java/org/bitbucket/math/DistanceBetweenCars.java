package org.bitbucket.math;

public class DistanceBetweenCars {
    public static double calculateDistanceBetweenCars(int firstCarSpeed, int secondCarSpeed, double timeAfter, int distanceBetweenBefore) {
        return Math.addExact(firstCarSpeed, secondCarSpeed)*timeAfter+distanceBetweenBefore;
    }
}
