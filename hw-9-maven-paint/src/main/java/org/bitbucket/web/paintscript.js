const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");
let coord = { x: 0, y: 0 };

document.addEventListener("mousedown", start);
document.addEventListener("mouseup", stop);
window.addEventListener("resize", resize);

resize();

var brushSize = parseInt(document.getElementById("brushSize").value);
var selectedColor = "#000000";
ctx.lineCap = "round";

function resize() {
  ctx.canvas.width = 800;
  ctx.canvas.height = 700;
}
function reposition(event) {
  coord.x = event.clientX - canvas.offsetLeft;
  coord.y = event.clientY - canvas.offsetTop;
}
function start(event) {
  document.addEventListener("mousemove", draw);
  reposition(event);
}
function stop() {
  document.removeEventListener("mousemove", draw);
}
function draw(event) {
  ctx.beginPath();
  ctx.strokeStyle = selectedColor;
  ctx.moveTo(coord.x, coord.y);
  reposition(event);
  ctx.lineTo(coord.x, coord.y);
  ctx.stroke();
}

function colorSelect() {
    selectedColor = document.getElementById("colorSelect").value;
}

function setBrushSize() {
  brushSize = parseInt(document.getElementById("brushSize").value);
  document.getElementById("output").innerHTML = brushSize;
  ctx.lineWidth = brushSize;
  console.log(brushSize);
}

function eraserSet() {
    if(document.getElementById("eraserSelect").checked == true) {
        document.getElementById("colorSelect").disabled = true;
        ctx.lineCap = "round";
        selectedColor = "#FFFFFF";
    } else if(document.getElementById("eraserSelect").checked == false) {
        document.getElementById("colorSelect").disabled = false;
        colorSelect();
    }
}